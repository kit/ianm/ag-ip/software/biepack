%> @file PiecewiseCurve.m
%> @brief Contains the geom.PiecewiseCurve class.
% ======================================================================
%> @brief A list of connected ParamCurve objects
%>
%> A PiecewiseCurve represents a list of connected curve segments, each
%> represented by a ParamCurve object. It contains information about the
%> orientation of each curve segment and thus about which points of
%> adjacent segments correspond to each other.
%>
%> Its implementation makes use of the CurveUnion functionality.
%>
%> @todo Add possibility for non-subclass to mark the curve as closed.
% ======================================================================
classdef PiecewiseCurve < BIEPack.geom.CurveUnion
    
    properties (SetAccess = protected)
        
        %> @brief The orientation of each curve segment as part of the overall PiecewiseCurve:
        %>
        %> +1 if the segment is oriented as the ParamCurve itself. 
        %>
        %> -1 if the orientation of the segment is reversed.
        orientations;
        
    end

    
    methods
       
        %> @brief Constructs a PiecewiseCurve by a list of curves and their
        %> orientations. The curve objects must be derived from the 
        %> ParamCurve class. The orientation is either +1 (from start point 
        %> to end point) or -1 (from end point to start point). These 
        %> points must coincide for adjacent segments. 
        %>        
        %> @param varargin List  (curve1, orientation1, curve2,
        %> orientation2, ...)  of the curves and their orientations. 
        %> Number of arguments should be even.
        %>
        %> @retval The newly constructed PiecewiseCurve object.

        function piecewiseCurve = PiecewiseCurve(varargin)
            
            % number of arguments should be even
            if ( mod(nargin,2) ~= 0 )
                error( 'In PiecewiseCurve constructor: number of arguments is odd.' );
            end
            
            onlySegments = varargin(1:2:nargin-1);
            piecewiseCurve@BIEPack.geom.CurveUnion(onlySegments{:});
            
            % initialize the orientations property
            piecewiseCurve.orientations = zeros(nargin/2,1);
            
            % Check whether each segment is of the correct class.
            % Assign orientations
            for j=1:2:nargin-1
                
                if ( ~ BIEPack.utils.derivedFrom( varargin{j}, 'BIEPack.geom.ParamCurve' ) )
                    error( 'In PiecewiseCurve constructor: argument %d is not a BIEPack.geom.ParamCurve', j );
                end
                if ( ( varargin{j+1} ~= 1 ) && ( varargin{j+1} ~= -1 ) )
                    error( 'In PiecewiseCurve constructor: argument %d is not +/- 1', j+1 );
                end
                
                piecewiseCurve.orientations( (j+1)/2 ) = varargin{j+1};                
                
            end
            
        end
                
    end
    
end

