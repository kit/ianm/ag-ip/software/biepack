function genUse

% import the BIEPack package
import BIEPack.*;

% define a rectangle with corner points (1,1) and (3,2)
rect1 = geom.Rectangle( [1;1], [3;2] );
rect2 = rect1.copy;
rect2.rotate( pi/2, [1;1] ).translate( [4; 0] );

% plot the rectangles
hold on

rect1.plot(2);
rect2.plot(2);

hold off

axis( [ 0 6 0 3] );
axis equal

end