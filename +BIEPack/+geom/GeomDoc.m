%> @file GeomDoc.m
%> @brief The documentation of the @a geom package
%
% ======================================================================
%>
%> @namespace BIEPack::geom
%> @brief Classes representing geometrical objects
%>
%> The package @a geom provides geometrical objects. In the current implementation,
%> these are mathematical curves represented by the class Curve. Such
%> objects are unions of curve segments which each can be represented by a
%> regular differentiable parametrization.
%>
%> Elementary geometrical transformations such as translations, rotations and 
%> reflections may be applied to any curve. It is also possible to create a
%> plot of a curve. The functionality for these operations is
%> located directly in the abstract Curve class. The class also has fields
%> describing smoothness or topology of the curve.
%>
%> The simplest concrete examples of Curve objects are obtained using a
%> single parametrization for the entire curve. Such objects are represented
%> by the ParamCurve class and derived from it are many concrete curves such
%> as Circle, StraightLine or the Kite from the Colton/Kress books. The
%> ParamCurve class provides the interface for evaluation of the
%> parametrization while the derived classes implement concrete
%> parametrizations.
%> 
%> Assembling a general Curve from individual segments is done through the
%> CurveUnion and PiecewiseCurve classes. A CurveUnion represents a number
%> of segments, each of which can be a Curve itself and has no special
%> relation to the other segments. An example might be the boundary of an annulus
%> which consists of two separate circles. In a PiecewiseCurve object, each
%> segment is represented by a parametrizations and the end point of a segment 
%> is the starting point of the next segment. It is also possible to give an 
%> orientation to each segment. A concrete example is given by the class Rectangle wich
%> consists of four connected StraightLine objects. 
%
% ======================================================================


