%> @file DegenerateKernelApprox.m
%> @brief File containing the BIEPack.methods.DegenerateKernelApprox class.
% ======================================================================
%> @ingroup MethodsGroup 
%> @brief Class representing a degenerate kernel approximation method based
%> on interpolation by piecewise linear splines.
%
%> The method can be applied to an integral equation of the second kind with 
%> an integral operator with a continuous kernel. The kernel function is
%> approximated by a degenerate kernel through interpolation by piecewise 
%> linear splines and the linear system equivalent to the approximated 
%> equation is solved.
%>
%> On construction, the operator @a A and the right hand side @f$\psi@f$ in 
%> the equation
%> @f[
%>     ( I - A ) \, \varphi = \psi
%> @f]
%> have to be provided. 
%>
%> The operator is represented as an instance of @link BIEPack.ops.DegenerateKernelOp DegenerateKernelOp @endlink which
%> requires it to have a continuous kernel and to map a @link BIEPack.spaces.BemSpace BemSpace @endlink onto
%> itself. This space must be based on a @link BIEPack.meshes.ParamCurveMesh 
%> ParamCurveMesh @endlink
%>
%> The method works simply by interpolation with piecewise linear splines 
%> on the mesh. The right hand side is provided as a coefficient vector in the 
%> BemSpace, i.e. it is provides as column vector of function values in the 
%> nodes.
%>
%> Likewise, application of the method returns the solution as a column
%> vector of function values in the nodes.
% ======================================================================
classdef DegenerateKernelApprox < BIEPack.BIEPackObject
    
    properties ( Constant )
        
        %> @brief name of the method used in requirement checking
        METHOD_NAME = 'DegenrateKernelApprox';
        
    end
    
    
    properties (SetAccess = protected)
        
        %> @brief integral operator with a continuous kernel
        op = [];
        
        %> @brief coefficient vector representing the right hand side of the
        %> integral equation.
        rhs = [];
        
        %> @brief matrix of the linear system
        A = [];
        
        %> @brief vector for the right hand side
        b = [];
        
    end
    
    methods
        
        %> @brief Construct the method using an integral operator and
        %> a right hand side.        
        %>
        %> @param op an operator suitable for application of the degenerate
        %>     kernel approximation method
        %> @param rhs vector of nodal values in @c op.defSpace.theMesh
        %> 
        function degKernelApprox = DegenerateKernelApprox( op, rhs )
                        
            if ( nargin > 0 )
                degKernelApprox.setOperator(op);
                degKernelApprox.setRhs(rhs);                
            end
            
        end
        
        
        %> @brief Construct the method using an integral operator and
        %> a right hand side.
        %>
        %> The method checks that the integral operator meets the
        %> requirements of the degenerate kernel approximation method.
        %>
        %> @param op the integral operator, instance of 
        %>    @link BIEPack.ops.DegenerateKernelOp DegenerateKernelOp  @endlink.               
        function setOperator( degKernelApprox, op )
                                    
            if ( ~ op.checkRequirements( degKernelApprox.METHOD_NAME ) )
                error('In DegenerateKernelApprox method setOperators: operator does not support degenerate kernel approximation method.');
            end
            
            degKernelApprox.op = op;
            
            % set new state for object
            degKernelApprox.rhs = [];
            degKernelApprox.A = [];
            degKernelApprox.b = [];
            
        end
        
        
        %> @brief Sets the right hand side vector for the problem.
        %>
        %> This method should be called after setOperator.
        %>
        %> @todo The method should check that rhs has the right dimentsion
        function setRhs( degKernelApprox, rhs )
            
            degKernelApprox.rhs = rhs;
            
        end
        
        
        %> @brief Assembles the linear system matrix for the given problem         
        function assembleMatrix( degKernelApprox )
            
            degKernelApprox.A = degKernelApprox.op.getImplementation(degKernelApprox.METHOD_NAME);
            degKernelApprox.b = degKernelApprox.A * degKernelApprox.rhs;
            
        end                

        
        %> @brief apply the method to the given operator and right hand side
        %>
        %> @retval the vector of nodal values of the solution
        function phi = apply( degKernelApprox )
            
            if ( isempty( degKernelApprox.op ) )
                error('In DegenerateKernelApprox method apply(): operator matrix is unset. ');
            elseif ( isempty( degKernelApprox.rhs ) )
                error('In DegenerateKernelApprox method apply(): right hand side vector is unset. ');
            end
            
            if ( isempty(degKernelApprox.A) )
                degKernelApprox.assembleMatrix;
            end
            
            N = degKernelApprox.op.defSpace.dimension;
            phi = degKernelApprox.rhs + (eye(N) - degKernelApprox.A) \ degKernelApprox.b;
            
        end
        
        
    end
end
