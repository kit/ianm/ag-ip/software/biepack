%> @file BIEPackConfig.m
%> @brief Provides the singleton BIEPackConfig object.
%>
%> Any BIEPack class or function requiring configured data, can call
%> BIEPackConfig.getInstance() to obtain the BIEPackConfig object. 

%> This script checks that paths are set correctly. If external packages
%> are used, it checks that paths are correctly provided
classdef (Sealed) BIEPackConfig < handle

   properties (SetAccess = private)

    % Obtain the path to this initialization file. Check that the path is on
    % the Matlab path.
    
    
    % For using Periodic Green's functions: The Faddeeva function is needed. 
    % Check that it can be found.
    %
    % If you want to use periodic Green's functions or waveguide functions,
    % install the Faddeeva package by Simon Chandler Wilde and Mohammad Al
    % Azah, from https://github.com/sms03snc/Faddeeva
    
    %> Is true if the Faddeeva package is available
    faddeeva_package = false;
    
    %> The path to the Faddeeva package.
    faddeeva_path = '';

   end

   methods (Access = private)
    
      function obj = BIEPackConfig()
          
          run('biepack_local_settings.m');
          
          if (exist('biepack_conf_faddeeva', 'var') == 1)
              obj.faddeeva_package = true;
              obj.faddeeva_path = biepack_conf_faddeeva;
              
              if (exist('Faddeeva', 'dir') == 0)
                  % The Faddeeva directiory is not yet in the path
                  addpath(obj.faddeeva_path);
              end
          end
      end
   
   end

   methods (Static)

       function biepackConfig = getInstance()

         persistent singleConfig

         if isempty(singleConfig) || ~isvalid(singleConfig)
            singleConfig = BIEPack.BIEPackConfig();
         end
         biepackConfig = singleConfig;

       end

   end
end
