%> @file OpsDoc.m
%> @brief File containing the documentation of the @a ops package
%
% ======================================================================
%
%> @namespace BIEPack::ops
%> @brief Classes representing discretized boundary integral operators
%>
%> In the view taken by BIEPack all operators map elements of a discrete
%> space (the <i>definition space</i>) to elements of some other discrete space 
%> (the <i>range space</i>). Both spaces are instances of classes in the
%> package @link BIEPack::spaces spaces @endlink.
%>
%> The main class in this package @c ops is GeneralOp. All other operator classes
%> are derived from this class. GeneralOp provides the properties @c defSpace
%> and @c rangeSpace and the two abstract methods @c checkRequirements()
%> and @c getImplementation() (see the @link BIEPack::methods documentation
%> of the methods package @endlink) for more details.
%>
%> Together with the class LinearCompOp, GeneralOp also provides the first 
%> layer of operator functionality in BIEPack: linear combinations of operators
%> may be formed.
%>
%> The second layer of functionality is formed by the <i>interface classes</i>.
%> Each interface class is derived from GeneralOp and provides the functionality
%> to apply a specific method for solving a boundary integral equation. This is
%> explained in more detail in the BIEPack::methods documentation of the methods package @endlink.
%> 
%> The third layer of functionality is formed by concrete implementations of
%> specific operators, such as LaplaceSLOp for the single layer operator associated
%> with the Laplace operater, HemholtzDLOp for the double layer operator associated 
%> with the Helmholtz operator and so on.
%
