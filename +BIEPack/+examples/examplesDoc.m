%> @file examplesDoc.m
%> @brief The documentation of the @a examples package
%
% ======================================================================
%
%> @namespace BIEPack::examples
%> @brief Example scripts and functions