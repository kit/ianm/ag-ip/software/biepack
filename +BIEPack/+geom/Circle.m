%> @file Circle.m
%> @brief Contains the geom.Circle class.
%
% ======================================================================
%
%> @brief Circular curve with center and radius. 
classdef Circle < BIEPack.geom.ParamCurve
    
    properties
        
        %> @brief 1st coordinate of the center of the circle
        %>
        %> This property is the original center coordinate before
        %> application of any affine transformations implemented in the
        %> ParamCurve class.
        c1 = 0;
        
        %> @brief 2nd coordinate of the center of the circle
        %>
        %> This property is the original center coordinate before
        %> application of any affine transformations implemented in the
        %> ParamCurve class.
        c2 = 0;
        
        %> @brief Radius of the circle
        %>
        %> This property is the circle radius before
        %> application of any affine transformations implemented in the
        %> ParamCurve class.
        r = 1;
        
    end
    
    methods
        
        %> @brief Constructs a Circle object from given center and radius.
        %>
        %> Center and radius are initialized as given. A random name
        %> starting with 'Circle_' is also set.
        %>
        %> @param c Vector containing the coordinates of the center.
        %> @param r Radius of the circle.
        %>
        %> @retval circle The newly constructed Circle object.
        function circle = Circle( c, r )
            
            circle.randomName;
            circle.name = strcat('Circle_',circle.name);
            circle.c1 = c(1);
            circle.c2 = c(2);
            circle.r = r;
            
            circle.startParam = -pi;
            circle.endParam = pi;
            
            circle.closed = true;
            circle.smoothness = inf;
            circle.globalSmoothness = inf;
            circle.highestImplementedDerivative = 2;
        end
        
        %> @brief Implementation of parametrization (cos(t), sin(t)) over 
        %> the interval [-pi,pi]
        %>
        %> Derivatives of the parametrization up to second order are
        %> implemented.
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. Same size as @c t
        %> @retval y2 2nd coordinate of the curve points. Same size as @c t
        function [y1, y2] = paramEval(circle, t, n)
            
            if ( n == 0 )
                y1 = circle.c1 + circle.r * cos(t);
                y2 = circle.c2 + circle.r * sin(t);
            elseif ( n == 1 )
                y1 = - circle.r * sin(t);
                y2 = circle.r * cos(t);
            else
                y1 = - circle.r * cos(t);
                y2 = - circle.r * sin(t);
            end
            
        end
        
        %> @brief Obtain the centre of the translated circle.
        %>
        %> Returns the center of the circle after application of the affine
        %> transform from the ParamCurve class.
        %>
        %> @retval c coordinates of the center as a column vector
        function c = getCenter( circle )
            
            c = circle.matrixA * [circle.c1; circle.c2] + circle.vectorB;
            
        end
        
    end
    
end

