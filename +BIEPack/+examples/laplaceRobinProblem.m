%> @file laplaceRobinProblem.m
%> @brief Contains the laplaceRobinProblem() example function.
%
% ======================================================================
%
%> @brief Example solving a Robin boundary value problem for Laplace's
%> equation.
%>
%> This example function computes the solution to a Robin problem for
%> Laplace's equation,
%>
%> @f[
%>    \Delta u = 0  \quad \text{in } D \, , \qquad
%>    \frac{\partial u}{\partial n} + u = f \quad \text{on } \partial D
%> @f]
%>
%> An ansatz for @f$u@f$ as a single layer potential @f$u = \operatorname{SL} 
%> \varphi @f$ is chosen which gives the boundary integral equation
%>
%> @f[
%>   \varphi + 2 K^* \varphi + 2 S \varphi = 2 f
%> @f]
%> 
%> Here @f$K^*@f$ denotes the adjoint double layer operator and @f$S@f$ the
%> single layer operator
%>
%> This equation is solved via the Nyström method using the composite
%> trapezoidal rule for smooth integrals and a rule based on interpolation by
%> trigonometric interpolation for the logarithmic singularities.
%>
%> @param N half the number of quadrature points to be used. The default value is 32.
%>
%> @todo introduce a parameter for the method to be used
%>
function laplaceRobinProblem(N, method)

% If N is not given as an argument, set it to a default value.
if ( nargin == 0 )
    N = 32;
end   
if ( nargin < 2 )
    method = 'nystroem';
end

% import the BIEPack package
import BIEPack.*;

% construct a mesh on the curve
curve = geom.Kite;
mesh = meshes.ParamCurveMesh(curve,N);

% Construct the correct function space to use for the method
if ( strcmp( method, 'nystroem' ) )
    
    % construct a space of trigonometric polynomials
    space = spaces.TrigPol( mesh );
    pointX = mesh.nodesX.';
    pointY = mesh.nodesY.';
    [nX,nY] = mesh.theCurve.normal(mesh.t);
    nnorm = sqrt( nX.^2 + nY.^2 );
    pointNX = ( nX ./ nnorm ).';
    pointNY = ( nY ./ nnorm ).';
    
elseif  ( strcmp( method, 'collocLinear' ) )
    
    % construct a space of piecewise linear functions
    space = spaces.BemSpace( mesh, spaces.BemSpace.PIECEWISE_LINEAR );
    pointX = space.collocPoints.X;
    pointY = space.collocPoints.Y;
    pointNX = space.collocPoints.nX;
    pointNY = space.collocPoints.nY;
    
elseif  ( strcmp( method, 'collocConst' ) )
    
    % construct a space of piecewise constant functions
    space = spaces.BemSpace( mesh, spaces.BemSpace.PIECEWISE_CONST );
    pointX = space.collocPoints.X;
    pointY = space.collocPoints.Y;
    pointNX = space.collocPoints.nX;
    pointNY = space.collocPoints.nY;
    
else    
    
    error('The method %s is not supported.', method);
    
end
    
% define the two boundary operators
S = ops.LaplaceSLOp( space, space );
Kstar = ops.LaplaceADLOp( space, space );

% define the operator A on the lefthand side of the equation (I - A) phi = f
theOp = -2*Kstar + (-2)*S;

% evaluate the function f on the curve for u = x_1^2 - x_2^2
u = ( pointX.^2 - pointY.^2 );
dudn = 2 * ( pointNX .* pointX - pointNY .* pointY );
f = dudn + u;

% define the method
if ( strcmp( method, 'nystroem' ) )
    theMethod = methods.NystroemPeriodicLog(theOp,2*f);
else
    % for collocation methods, the identity operator needs to be added
    theOp = ops.IdentityOp(space) + (-1)*theOp;
    theMethod = methods.Collocation(theOp,2*f);
end

% compute the solution
phi = theMethod.apply;

figure(1);
plot(mesh.t,phi);
title('Density on the Boundary');

% Apply the single layer operator to phi to obain the approximation to the 
% function u on the boundary. Compute and plot the error.
if ( strcmp( method, 'nystroem' ) )
    S_disc = S.getPeriodicLogImplementation;
else
    S_disc = S.getCollocationImplementation( theMethod.quadRules );
end

u_approx = S_disc * phi;

figure(2);
semilogy(mesh.t,abs(u - u_approx));
title('Error in Dirichlet Boundary Values');

% evaluate the solution in points in the domain
% TODO make just one fixed mesh indpendent of N
[theta, rho ] = cart2pol( mesh.nodesX, mesh.nodesY );
theta = [theta theta(1)];
rho = [rho rho(1)];
r_N = floor(length(theta)/2);
[ T, Rho ] = meshgrid( theta, linspace(0,1,r_N) );
r = 0.85*rho;
X = ( ones(r_N,1) * r ) .* Rho .* cos(T);
Y = ( ones(r_N,1) * r ) .* Rho .* sin(T);

slp = ops.LaplaceSLPot( space );
u_dom = slp.evalPotential(X,Y,phi);
u_domcor = X.^2 - Y.^2;

figure(3);
surf( X, Y, u_dom ); shading interp;
title('Solution of BVP in the domain');

figure(4);
surf( X, Y, log(abs(u_dom - u_domcor) ) ); shading interp; colorbar
title('Logarithm of the Error in the Solution to the BVP');

end

