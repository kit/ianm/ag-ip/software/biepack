function collocationLogOp(N)

% import the BIEPack package
import BIEPack.*;

% construct a mesh on the curve
curve = geom.Kite;
mesh = meshes.ParamCurveMesh(curve,N);

theSpace = spaces.BemSpace( mesh, spaces.BemSpace.PIECEWISE_LINEAR );

% define the two boundary operators
S = ops.LaplaceSLOp( theSpace, theSpace );
AD = ops.LaplaceADLOp( theSpace, theSpace );

% define the complete operator on the lefthand side of the equation
theOp = 2*AD + 2*S;

% evaluate the function f on the curve for u = x_1^2 - x_2^2
u = ( mesh.nodesX.^2 - mesh.nodesY.^2 ).';
[xip,etap] = curve.eval(mesh.t,1);
dudn = ( 2 * ( etap.*mesh.nodesX + xip.*mesh.nodesY ) ./ sqrt( xip.^2 + etap.^2 ) ).';
psi = dudn + u;

theMethod = methods.Collocation( theOp, psi );

% solve the problem
phi = theMethod.apply();

figure
plot(mesh.t,phi);