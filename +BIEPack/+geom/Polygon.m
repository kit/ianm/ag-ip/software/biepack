classdef Polygon < BIEPack.geom.PiecewiseCurve
    % Polygon A piecewise linear boundary curve 
    %
    % This class represents a general polygon which is formed by straight
    % lines connecting given vertices.
    
    properties
    end
    
    methods
        
        % Creates the polygon by specification of the corner points. 
        % 
        % The corner points may be either specified as a list of points,
        % each a column vector with two rows, or as a single matrix with
        % the x-coordinates on the first and the y-coordinates on the
        % second row.
        function polygon = Polygon( varargin ) 
            
            % Transform input to a single matrix and add first point at the
            % end
            if ( nargin == 1 )
                coordMatrix = [ varargin{1}, varargin{1}(:,1) ];
            else
                coordMatrix = zeros(2,nargin+1);
                for j=1:nargin
                    coordMatrix(:,j) = varargin{j};
                end
                coordMatrix(:,end) = varargin{1};
                
            end
            
            numLines = size(coordMatrix,2) - 1;
            
            % create a cell array that is the input for the PiecewiseCurve
            % constructor
            pcArray = cell(2,numLines);
            for j=1:numLines
                pcArray{1,j} = BIEPack.geom.StraightLine( ...
                    [ coordMatrix(1,j); coordMatrix(2,j) ], [ coordMatrix(1,j+1); coordMatrix(2,j+1) ] );
                pcArray{2,j} = 1;
            end
            
            % Glue all the StraightLine objects together
            polygon@BIEPack.geom.PiecewiseCurve( pcArray{:} );
                        
            % Create a random name starting with 'Polygon'
            polygon.randomName;
            polygon.name = strcat('Polygon_',polygon.name);
            
            polygon.closed = true;
            
        end
        
    end
    
end

