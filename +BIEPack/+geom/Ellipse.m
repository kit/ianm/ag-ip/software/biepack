%> @file Ellipse.m
%> @brief Contains the geom.Ellipse class.
%
% ======================================================================
%
%> @brief Ellipsoidal boundary curve
classdef Ellipse < BIEPack.geom.ParamCurve
    
    properties
        
        %> @brief 1st coordinate of the center of the ellipse.
        %> This property is the original center coordinate before 
        %> application of any affine transformations implemented in the 
        %> ParamCurve class. 
        c1 = 0;
        
        %> @brief 2nd coordinate of the center of the ellipse.
        %> This property is the original center coordinate before 
        %> application of any affine transformations implemented in the 
        %> ParamCurve class. 
        c2 = 0;
        
        
        %> @brief Length of the half-axe in the initial x1-direction.
        %> This property is the half-axe length before 
        %> application of any affine transformations implemented in the 
        %> ParamCurve class.
        a = 1;
        
        %> @brief Length of the half-axe in the initial x2-direction.
        %> This property is the half-axe length before 
        %> application of any affine transformations implemented in the 
        %> ParamCurve class.
        b = 0.5;
        
    end
    
    methods
        
        %> @brief Constructs an Ellipse object from given center and
        %> half-axe lengths.
        %> Center and half-axe lengths are initialized as given. A random
        %> name starting with 'Ellipse_' is also set.
        %>
        %> @param c Vector containing the coordinates of the center.
        %> @param a Length of the half-axe in the initial x1-direction.
        %> @param b Length of the half-axe in the initial x2-direction.
        %>
        %> @retval ellipse The newly constructed Ellipse object.
        function ellipse = Ellipse( c, a, b )
            
            ellipse.randomName;
            ellipse.name = strcat('Ellipse_',ellipse.name);
            ellipse.c1 = c(1);
            ellipse.c2 = c(2);
            ellipse.a = a;
            ellipse.b = b;
            
            ellipse.startParam = -pi;
            ellipse.endParam = pi;
            
            ellipse.closed = true;
            ellipse.smoothness = inf;
            ellipse.globalSmoothness = inf;
            ellipse.highestImplementedDerivative = 2;
        end
        
        %> @brief Implementation of parametrization (a * cos(t),  b * sin(t)) over 
        %> the interval [-pi,pi].
        %>
        %> Derivatives of the parametrization up to second order are
        %> implemented.
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. 
        %> @retval y2 2nd coordinate of the curve points.
        function [y1, y2] = paramEval(ellipse, t, n)
            
            if ( n == 0 )
                y1 = ellipse.c1 + ellipse.a * cos(t);
                y2 = ellipse.c2 + ellipse.b * sin(t);
            elseif ( n == 1 )
                y1 = - ellipse.a * sin(t);
                y2 = ellipse.b * cos(t);
            else
                y1 = - ellipse.a * cos(t);
                y2 = - ellipse.b * sin(t);
            end
            
        end
        
        
        %> @brief Obtain the center of the translated ellipse.
        %>
        %> Returns the center of the ellipse after application of the affine
        %> transformation from the ParamCurve class.
        %>
        %> @retval c Coordinates of the center as a column vector.
        function c = getCenter( ellipse )
            
            c = ellipse.matrixA * [ellipse.c1; ellipse.c2] + ellipse.vectorB;
            
        end
        
    end
    
end

