%> @file BIEPackDoc.m
%> @brief File containing the main documentation pages of BIEPack.
%
% ======================================================================
%
%> @mainpage The BIEPack Package for Boundary Integral Equations
%>
%> BIEPack is a software package for %Matlab that uses boundary integral
%> equations for solving boundary value problems in 2D. The functionality
%> includes the definition of the geometry, mesh generation and setting up
%> the discrete spaces, the formulation of the integral equation and
%> the application of various numerical methods for its solution.
%>
%> The basic design principle of BIEPack is to use object oriented
%> programming to seperate as much as possible the definition of the
%> mathematical problem from the actual numerical method that is
%> used to compute the solution. The goal is: It should be as easy as possible
%> for the user to solve an integral equation by different numerical methods 
%> and to compare the results. 
%>
%> BIEPack was primarily designed for teaching purposes. Students on a course in 
%> numerical methods for integral equations or beginning graduate students 
%> should be enabled to solve non-trivial problems by building on an easy
%> to use boundary integral equation toolbox. However, the routines have
%> also been used for producing computational examples in research papers.
%>
%> The documentation of BIEPack is organized as follows:
%> - @link InstallationDoc Installation and use @endlink
%> - @link GetStartedDoc A detailed example for getting started @endlink
%> - @link PackagesDoc A description of the BIEPack packages @endlink
%>
%> To further learn about the library, have a look at the package
%> descriptions and at the examples listed in the @link BIEPack.examples
%> examples package description. @endlink
% ======================================================================
%
%> @page InstallationDoc Installation and Use
%>
%> @section InstallSec Installation
%>
%> The files of BIEPack can be obtained via @a git 
%> <a href="">https://git.scc.kit.edu/ianmip-software/biepack</a> or in a 
%> compressed archive file. In either case, simply put all files in any 
%> desired folder on your filesystem, which we will refer to as 
%> @a biepackroot below. 
%>
%> In order for %Matlab to find a BIEPack class or file, the folder 
%> @a biepackroot needs to be in the %Matlab path.
%>
%> Some optional functionality depends on external software. This software
%> needs to be installed separately on your system. You can then copy the
%> template file biepack_local_settings.m.dist to biepack_local_settings.m
%> and modify this file to provide the necessary configuration data for
%> these packages.
%>
%> @section UseSec Use
%>
%> BIEPack is organized in %Matlab packages. Your refer to a BIEPack class by
%>
%> BIEPack.PackageName.ClassName
%>
%> Thus to create an object of class @link BIEPack.geom.Circle Circle @endlink 
%> from the package @link BIEPack.geom geom @endlink, of 
%> radius 1 centred at the origin, write
%> @code
%> c = BIEPack.geom.Circle( [0; 0], 1 );
%> @endcode
%>
%> This maybe abbreviated by using the %Matlab @c import function. To
%> import all BIEPack packages put
%> @code
%> import BIEPack.*;
%> @endcode
%> near the beginning of your program. To create an instance of the class
%> BIEPack.geom.Circle you may then write
%> @code
%> c = geom.Circle( [0;0], 1 );
%> @endcode
%>
%> Next topic: @ref GetStartedDoc
%
% ======================================================================
%
%> @page GetStartedDoc Getting Started
%>
%> As a starting example, we show how to solve a boundary value problem for
%> Laplace's equation via a boundary integral equation using BIEPack. The
%> full code example is in the file laplaceRobinProblem.m in the @link
%> BIEPack.examples examples @endlink package.
%>
%> Consider the Robin boundary value problem for
%> Laplace's equation in a bounded smooth domain @f$D \subseteq
%> \mathbb{R}^2@f$,
%> @f[
%>     \Delta u = 0 \qquad \text{in } D \, , \qquad \frac{\partial
%>      u}{\partial n} + u = f \qquad \text{on } \partial D \, .
%> @f]
%> Making a single layer potential ansatz for the solution @f$u =
%> \operatorname{SL} \varphi@f$ leads to the integral equation
%> @f[
%>    \varphi + 2 \tilde{K} \varphi + 2 S \varphi = 2 f \qquad \text{on }
%>    \partial D \, .
%> @f]
%> Here @f$\tilde{K}@f$ denotes the adjoint of the double-layer operator
%> and @f$S@f$ the single layer operator. We remark that this boundary
%> integral equation is not well-posed if the single layer operator for
%> @f$ D @f$ is not injective. This is the case, for example, if @f$ D @f$ is the
%> unit circle, which can be easily tested with BIEPack.
%>
%> The example in laplaceRobinProblem.m is a function that solves the above integral 
%> equation via a Nyström method. The function takes an optional argument 
%> @a N that specifies the number of quadrature points to be used in the
%> quadrature rules. The quadrature rules are based on interpolation by
%> trigonometric polynomials. If @a N is not specified, the default value
%> of 32 is used.
%>
%> @code
%>    function laplaceRobinProblem(N)
%>   
%>    if ( nargin == 0 )
%>        N = 32;
%>    end
%> @endcode
%>
%> Next, we import all BIEPack packages for easier access.
%>
%> @code
%>    import BIEPack.*;
%> @endcode
%>
%> The next steps are the creation of the geometry, the mesh and the
%> discrete space in which to solve the integral equation. As the domain, we
%> may choose any domain bounded by a smooth curve, provided the single layer 
%> operator is injective, as remarked above. We chosse the well-known kite 
%> shape from the Colton/Kress books. On it we define a mesh with @a N points that are
%> distributed uniformly in parameter space. The @link BIEPack.meshes.ParamCurveMesh
%> ParamCurveMesh @endlink class provides just that. Finally, we create a
%> space of trigonometric polynomials on this mesh (actually, the pull-back
%> onto the parameter intervall of the functions in this space are
%> trigonometric polynomials). The class @link BIEPack.spaces.TrigPol TrigPol
%> @endlink provides this.
%>
%> @code
%>    curve = geom.Kite;
%>    mesh = meshes.ParamCurveMesh(curve,N);
%>    trigPols = spaces.TrigPol( mesh );
%> @endcode
%>
%> Now we can define the integral operators. The standard boundary
%> operators for Laplace's equation are provided by the library. We only need
%> to create the correct linear combination. As the class implementing the
%> Nyström method solves an equation @f$ (I - A) \varphi = \psi @f$ and we
%> have to provide @f$A@f$, a minus sign is introduced.
%>
%> @code
%>    S = ops.LaplaceSLOp( trigPols, trigPols );
%>    AD = ops.LaplaceADLOp( trigPols, trigPols );
%>    theOp = -2*AD + (-2)*S;
%> @endcode
%>
%> The next step is to provide the right hand side in the integral
%> equation. We choose @f to correspond to the Robin boundary values of the
%> harmonic function @f$u(x,y) = x^2 - y^2@f$. To compute the normal
%> derivative, we require the first derivatives of the parametrization,
%> which we store in @c xip and @c etap respectively.
%>
%> @code
%>    u = ( mesh.nodesX.^2 - mesh.nodesY.^2 ).';
%>    [xip,etap] = curve.eval(mesh.t,1);
%>    dudn = ( 2 * ( etap.*mesh.nodesX + xip.*mesh.nodesY ) ./ sqrt( xip.^2 + etap.^2 ) ).';
%>    f = dudn + u;
%> @endcode
%>
%> Finally, we have everything to define and apply the numerical method:
%>
%> @code
%>    nystroemMethod = BIEPack.methods.NystroemPeriodicLog(theOp,2*f);
%>    phi = nystroemMethod.apply;
%> @endcode
%> 
%> The function in laplaceRobinProblem.m provides various plots of the
%> solution of the integral equation as well as of the solution of the
%> boundary value problem. We here just show how to plot @f$\varphi@f$ and
%> the error between the error between the exact and approximated solutions
%> on @f$\partial D@f$. To obtain the boundary values, we need to apply the
%> single layer operator to the computed density. The operator easily
%> provides us with the corresponding matrix.
%>
%> @code
%>    figure
%>    plot(mesh.t,phi);
%>    S_disc = S.getPeriodicLogImplementation;
%>    u_approx = S_disc * phi;
%>    
%>    figure(2);
%>    semilogy(mesh.t,abs(u - u_approx));
%> @endcode
%>
%>
%> Next topic: @ref PackagesDoc
%
% ======================================================================
%
%> @page PackagesDoc The BIEPack Packages
%>
%> BIEPack is organised in a number of packages which are detailed below. %Matlab 
%> packages will also be referred to as @a namespaces in this documentation.
%>
%> The @a biepackroot directory contains the directory <tt>+BIEPack/</tt>
%> which corresponds to the %Matlab package @a BIEPack. The individual 
%> BIEPack packages are subpackages contained in @a BIEPack.
%>
%> All classes in BIEPack are derived from the basic class
%> BIEPack.BIEPackObject which provides basic
%> functionality shared by all classes. All other classes and functions are
%> contained in one of the following subpackages:
%> - @link BIEPack.geom geom @endlink Classes representing geometrical objects such as curves.
%> - @link BIEPack.meshes meshes @endlink Classes representing meshes defined on the geometrical objects.
%> - @link BIEPack.spaces spaces @endlink Classes representing discrete spaces.
%> - @link BIEPack.ops ops @endlink Classes representing discretized integral operators
%>         which map between objects from the @a spaces package.
%> - @link BIEPack.methods methods @endlink Numerical methods used for solving
%>         integral equations.
%> - @link BIEPack.utils utils @endlink Utility functions and classes.
%> - @link BIEPack.examples examples @endlink Example scripts and functions 
%>         showing how to use the library.
%>
%> Back to the main page: @ref mainpage
%
% ======================================================================

