classdef LaplaceDdnSLPot < BIEPack.ops.PeriodicSmoothOp
    % BIEPack.ops.LaplaceDdnSLPot < BIEPack.ops.PeriodicSmoothOp
    % 
    % Normal derivative of the single layer potential for the Laplace
    % operator evaluated on a curve D away from the curve of integration C,
    %
    % d/dn (S phi) (x) = 1/(2pi) \int_C n(x) . (y - x)/|x - y|^2 phi(y) ds(y), x \in D
    %
    % Implementations as an integral operator include:
    %  - over a smooth closed curve C mapping a space of trigonometric
    %    polynomials (BIEPack.spaces.TrigPol) into a space of trigonometric 
    %    polynomials defined on another smooth closed curve D.
    
    properties
    end
    
    methods
        
                
        % Construct the operator
        %
        % Just passes on the call to the super class constructor. Checks
        % whether the spaces are compatible with this type of operator are
        % carried out by the checkRequirements function
        function ddnSl = LaplaceDdnSLPot( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            ddnSl@BIEPack.ops.PeriodicSmoothOp(defSpace,rangeSpace);
            
            ddnSl.randomName;
            ddnSl.name = strcat('LaplaceSLPot_',ddnSl.name);
            
        end
        
        % Evaluation of the functions defining the kernel. Implementation
        % of the function inherited from BIEPack.ops.PeriodicSmoothOp
        function K = evalPeriodicSmoothKernel(intOp,s,t)
                        
            defCurve = intOp.defSpace.theCurve;
            rangeCurve = intOp.rangeSpace.theCurve;
            
            s_orig = int_op.rangeSpace.theMesh.s;
            [xi_s, eta_s] = rangeCurve.eval(s_orig,0);            
            [xip_s, etap_s] = rangeCurve.eval(s_orig,1);
            % scale for parametrization over [0,2*pi]
            xip_s = intOp.rangeSpace.L/(2*pi) * xip_s;
            etap_s = intOp.rangeSpace.L/(2*pi) * etap_s;
            
            t_orig = intOp.defSpace.theMesh.t;
            xi_t = intOp.defSpace.theMesh.nodesX;
            eta_t = intOp.defSpace.theMesh.nodesY;
            
            [xip_t, etap_t] = defCurve.eval(t_orig,1);
            % scale for parametrization over [0,2*pi]
            xip_t = intOp.rangeSpace.L/(2*pi) * xip_t;
            etap_t = intOp.rangeSpace.L/(2*pi) * etap_t;
            
            M = length(s);
            N = length(t);
            ones_h = ones(1,N);
            ones_v = ones(M,1);
            
            norm_xp_s = sqrt( xip_s.^2 + etap_s.^2 );
            norm_xp_t = sqrt( xip_t.^2 + etap_t.^2 );

            X1 = xi_s * ones_h - ones_v * xi_t;
            X2 = eta_s * ones_h - ones_v * eta_t;
            R = sqrt( X1.^2 + X2.^2 );
            
            K = 1/(2*pi) * ( xip_s * ones_h .* X2 - etap_s * ones_h .* X1 ) ./ R.^2 ...
                ./ ( norm_xp_s * ones_h ) .* ( ones_v * norm_xp_t );
            
        end
                
    end
    
end