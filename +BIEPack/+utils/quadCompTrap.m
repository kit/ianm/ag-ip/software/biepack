%> @file quadCompTrap.m
%> @brief The quadCompTrap() function.
% ======================================================================
%> @brief Computation of the points and weights of the composite trapeoidal
%> quadrature rule for a general interval [a,b]
%>
%> The function takes arguments @a N, @a a, @a b, where @a a, @a b are the
%> interval end points and a @a N+1 point rule is computed. The function
%> returns row vectors @a x and @a w of equal length containing the
%> quadrature points and weights, respectively.
% ======================================================================
function [x,w] = quadCompTrap(N,a,b)

x = linspace(a,b,N+1);
w = (b-a) / N * ones(size(x));

w(1) = w(1) / 2;
w(N+1) = w(N+1) / 2;
