%> @file DegenerateKernelOp.m
%> @brief File containing the @link BIEPack.ops.DegenrateKernelOp DegenerateKernelOp @endlink class.
% ======================================================================
%> @brief An integral operator to which the degenerate kernel approximation
%> method can be applied.
%>
%> The requirements for such an operator are
%>   - it has a continuous kernel,
%>   - it is defined on a @link BIEPack.spaces.BemSpace BemSpace @endlink of 
%>     continuous piecewise linear functions,
%>   - the range space is the same as the space of definition,
%>   - the BemSpace is based on a @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink.
%>
%> The implementation of such an operator represents the
%> discretization obtained using a degenerate kernel
%> approximation through interpolation with continuous piecewise linear 
%> functions on the parameter intervall.
%>
%> The purpose of providing an implementation of this method is purely
%> educational: The degenerate kernel approximation is an example of a
%> numerical method that can be analysed using only the perturbation lemma
%> (Neumann series). The last restriction in (ParamCurveMesh) in particular
%> is just there to keep the implementation as simple as possible.
% ======================================================================
classdef DegenerateKernelOp < BIEPack.ops.GeneralOp
        
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. The 
        %> restrictions on the parameters listed below have to be satisfied for an
        %> operator used in a @link BIEPack.method.DegenerateKernelApprox 
        %> DegenerateKernelApprox @endlink method. Checks whether this is actually
        %> the case are carried out by the checkRequirements function of
        %> the method.
        %>
        %> @param defSpace a @link BIEPack.spaces.BemSpace BemSpace @endlink 
        %>     of piecewise linear continuous functions, based on a 
        %>     @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink
        %> @param rangeSpace identical to @a defSpace
        function degKernelOp = DegenerateKernelOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            degKernelOp@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
            degKernelOp.randomName;
            degKernelOp.name = strcat('DegenerateKernelOp_',degKernelOp.name);
            
        end
        
        %> @brief Compute a matrix that represents a degenerate kernel 
        %> approximation of the integral operator. 
        %>
        %> For the degenerate kernel approximation, the discretization
        %> consists of the product of two matrices: The first simply
        %> contains evaluations of the kernel in the nodes, the second
        %> contains scalar products of the Lagrange basis functions.
        %>
        %> @retval A the matrix representation of the operator
        function A = getDegenerateKernelImplementation(degKernelOp)
            
            % the mesh we are using
            mesh = degKernelOp.defSpace.theMesh;
            
            % obtain the parameter values of the nodes of the mesh
            t = mesh.t;
            
            % Evaluate the kernel function. Additionally, we have to
            % multiply with a term from inserting the parametrization of
            % the curve
            mesh.evalDerivs(1);
            C = degKernelOp.evalContinuousKernel( t.', t ) ...
                * diag( sqrt( mesh.xDeriv.^2 + mesh.yDeriv.^2 ) );
            
            % Assemble the matrix of scalar products of the Lagrange basis
            % functions element by element. For each element, an integral
            % on the reference element needs to be computed.
            D = zeros( mesh.numNodes );            
            h =  mesh.endParameter - mesh.startParameter;
            for j=1:mesh.numElements
                nodeIndeces = degKernelOp.defSpace.localToGlobalDOFMap(j).globalDOF;
                D(nodeIndeces,nodeIndeces) = D(nodeIndeces,nodeIndeces) ...
                    + h(j)/6 * [ 2  1; 1, 2 ];
            end
            
            A = C * D;
            
        end
        
        %> @brief Checks whether the geometry, mesh and spaces are such that this
        %> operator may be used in a degenerate kernel approximation method.
        %>
        %> Requirements are that the definition and range spaces are equal
        %> and that they are a @link BemSpace BIEPack.spaces.BemSpace @endlink
        %> instance using only piecewise linear functions. The space has to
        %> be based on a @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink
        %>
        %> @retval isOk True if the operator meets the requirements.
        %>
        %> @todo properties of functions in space are not checked.
        function isOk = checkRequirementsForDegenerateKernelOp(degKernelOp)
                        
            if ( ~ BIEPack.utils.derivedFrom( degKernelOp.defSpace, 'BIEPack.spaces.BemSpace' ) )
                isOk = false;
                return
            end
            
            if ( degKernelOp.defSpace ~= degKernelOp.rangeSpace )
                isOk = false;
                return
            end       
            
            if ( ~ BIEPack.utils.derivedFrom( degKernelOp.defSpace.theMesh, 'BIEPack.meshes.ParamCurveMesh' ) )
                isOk = false;
                return
            end
            
            isOk = true;
            
        end
        
    end
    
    
    methods ( Abstract )

        %> @brief Evaluate the kernel function which is approximated by a degenerate 
        %> kernel. 
        %>
        %> @param t column vector of parameter values on the curve
        %> @param tau row vector of parameter values on the curve
        %>
        %> @retval K matrix of size length(t) x length(tau)
        %> containing the corresponding values of the kernel.
        K = evalContinuousKernel( degKernelOp, t, tau );        
        
    end
    
end
