%> @file collocationOneCorner.m
%> @brief skript for testing convergence rates of a collocation method

clear all
import BIEPack.*;

% define a domain with one corner and a BemMesh on it
alpha = 3/2;
curve = geom.OneCorner( pi/alpha, 1 );

% define a series of meshes and spaces
numMeshes = 8;
mesh{numMeshes} = [];
theSpace{numMeshes} = [];

%mesh{1} = meshes.BemMesh( curve, 0.4 );
mesh{1} = meshes.ParamCurveMesh( curve, 20 );

for j = 2:numMeshes
    mesh{j} = mesh{j-1}.uniformRefinement;
end

% solve the problems
phi{numMeshes} = [];

for j = 1:numMeshes
    theSpace{j} = spaces.BemSpace( mesh{j}, spaces.BemSpace.PIECEWISE_LINEAR );
    %theSpace{j} = spaces.BemSpace( mesh{j}, spaces.BemSpace.PIECEWISE_CONST );

    intOp = ops.IdentityOp(theSpace{j}) + (-2) * ops.LaplaceDLOp(theSpace{j}, theSpace{j});

     % definition of right-hand side
    [theta, r] = cart2pol(theSpace{j}.collocPoints.X, theSpace{j}.collocPoints.Y);
    psi = -2*r.^(alpha) .* cos(alpha*theta);

    theMethod = methods.Collocation( intOp, psi );

    % solve the problem
    phi{j} = theMethod.apply();
    
    % compute error in coarsest mesh points
    if ( j==1 )
%        t_restrict = t;
        phi_restrict{j} = phi{1};
    else
        rel = spaces.BemSpaceRelation( theSpace{1}, theSpace{j} );
        phi_restrict{j} = rel.restrict( phi{j} );
    end
    
end

e = zeros(numMeshes-1,1);
for j=1:numMeshes-1       
    
    e(j) = max( abs( phi_restrict{j} - phi_restrict{numMeshes} ) );
    
    fprintf(' %4d   %e   ', theSpace{j}.dimension, e(j) );
    if (j > 1)
        fprintf(' %f', log( e(j-1) ./ e(j) ) / log(2) );
    end
    fprintf('\n');
  
end

%hold off

%figure, plot( t,phi,t, exp(cos(t)) .* cos(sin(t)) )
%fprintf('Error %e\n\n',max(abs(phi - exp(cos(t)) .* cos(sin(t)) ) ) )
%
%fprintf('\n\n');
