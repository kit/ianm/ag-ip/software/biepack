%> @file PeriodicLogOp.m
%> @brief File containing the BIEPack.ops.PeriodicLogOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief Class representing an integral operator with a periodically
%> repeated logrithmic singularity.
%
%> The class represents the discretization of an integral operator defined 
%> on spaces of 2pi-periodic functions on the real line. The kernel is of 
%> the form
%>
%>  1/(2pi) log( 4 sin^2( (s-t) / 2 ) ) K_1(s,t) + K_2(s,t)
%>
%> with K_1, K_2 being smooth and 2pi-periodic with respect to both
%> arguments.
%>
%> The discrete definition and range spaces must be equal and specifically
%> a TrigPol instance. The discrete operator is obtained by applying 
%> quadrature rules to the integrals: the composite trapeoidal rule for the 
%> containing K_2 and a product quadrature for the integral containing the
%> logarithmic singularity. The quadrature points are -pi + j pi / N, 
%> j = 0, ..., 2N-1. In this way, the operator can be applied in a Nyström
%> or a collocation method for trigonometric polynomials.
%>
%> This abstract class may be implemented by any integral operator
%> defined on a closed smooth curve and mapping a TrigPol space on this 
%> curve on itself. The class uses implementations of K_1 and K_2 to 
%> provide a discrete realization of the operator. For a parameter interval
%> of length 2pi, the quadrature points correspond exactly to the points
%> t in the underlying @link ParamCurveMesh BIEPack.meshes.ParamCurveMesh. @endlink
%>
%> A general smooth closed curve may have a different length L of the
%> parameter interval than 2pi. In this case, the appropriate scaling of
%> the derivatives of the parametrization must be taken care of in
%> implementations of K_1, K_2.
% ======================================================================
classdef PeriodicLogOp < BIEPack.ops.GeneralOp
    % BIEPack.ops.PeriodicLogOp < BIEPack.ops.GeneralOp    
    
    methods 
        
        %> @brief Construct the operator
        %
        %> The function just passes on the call to the super class constructor. 
        %> Checks whether the spaces are compatible with a PeriodicLogOp are 
        %> carried out by the checkRequirementsForPeriodicLog() function.        
        function perLogOp = PeriodicLogOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            perLogOp@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
            perLogOp.randomName;
            perLogOp.name = strcat('PeriodicLogOp_',perLogOp.name);
            
        end
        
        %> @brief Compute the discretization of the operator.
        %
        %> The implementation uses a product integration rule for the
        %> logarithmic singularity and the composite trapezoidal
        %
        %> @todo Should the weights be computed here or rather in the
        %> method? They may be the same for many operators.
        function A = getPeriodicLogImplementation(perLogOp)
            
            % The grid has 2N+1 euqidistant points, function values on the
            % first and last coincide. These are the same as the mesh
            % points of the underlying ParamCurveMesh, but so cheap to
            % compute that we just do it here.
            N = perLogOp.defSpace.N;
            t = linspace(0,2*pi,2*N+1);
            t = t(1:2*N);
            
            % evaluate the kernels
            [K1, K2] = perLogOp.evalPeriodicLogKernels();
            
            % weights for log sin part
            R = (-1).^(0:2*N-1) / (2*N);
            for l = 1:N-1
                R = R + 1/l * cos( l * t );
            end
            R = -1/N * R;
            W1 = toeplitz(R);

            % weights for smooth part (composite trapezoidal rule)
            W2 = pi/N;

            % Assemble matrix
            A = W1 .* K1 + W2 * K2;
            
        end
        
        %> @brief Checks whether the geometry, mesh and spaces are such that 
        %> this operator may be used in a method that uses a PeriodicLogOp
        %> object.
        %
        %> The following conditions must be met:
        %>  - The defSpace must be a TrigPol space
        %>  - defSpace and rangeSpace must be equal
        %>  - The operator must be defined on a closed curve which has global
        %>    smoothness at least 2
        %
        %> @todo add that the curve might be a periodically extensible smooth 
        %> i.e. support for scattering by periodic sufaces
        function isOk = checkRequirementsForPeriodicLogOp(perLogOp)
            
            if ( ~ BIEPack.utils.derivedFrom( perLogOp.defSpace, 'BIEPack.spaces.TrigPol' ) )
                isOk = false;
                % warning('In PeriodicLogOp method checkRequirements: definition space is not BIEPack.spaces.TrigPol');
                return
            end
            
            if ( perLogOp.defSpace ~= perLogOp.rangeSpace )
                isOk = false;
                % warning('In PeriodicLogOp method checkRequirements: definition space is not equal to range space.');
                return
            end
            
            curve = perLogOp.defSpace.theCurve;
            
            if ( ~ curve.closed )
                isOk = false;
                % warning('In PeriodicLogOp method checkRequirements: curve the operator is defined on is not closed.');
                return
            end
            
            if ( curve.globalSmoothness < 2 )
                isOk = false;
                % warning('In PeriodicLogOp method checkRequirements: parametrization of curve is not twice continuously differentiable.');
                return
            end
            
            isOk = true;
            
        end
        
    end
    
    
    methods ( Abstract )
        
        %> @brief Evaluate the kernel functions K_1 and K_2 in the quadrature points.
        %
        %> The function evaluates K_j(t_k,t_l), j=1,2, for the quadrature
        %> points t_k, k= 0,...,2N-1. These correspond to the nodes of the
        %> underlying ParamCurveMesh scaled to the interval [-pi,pi].
        %
        %> Note that this class assumes that the parameter interval is
        %> [-pi,pi]. If the underlying curve uses a different parameter
        %> interval, the implementation of this function must take care of
        %> the rescaling.
        [K1, K2] = evalPeriodicLogKernels( perLogOp );
        
    end
    
end

