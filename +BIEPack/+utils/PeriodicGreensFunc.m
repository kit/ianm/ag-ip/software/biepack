%> @file PeriodicGreensFunc.m
%> @brief Contains the utils.PeriodicGreensFunc class.
%>
%> The code in this file requires the Faddeeva package by Simon
%> Chandler-Wilde and Mohammad Al Azah, which is available at
%> https://github.com/sms03snc/Faddeeva
%> Please see the README.md for details on how to install.
%
% ======================================================================
%
%> @brief A class which is a collection of static method to evaluate
%> periodic Green's functions and Green's functions for waveguides.
%>
%> Three variants are available for evaluation of the (in one direction)
%> periodic Green's function for the Helmholtz equation:
%> - modal expansion (plane propgating and evanescent waves),
%> - series of Hankel functions
%> - using an integral representation of the series of Hankel functions.
%>
%> The Green's function itself, its gradient and the Hessian matrix may be
%> evaluated. In all cases, the integral representation is recommended, as
%> it provides the most accurate results.
%>
%> The Green's function for a waveguide with Neumann boundary conditions is
%> also available, plus functions for the evaluation of its gradient and
%> mixed second order derivatives. Here, evaluation may be based on modal
%> expansions or on using the periodic Green's function (evaluated by the
%> integral representation).
classdef PeriodicGreensFunc
    
    properties (Constant)
            
        % Points and weights of a 40 point generalized Gauss-Laguerre
        % quadrature rule for the weight function t^(-1/2) exp(-t).
        % Only the first 22 points are included as the weights of the
        % remaining points sum up to less than 2e-15.
        gauss_points = [     0.0153256633315, 0.137966001741,  0.3834338413928, ...
            0.7521050835315, 1.244547551113,  1.861525845317,  2.604007976597, ...
            3.473173911353,  4.470426220642,  5.597403070341,  6.85599385527, ...
            8.24835785637,   9.776946394185, 11.44452906932,  13.2542248286, ...
            15.20953878472, 17.31440596067,  19.57324344853,  21.99101289126, ...
            24.57329575658, 27.32638462934,  30.25739478733 ];
        
        gauss_weights =[        0.4876717076145,    0.4315498925489,   0.3378759385518, ...
            0.2339614676086,    0.1432014953837,    0.07741719982969,  0.03693150230886, ...
            0.0155280788108,    0.005746395066181,  0.001868635373923, 0.0005329557733869, ...
            1.330348248363e-4,  2.899288838227e-5,  5.501449254803e-6, 9.061071131725e-7, ...
            1.290892590025e-7,  1.584584657831e-8,  1.668611185219e-9, 1.499943982933e-10, ...
            1.144655943702e-11, 7.369699640332e-13, 3.975041669405e-14 ];
    end
    
    methods (Static)   
        
        % Calculate the quasi-periodic Green's function using a truncated
        % Fourier series. The argument N is the cutoff parameter, the sum
        % is calculated over n=-N:N.
        %
        % z1, z2 column vectors with components of k (x - y)
        % alpha quasi-periodicity, corresponds to alpha/k in the GF paper
        % kL wavenumber times period
        %
        function G = g_per_exp_series(z1, z2, alpha, kL, N)

            if (nargin < 5)
                N = 50;
            end
            mu = -N:N;
            fourier_mu =  2*pi*mu / kL;
            alpha_mu = alpha + fourier_mu;
            beta_mu = sqrt(1 - alpha_mu.^2);

            G = zeros(size(z1));
            for j=1:2*N+1
                G = G + exp(1i * (alpha_mu(j) * z1 + beta_mu(j) * abs(z2))) ./ beta_mu(j);
            end

            G = 1i / (2*kL) * G;
        end      
        
        % Calculate the gradient of the quasi-periodic Green's function 
        % using a truncated Fourier series. The argument N is the cutoff 
        % parameter, the sum is calculated over n=-N:N.
        %
        % The derivative is calculated with respect to the z_j.
        %
        % z1, z2 column vectors with components of k (x - y)
        % alpha quasi-periodicity, corresponds to alpha/k in the GF paper
        % kL wavenumber times period
        %
        function [G1, G2] = g_per_exp_series_gradient(z1, z2, alpha, kL, N)

            if (nargin < 5)
                N = 50;
            end
            mu = -N:N;
            fourier_mu =  2*pi*mu / kL;
            alpha_mu = alpha + fourier_mu;
            beta_mu = sqrt(1 - alpha_mu.^2);

            G1 = zeros(size(z1));
            G2 = zeros(size(z1));
            for j=1:2*N+1
                exp_term = exp(1i * (alpha_mu(j) * z1 + beta_mu(j) * abs(z2)));
                G1 = G1 + 1i * alpha_mu(j) ./ beta_mu(j) * exp_term;
                G2 = G2 + 1i * sign(z2) .* exp_term;
            end

            G1 = 1i / (2*kL) * G1;
            G2 = 1i / (2*kL) * G2;
        end
        
        % Calculate the gradient quasi-periodic Green's function using a truncated
        % series of mirror sources. The argument N is the cutoff parameter, 
        % the sum is calculated over n=-N:N.
        %
        % The derivative is calculated with respect to the z_j.
        %
        % z1, z2 column vectors with components of k (x - y)
        % alpha quasi-periodicity, corresponds to alpha/k in the GF paper
        % kL wavenumber times period
        %
        function G = g_per_hankel_series(z1, z2, alpha, kL, N)

            if (nargin < 5)
                N = 100;
            end

            G = 0.25i * besselh(0, 1, sqrt(z1.^2 + z2.^2));

            for n = 1:N
                nkL = n*kL;
                hor1 = z1 - nkL;
                hor2 = z1 + nkL;
                r1 = sqrt(hor1.^2 + z2.^2);
                r2 = sqrt(hor2.^2 + z2.^2);
                G = G + 0.25i * (exp(1i * alpha * nkL) .* besselh(0, 1, r1) ...
                    + exp(-1i * alpha * nkL) .* besselh(0, 1, r2));
            end
        end
        
        % Calculate the quasi-periodic Green's function using a truncated
        % series of mirror sources. The argument N is the cutoff parameter, 
        % the sum is calculated over n=-N:N.
        %
        % z1, z2 column vectors with components of k (x - y)
        % alpha quasi-periodicity, corresponds to alpha/k in the GF paper
        % kL wavenumber times period
        %
        function [G1, G2] = g_per_hankel_series_gradient(z1, z2, alpha, kL, N)

            if (nargin < 5)
                N = 100;
            end

            r = sqrt(z1.^2 + z2.^2);
            h_term = 0.25i * besselh(1, 1, r) ./ r;
            G1 = h_term .* z1;
            G2 = h_term .* z2;

            for n = 1:N
                nkL = n*kL;
                hor1 = z1 - nkL;
                hor2 = z1 + nkL;
                r1 = sqrt(hor1.^2 + z2.^2);
                r2 = sqrt(hor2.^2 + z2.^2);
                h_term_1 = 0.25i * exp(1i * alpha * nkL) .* besselh(1, 1, r1) ./ r1;
                h_term_2 = 0.25i * exp(1i * alpha * nkL) .* besselh(1, 1, r2) ./ r2;

                G1 = G1 + h_term_1 .* hor1 + h_term_2 .* hor2;
                G2 = G2 + (h_term_1 + h_term_2) .* z2; 
            end
        end

        % Computation of a series of mirror images of a point source via an
        % integral representation of the Hankel function.
        % 
        % Code due to Simon Chandler-Wilde, Tilo Arens
        function S = integral_rep_hankel_sum(z1, z2, alpha, kL, N, M)
            
            config = BIEPack.BIEPackConfig.getInstance();
            if (~ config.faddeeva_package)
                error('Periodic Green''s function cannot be evaluated: Faddeeva package is not installed.')
            end

            S = zeros(size(z1));
            z2_sqr = z2.^2;
            c = exp(1i*alpha*kL);
            
            % first add series term from N to M-1 explicitly
            for n = N:M
               xi_1 = z1 - n*kL;
               r = sqrt(xi_1.^2 + z2_sqr);
               S = S + c^n * besselh(0, 1, r);
            end
            
            S = 1i/4 * S;
            
            % compute the remainign sum by an integral representation of
            % the Hankel function. The nearest pole of the integrand to the
            % origin is subtracted and the integral computed using a generalized
            % Gauss-Laguerre rule. The integral over the pole is expressed
            % and evaluated using the Faddeeva function.
            
            xi = -z1 + (M+1)*kL;
            exp_kL_u_hat = exp(1i*kL*(1+alpha));
            gamma = angle(exp_kL_u_hat) / kL;
            
            root1 = sqrt(2 - gamma);
            root2 = sqrt(gamma) * root1;
            
            cs = cos(z2*root2);
            
            F_of_u_hat = (1+1i)/sqrt(2) * cs / root1;
            
            % compute the integral over the function F minus closest pole
            integral = zeros(size(z1));
            
            import BIEPack.utils.PeriodicGreensFunc
            for n = 1:length(PeriodicGreensFunc.gauss_points)
               u = PeriodicGreensFunc.gauss_points(n) ./ xi;
               root = sqrt(u-2i);
               cs_of_u = cos( z2 .* sqrt(u) .* root );
               F_of_u = cs_of_u ./ root;
               
               integral = integral + PeriodicGreensFunc.gauss_weights(n) ...
                   * ( F_of_u ./ (1 - exp_kL_u_hat*exp(-kL*u)) - F_of_u_hat / kL ./ (u - 1i*gamma) );               
            end
            
            % compute the integral over the term with the pole using the
            % Faddeeva function
            root_u_hat = (1+1i) * sqrt(gamma/2);
            root_xi = sqrt(xi);
            % w_val = PeriodicGreensFunc.fadeeva(root_u_hat * root_xi);
            w_val = wTrap(root_u_hat * root_xi, 11);
            
            S = S + exp(1i * (xi + (M+1)*alpha*kL)) .* (integral ./ (2*pi*root_xi) ...
                + F_of_u_hat/kL * 0.5i / root_u_hat .* w_val);
        end


        % Computation the gradient of a series of mirror images of a point source via an
        % integral representation of the Hankel function.
        function [S1, S2] = integral_rep_hankel_sum_gradient(z1, z2, alpha, kL, N, M)
                        
            config = BIEPack.BIEPackConfig.getInstance();
            if (~ config.faddeeva_package)
                error('Periodic Green''s function cannot be evaluated: Faddeeva package is not installed.')
            end

            S1 = zeros(size(z1));
            S2 = zeros(size(z1));             
            z2_sqr = z2.^2;
            c = exp(1i*alpha*kL);
            
            % first add series term from N to M-1 explicitly
            for n = N:M
               xi = z1 - n*kL;
               r = sqrt(xi.^2 + z2_sqr);
               
               h_term = c^n * besselh(1, 1, r) ./ r;
               S1 = S1 + h_term .* xi;
               S2 = S2 + h_term .* z2;
            end
            
            S1 = -1i/4 * S1;
            S2 = -1i/4 * S2;
            
            % compute the remainign sum by an integral representation of
            % the Hankel function. The nearest pole of the integrand to the
            % origin is subtracted and the integral computed using a generalized
            % Gauss-Laguerre rule. The integral over the pole is expressed
            % and evaluated using the Faddeeva function.
            
            xi = -z1 + (M+1)*kL;
            exp_kL_u_hat = exp(1i*kL*(1+alpha));
            gamma = angle(exp_kL_u_hat) / kL;
            
            root1 = sqrt(2 - gamma);
            root2 = sqrt(gamma) * root1;
            
            cs = cos(z2*root2);
            sn = sin(z2*root2);
            
            F_of_u_hat_1 = (1-1i)/sqrt(2) * (1-gamma) * cs / root1;
            F_of_u_hat_2 = (1+1i)/sqrt(2) * sqrt(gamma) * sn;
            
            % compute the integral over the function F minus closest pole
            integral1 = zeros(size(z1));
            integral2 = zeros(size(z1));
            
            
            import BIEPack.utils.PeriodicGreensFunc
            for n = 1:length(PeriodicGreensFunc.gauss_points)
               u = PeriodicGreensFunc.gauss_points(n) ./ xi;
               root = sqrt(u-2i);
               cs_of_u = cos( z2 .* sqrt(u) .* root );
               F_of_u_1 = cs_of_u .* (u-1i) ./ root;
               F_of_u_2 = -sin( z2 .* sqrt(u) .* root ) .* sqrt(u);
               
               integral1 = integral1 + PeriodicGreensFunc.gauss_weights(n) ...
                   * ( F_of_u_1 ./ (1 - exp_kL_u_hat*exp(-kL*u)) - F_of_u_hat_1 / kL ./ (u - 1i*gamma) );
               integral2 = integral2 + PeriodicGreensFunc.gauss_weights(n) ...
                   * ( F_of_u_2 ./ (1 - exp_kL_u_hat*exp(-kL*u)) - F_of_u_hat_2 / kL ./ (u - 1i*gamma) );               
            end
            
            % compute the integral over the term with the pole using the
            % Faddeeva function
            root_u_hat = (1+1i) * sqrt(gamma/2);
            root_xi = sqrt(xi);
            %w_val = PeriodicGreensFunc.fadeeva(root_u_hat * root_xi);
            w_val = wTrap(root_u_hat * root_xi, 11);
            
            S1 = S1 + exp(1i * (xi + (M+1)*alpha*kL)) .* (integral1 ./ (2*pi*root_xi) ...
                + F_of_u_hat_1/kL * 0.5i / root_u_hat .* w_val);
            S2 = S2 + exp(1i * (xi + (M+1)*alpha*kL)) .* (integral2 ./ (2*pi*root_xi) ...
                + F_of_u_hat_2/kL * 0.5i / root_u_hat .* w_val);
        end

        % Computation of the Hessian of a series of mirror images of a point source via an
        % integral representation of the Hankel function.
        function [S11, S12, S22] = integral_rep_hankel_sum_hessian(z1, z2, alpha, kL, N, M)
            
            config = BIEPack.BIEPackConfig.getInstance();
            if (~ config.faddeeva_package)
                error('Periodic Green''s function cannot be evaluated: Faddeeva package is not installed.')
            end

            S11 = zeros(size(z1));
            S12 = zeros(size(z1)); 
            S22 = zeros(size(z1)); 
            z2_sqr = z2.^2;
            c = exp(1i*alpha*kL);
            
            % first add series term from N to M-1 explicitly
            for n = N:M
               xi_1 = z1 - n*kL;
               r = sqrt(xi_1.^2 + z2_sqr);
               h_term_1 = c^n * besselh(0, 1, r);
               h_term_2 = c^n * besselh(1, 1, r) ./ r;
               S11 = S11 + (h_term_1 - 2*h_term_2) .* xi_1.^2 ./ r.^2 + h_term_2;
               S12 = S12 +  (h_term_1 - 2*h_term_2) .* xi_1 .* z2 ./ r.^2;
               S22 = S22 + (h_term_1 - 2*h_term_2) .* z2_sqr ./ r.^2 + h_term_2;
            end
            
            S11 = -1i/4 * S11;
            S12 = -1i/4 * S12;
            S22 = -1i/4 * S22;
            
            % compute the remainign sum by an integral representation of
            % the Hankel function. The nearest pole of the integrand to the
            % origin is subtracted and the integral computed using a generalized
            % Gauss-Laguerre rule. The integral over the pole is expressed
            % and evaluated using the Faddeeva function.
            
            xi = -z1 + (M+1)*kL;
            exp_kL_u_hat = exp(1i*kL*(1+alpha));
            gamma = angle(exp_kL_u_hat) / kL;
            
            root1 = sqrt(2 - gamma);
            root2 = sqrt(gamma) * root1;
            
            cs = cos(z2*root2);
            sn = sin(z2*root2);
            
            F_of_u_hat_11 = (-1-1i)/sqrt(2) * (1-gamma)^2 * cs / root1;
            F_of_u_hat_12 = (-1+1i)/sqrt(2) * (1-gamma) * sqrt(gamma) * sn;
            F_of_u_hat_22 = (-1-1i)/sqrt(2) * root1 * gamma * cs;
            
            % compute the integral over the function F minus closest pole
            integral1 = zeros(size(z1));
            integral2 = zeros(size(z1));
            integral3 = zeros(size(z1));
            
            import BIEPack.utils.PeriodicGreensFunc
            for n = 1:length(PeriodicGreensFunc.gauss_points)
               u = PeriodicGreensFunc.gauss_points(n) ./ xi;
               root = sqrt(u-2i);
               cs_of_u = cos( z2 .* sqrt(u) .* root );
               F_of_u_11 = cs_of_u .* (u-1i).^2 ./ root;
               F_of_u_12 = -sin( z2 .* sqrt(u) .* root ) .* (u-1i) .* sqrt(u);
               F_of_u_22 = -cs_of_u .* root .* u;
               
               integral1 = integral1 + PeriodicGreensFunc.gauss_weights(n) ...
                   * ( F_of_u_11 ./ (1 - exp_kL_u_hat*exp(-kL*u)) - F_of_u_hat_11 / kL ./ (u - 1i*gamma) );
               integral2 = integral2 + PeriodicGreensFunc.gauss_weights(n) ...
                   * ( F_of_u_12 ./ (1 - exp_kL_u_hat*exp(-kL*u)) - F_of_u_hat_12 / kL ./ (u - 1i*gamma) );
               integral3 = integral3 + PeriodicGreensFunc.gauss_weights(n) ...
                   * ( F_of_u_22 ./ (1 - exp_kL_u_hat*exp(-kL*u)) - F_of_u_hat_22 / kL ./ (u - 1i*gamma) );
               
            end
            
            % compute the integral over the term with the pole using the
            % Faddeeva function
            root_u_hat = (1+1i) * sqrt(gamma/2);
            root_xi = sqrt(xi);
            %w_val = PeriodicGreensFunc.fadeeva(root_u_hat * root_xi);
            w_val = wTrap(root_u_hat * root_xi, 11);
            
            S11 = S11 + exp(1i * (xi + (M+1)*alpha*kL)) .* (integral1 ./ (2*pi*root_xi) ...
                + F_of_u_hat_11/kL * 0.5i / root_u_hat .* w_val);
            S12 = S12 + exp(1i * (xi + (M+1)*alpha*kL)) .* (integral2 ./ (2*pi*root_xi) ...
                + F_of_u_hat_12/kL * 0.5i / root_u_hat .* w_val);
            S22 = S22 + exp(1i * (xi + (M+1)*alpha*kL)) .* (integral3 ./ (2*pi*root_xi) ...
                + F_of_u_hat_22/kL * 0.5i / root_u_hat .* w_val);
        end


        % Calculate the quasi-periodic Green's function using an integral 
        % representation of series over Hankel functions.
        %
        % The parameter n_cut defines which summands made up of Hankel
        % functions are not included in the computation. The Hankel
        % functions for -n_cut:n_cut are left out. If n_cut is negative or
        % empty, all terms are included. This functionality allows the use
        % in a Nyström method, where auxiliary functions need to be
        % evaluated at the singularity.
        function G = g_per_integral_rep(z1_in, z2_in, alpha, kL, n_cut)
            
            z1 = z1_in(:);
            z2 = z2_in(:);

            if (nargin < 5)
                n_cut = -1;
            end

            if n_cut < 0
                sum_start_plus = 0;               
                sum_start_minus = 1;
                M = 1;
            else
                sum_start_plus = n_cut+1;
                sum_start_minus = n_cut+1;
                M = n_cut + 1;
            end
            
            import BIEPack.utils.PeriodicGreensFunc.integral_rep_hankel_sum
            G_plus = integral_rep_hankel_sum(z1, z2, alpha, kL, sum_start_plus, M);
            G_minus = integral_rep_hankel_sum(-z1, z2, -alpha, kL, sum_start_minus, M);

            G = G_plus + G_minus;
            
            G = reshape(G, size(z1_in));
        end

        % Calculate the quasi-periodic Green's function using an integral 
        % representation of series over Hankel functions.
        %
        % The parameter n_cut defines which summands made up of Hankel
        % functions are not included in the computation. The Hankel
        % functions for -n_cut:n_cut are left out. If n_cut is negative or
        % empty, all terms are included. This functionality allows the use
        % in a Nyström method, where auxiliary functions need to be
        % evaluated at the singularity.
        function [G1, G2] = g_per_integral_rep_gradient(z1_in, z2_in, alpha, kL, n_cut)
            
            z1 = z1_in(:);
            z2 = z2_in(:);

            if (nargin < 5)
                n_cut = -1;
            end

            if n_cut < 0
                sum_start_plus = 0;               
                sum_start_minus = 1;
                M = 1;
            else
                sum_start_plus = n_cut+1;
                sum_start_minus = n_cut+1;
                M = n_cut + 1;
            end
            
            import BIEPack.utils.PeriodicGreensFunc.integral_rep_hankel_sum_gradient
            [G_plus_1, G_plus_2] = integral_rep_hankel_sum_gradient(z1, z2, alpha, kL, sum_start_plus, M);
            [G_minus_1, G_minus_2] = integral_rep_hankel_sum_gradient(-z1, z2, -alpha, kL, sum_start_minus, M);

            G1 = G_plus_1 - G_minus_1;
            G2 = G_plus_2 + G_minus_2;
            
            G1 = reshape(G1, size(z1_in));
            G2 = reshape(G2, size(z1_in));
        end

        % Calculate the Hessian of the quasi-periodic Green's function using 
        % an integral representation of series over Hankel functions.
        %
        % The parameter n_cut defines which summands made up of Hankel
        % functions are not included in the computation. The Hankel
        % functions for -n_cut:n_cut are left out. If n_cut is negative or
        % empty, all terms are included. This functionality allows the use
        % in a Nyström method, where auxiliary functions need to be
        % evaluated at the singularity.
        function [G11, G12, G22] = g_per_integral_rep_hessian(z1_in, z2_in, alpha, kL, n_cut)
            
            z1 = z1_in(:);
            z2 = z2_in(:);

            if (nargin < 5)
                n_cut = -1;
            end

            if n_cut < 0
                sum_start_plus = 0;               
                sum_start_minus = 1;
                M = 1;
            else
                sum_start_plus = n_cut+1;
                sum_start_minus = n_cut+1;
                M = n_cut + 1;
            end
            
            import BIEPack.utils.PeriodicGreensFunc.integral_rep_hankel_sum_hessian
            [G_plus_11, G_plus_12, G_plus_22] = integral_rep_hankel_sum_hessian(z1, z2, alpha, kL, sum_start_plus, M);
            [G_minus_11, G_minus_12, G_minus_22] = integral_rep_hankel_sum_hessian(-z1, z2, -alpha, kL, sum_start_minus, M);

            G11 = G_plus_11 + G_minus_11;
            G12 = G_plus_12 - G_minus_12;
            G22 = G_plus_22 + G_minus_22;

            G11 = reshape(G11, size(z1_in));
            G12 = reshape(G12, size(z2_in));
            G22 = reshape(G22, size(z2_in));

        end

        % Test the three different represenations for a number of points
        % The test correspond to cases from :
        %
        % C. Linton, The Green's function for the two-dimensional Helmholtz 
        % equation in periodic domains, J Eng Math 33, 377-402, 1998.
        % 
        % correct values according to this paper are
        % [ 0.4595298795 + 0.3509130869i
        %  -0.3306805081 + 0.1778394385i
        %  -0.3596087433 + 0.04626396800i ]
        function test_per_representations()

            L = 2;
            alpha = 1/sqrt(2);
            z1 = [0.02, 1.0, 1.0];
            z2 = [0.0,  0.2, 1.0];

            G1 = BIEPack.utils.PeriodicGreensFunc.g_per_exp_series(z1, z2, alpha, L);
            G2 = BIEPack.utils.PeriodicGreensFunc.g_per_hankel_series(z1, z2, alpha, L);
            G3 = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep(z1, z2, alpha, L);
           
            for j= 1:length(z1)
                fprintf('(%4.2f, %4.2f):\n', z1(j), z2(j));
                fprintf('Exponentials: %14.10f + %14.10f\n', real(G1(j)), imag(G1(j)));
                fprintf('Hankels:      %14.10f + %14.10f\n', real(G2(j)), imag(G2(j)));
                fprintf('Integral:     %14.10f + %14.10f\n', real(G3(j)), imag(G3(j)));
            end
        end

        % Test the three different represenations for a number of points.
        function test_per_gradient_representations()

            L = 2;
            alpha = 1/sqrt(2);
            z1 = [0.02, 1.0, 1.0];
            z2 = [0.0,  0.2, 1.0];
            h = 0.00001;

            [G11, G12] = BIEPack.utils.PeriodicGreensFunc.g_per_exp_series_gradient(z1, z2, alpha, L);
            [G21, G22] = BIEPack.utils.PeriodicGreensFunc.g_per_hankel_series_gradient(z1, z2, alpha, L);
            [G31, G32] = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_gradient(z1, z2, alpha, L);

            Gph1 = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep(z1+h, z2, alpha, L);
            Gmh1 = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep(z1-h, z2, alpha, L);
            Gph2 = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep(z1, z2+h, alpha, L);
            Gmh2 = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep(z1, z2-h, alpha, L);
            Gdq1 = (Gph1 - Gmh1) / (2*h);
            Gdq2 = (Gph2 - Gmh2) / (2*h);
            
           
            for j= 1:length(z1)
                fprintf('(%4.2f, %4.2f):\n', z1(j), z2(j));
                fprintf('Exponentials: %14.10f + %14.10f   %14.10f + %14.10f\n', real(G11(j)), imag(G11(j)), real(G12(j)), imag(G12(j)));
                fprintf('Hankels:      %14.10f + %14.10f   %14.10f + %14.10f\n', real(G21(j)), imag(G21(j)), real(G22(j)), imag(G22(j)));
                fprintf('Integral:     %14.10f + %14.10f   %14.10f + %14.10f\n', real(G31(j)), imag(G31(j)), real(G32(j)), imag(G32(j)));
                fprintf('Dif. Quot.:   %14.10f + %14.10f   %14.10f + %14.10f\n', real(Gdq1(j)), imag(Gdq1(j)), real(Gdq2(j)), imag(Gdq2(j)));
                fprintf('\n');
                %fprintf('Integral:     %14.10f + %14.10f\n\n', real(G3(j)), imag(G3(j)));
            end
        end

        % Test the three different represenations for a number of points.
        function test_per_hessian_representations()

            L = 2;
            alpha = 1/sqrt(2);
            z1 = [0.02, 1.0, 1.0];
            z2 = [0.0,  0.2, 1.0];
            h = 0.001;

            [G11, G12, G22] = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_hessian(z1, z2, alpha, L);

            [Gph11, Gph12] = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_gradient(z1+h, z2, alpha, L);
            [Gmh11, Gmh12] = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_gradient(z1-h, z2, alpha, L);
            [Gph21, Gph22] = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_gradient(z1, z2+h, alpha, L);
            [Gmh21, Gmh22] = BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_gradient(z1, z2-h, alpha, L);
            Gdq1_d1 = (Gph11 - Gmh11) / (2*h);
            Gdq2_d1 = (Gph12 - Gmh12) / (2*h);
            Gdq1_d2 = (Gph21 - Gmh21) / (2*h);
            Gdq2_d2 = (Gph22 - Gmh22) / (2*h);
            
           
            for j= 1:length(z1)
                fprintf('(%4.2f, %4.2f):\n', z1(j), z2(j));
                fprintf('2 derivatives with respect to z1:\n');
                fprintf('Integral:     %14.10f + %14.10f\n', real(G11(j)), imag(G11(j)));
                fprintf('Dif. Quot.:   %14.10f + %14.10f\n', real(Gdq1_d1(j)), imag(Gdq1_d1(j)));
                fprintf('1 derivatives with respect to z1, one with respect of z_2:\n');
                fprintf('Integral:     %14.10f + %14.10f\n', real(G12(j)), imag(G12(j)));
                fprintf('Dif. Quot.:   %14.10f + %14.10f   %14.10f + %14.10f\n', real(Gdq2_d1(j)), imag(Gdq2_d1(j)), real(Gdq1_d2(j)), imag(Gdq1_d2(j)));
                fprintf('2 derivatives with respect to z2:\n');
                fprintf('Integral:     %14.10f + %14.10f\n', real(G22(j)), imag(G22(j)));
                fprintf('Dif. Quot.:   %14.10f + %14.10f\n', real(Gdq2_d2(j)), imag(Gdq2_d2(j)));
                fprintf('\n');
            end
        end


        % modal expansion of Neumann waveguide Green's function
        function G = g_nwg_modal(kH, k_one_dif, k_x2, k_y2, M)

            if (nargin < 5)
                M = 50;
            end

            G = 1i / (2*kH) * exp(1i * abs(k_one_dif));

            for j=1:M
                k_j = j * pi / kH;
                beta_j = sqrt(1 - k_j.^2);
                expterm = exp(1i * beta_j * abs(k_one_dif));
                cos_x = cos(k_j * k_x2);
                cos_y = cos(k_j * k_y2);
                G = G + 1i / (kH * beta_j) * cos_x .* cos_y .* expterm;
            end

        end    

        % modal expansion of the gradient of the Neumann waveguide Green's function
        % The gradient is computed with respect to kx.
        function [G1, G2] = g_nwg_gradient_modal(kH, k_one_dif, k_x2, k_y2, M)

            if (nargin < 5)
                M = 50;
            end

            % G = 1i / (2*kH) * exp(1i * abs(k_one_dif));
            G1 = -1 / (2*kH) * sign(k_one_dif) .* exp(1i * abs(k_one_dif));
            G2 = zeros(size(k_one_dif));

            for j=1:M
                k_j = j * pi / kH;
                beta_j = sqrt(1 - k_j.^2);
                expterm = exp(1i * beta_j * abs(k_one_dif));
                cos_x = cos(k_j * k_x2);
                sin_x = sin(k_j * k_x2);
                cos_y = cos(k_j * k_y2);
                % G = G + 1i / (kH * beta_j) * cos_x .* cos_y .* expterm;
                G1 = G1 - 1/kH * sign(k_one_dif) .* cos_x .* cos_y .* expterm;
                G2 = G2 - 1i * k_j / (kH * beta_j) * sin_x .* cos_y .* expterm;
            end

        end  

        % modal expansion of second derivatives of the Neumann waveguide Green's function
        % one derivative is taken with respect to kx, one with respect to
        % ky.
        function [G11, G12, G21, G22] = g_nwg_2nd_deriv_modal(kH, k_one_dif, k_x2, k_y2, M)

            if (nargin < 5)
                M = 50;
            end

            G11 = 1i / (2*kH) * exp(1i * abs(k_one_dif));
            G12 = zeros(size(k_one_dif));
            G21 = zeros(size(k_one_dif));
            G22 = zeros(size(k_one_dif));

            for j=1:M
                k_j = j * pi / kH;
                beta_j = sqrt(1 - k_j.^2);
                expterm = exp(1i * beta_j * abs(k_one_dif));
                cos_x = cos(k_j * k_x2);
                sin_x = sin(k_j * k_x2);
                cos_y = cos(k_j * k_y2);
                sin_y = sin(k_j * k_y2);
                
                G11 = G11 + 1i*beta_j/kH * cos_x .* cos_y .* expterm;
                G12 = G12 + k_j/kH * sign(k_one_dif) .* cos_x .* sin_y .* expterm;
                G21 = G21 - k_j/kH * sign(k_one_dif) .* sin_x .* cos_y .* expterm;
                G22 = G22 + 1i * k_j^2 / (kH * beta_j) .* sin_x .* sin_y .* expterm;
            end

        end

        % representation of Neumann waveguide Green's function via periodic
        % Green's function
        %
        % If the parameter no_sing (default false) is set to true, the
        % Hankel function containing the singularity is left out of the
        % computation. This allows the use in implementation of a (for
        % example) a Nyström method, where the singularity is computed
        % explicitely.
        function G = g_nwg_via_periodic(kH, k_one_dif, k_x2, k_y2, no_sing)

            if nargin < 5
                no_sing = false;
            end

            z11 = k_x2 - k_y2;
            z12 = k_x2 + k_y2;

            import BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep
            if no_sing
                G = g_per_integral_rep(z11, k_one_dif, 0, 2*kH, 0) + g_per_integral_rep(z12, k_one_dif, 0, 2*kH);
            else
                G = g_per_integral_rep(z11, k_one_dif, 0, 2*kH) + g_per_integral_rep(z12, k_one_dif, 0, 2*kH);
            end
        end


        % representation of the gradient of the Neumann waveguide Green's function via periodic
        % Green's function
        % The gradient is computed with respect to kx.
        %
        % If the parameter no_sing (default false) is set to true, the
        % Hankel function containing the singularity is left out of the
        % computation. This allows the use in implementation of a (for
        % example) a Nyström method, where the singularity is computed
        % explicitely.
        function [G1, G2] = g_nwg_gradient_via_periodic(kH, k_one_dif, k_x2, k_y2, no_sing)

            if nargin < 5
                no_sing = false;
            end

            z11 = k_x2 - k_y2;
            z12 = k_x2 + k_y2;

            import BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_gradient
            if no_sing
                [G12, G11] = g_per_integral_rep_gradient(z11, k_one_dif, 0, 2*kH, 0);
                [G22, G21] = g_per_integral_rep_gradient(z12, k_one_dif, 0, 2*kH);
                G1 = G11 + G21;
                G2 = G12 + G22;
            else
                [G12, G11] = g_per_integral_rep_gradient(z11, k_one_dif, 0, 2*kH);
                [G22, G21] = g_per_integral_rep_gradient(z12, k_one_dif, 0, 2*kH);
                G1 = G11 + G21;
                G2 = G12 + G22;
            end
        end


        % representation of the secon derivatives of the Neumann waveguide 
        % Green's function via periodic Green's function
        % One derivative is taken with respect to a component of kx, the other
        % with respect of a component of ky.
        %
        % If the parameter no_sing (default false) is set to true, the
        % Hankel function containing the singularity (the primary source) 
        % is left out of the computation. This allows the use in implementation 
        % of (for example) a Nyström method where the singularity is computed
        % explicitely.
        function [G11, G12, G21, G22] = g_nwg_2nd_deriv_via_periodic(kH, k_one_dif, k_x2, k_y2, no_sing)

            if nargin < 5
                no_sing = false;
            end

            z11 = k_x2 - k_y2;
            z12 = k_x2 + k_y2;

            import BIEPack.utils.PeriodicGreensFunc.g_per_integral_rep_hessian
            if no_sing
                [G_p_22, G_p_12, G_p_11] = g_per_integral_rep_hessian(z11, k_one_dif, 0, 2*kH, 0);
            else
                [G_p_22, G_p_12, G_p_11] = g_per_integral_rep_hessian(z11, k_one_dif, 0, 2*kH);
            end
            
            [G_m_22, G_m_12, G_m_11] = g_per_integral_rep_hessian(z12, k_one_dif, 0, 2*kH);
            
            G11 = - G_p_11 - G_m_11;
            G12 = - G_p_12 + G_m_12;
            G21 = - G_p_12 - G_m_12;
            G22 = - G_p_22 + G_m_22;
        end



        function test_nwg_representations()

            kH = 2;

            k_one_dif = [0.01 0.5 1];
            k_x2 = ones(size(k_one_dif));
            k_y2 = [1.0 1.2 0.2];

            G1 = BIEPack.utils.PeriodicGreensFunc.g_nwg_modal(kH, k_one_dif, k_x2, k_y2);
            G2 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif, k_x2, k_y2);
           
            for j= 1:length(k_one_dif)
                fprintf('(%4.2f, %4.2f, %4.2f):\n', k_one_dif(j), k_x2(j),  k_y2(j));
                fprintf('Modal:     %14.10f + %14.10f\n', real(G1(j)), imag(G1(j)));
                fprintf('Periodic:  %14.10f + %14.10f\n', real(G2(j)), imag(G2(j)));
            end
        end


        function test_nwg_gradient_representations()

            kH = 2;

            k_one_dif = [0.01 0.5 1];
            k_x2 = ones(size(k_one_dif));
            k_y2 = [1.0 1.2 0.2];
            h = 0.001;

            [G11, G12] = BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_modal(kH, k_one_dif, k_x2, k_y2);
            [G21, G22] = BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic(kH, k_one_dif, k_x2, k_y2);

            Gph1 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif+h, k_x2, k_y2);
            Gmh1 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif-h, k_x2, k_y2);
            Gph2 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif, k_x2+h, k_y2);
            Gmh2 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif, k_x2-h, k_y2);
            Gdq1 = (Gph1 - Gmh1) / (2*h);
            Gdq2 = (Gph2 - Gmh2) / (2*h);
           
            for j= 1:length(k_one_dif)
                fprintf('(%4.2f, %4.2f, %4.2f):\n', k_one_dif(j), k_x2(j),  k_y2(j));
                fprintf('Modal:      %14.10f + %14.10f   %14.10f + %14.10f\n', real(G11(j)), imag(G11(j)), real(G12(j)), imag(G12(j)));
                fprintf('Periodic:   %14.10f + %14.10f   %14.10f + %14.10f\n', real(G21(j)), imag(G21(j)), real(G22(j)), imag(G22(j)));
                fprintf('Dif. Quot.: %14.10f + %14.10f   %14.10f + %14.10f\n', real(Gdq1(j)), imag(Gdq1(j)), real(Gdq2(j)), imag(Gdq2(j)));
                fprintf('\n');
            end
        end


        function test_nwg_2nd_deriv_representations()

            kH = 2;

            k_one_dif = [0.01 0.5 1];
            k_x2 = ones(size(k_one_dif));
            k_y2 = [1.0 1.2 0.2];
            h = 0.001;

            [G11_mod, G12_mod, G21_mod, G22_mod] = BIEPack.utils.PeriodicGreensFunc.g_nwg_2nd_deriv_modal(kH, k_one_dif, k_x2, k_y2);
            [G11_vp, G12_vp, G21_vp, G22_vp] = BIEPack.utils.PeriodicGreensFunc.g_nwg_2nd_deriv_via_periodic(kH, k_one_dif, k_x2, k_y2);

            [Gph1_x1, Gph1_x2] = BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic(kH, k_one_dif+h, k_x2, k_y2);
            [Gmh1_x1, Gmh1_x2] = BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic(kH, k_one_dif-h, k_x2, k_y2);
            [Gph2_x1, Gph2_x2] = BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic(kH, k_one_dif, k_x2, k_y2+h);
            [Gmh2_x1, Gmh2_x2] = BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic(kH, k_one_dif, k_x2, k_y2-h);
%             Gmh1 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif-h, k_x2, k_y2);
%             Gph2 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif, k_x2+h, k_y2);
%             Gmh2 = BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic(kH, k_one_dif, k_x2-h, k_y2);
%             Gdq1 = (Gph1 - Gmh1) / (2*h);
%             Gdq2 = (Gph2 - Gmh2) / (2*h);

            G11_dq = (Gmh1_x1 - Gph1_x1) / (2*h);
            G21_dq = (Gmh1_x2 - Gph1_x2) / (2*h);
            G12_dq = (Gph2_x1 - Gmh2_x1) / (2*h);
            G22_dq = (Gph2_x2 - Gmh2_x2) / (2*h);
           
            for j= 1:length(k_one_dif)
                fprintf('(%4.2f, %4.2f, %4.2f):\n', k_one_dif(j), k_x2(j),  k_y2(j));
                fprintf('Modal:      %14.10f + %14.10f   %14.10f + %14.10f\n', real(G11_mod(j)), imag(G11_mod(j)), real(G12_mod(j)), imag(G12_mod(j)));
                fprintf('            %14.10f + %14.10f   %14.10f + %14.10f\n', real(G21_mod(j)), imag(G21_mod(j)), real(G22_mod(j)), imag(G22_mod(j)));
                fprintf('Periodic:   %14.10f + %14.10f   %14.10f + %14.10f\n', real(G11_vp(j)), imag(G11_vp(j)), real(G12_vp(j)), imag(G12_vp(j)));
                fprintf('            %14.10f + %14.10f   %14.10f + %14.10f\n', real(G21_vp(j)), imag(G21_vp(j)), real(G22_vp(j)), imag(G22_vp(j)));
                fprintf('Dif. Quot.: %14.10f + %14.10f   %14.10f + %14.10f\n', real(G11_dq(j)), imag(G11_dq(j)), real(G12_dq(j)), imag(G12_dq(j)));
                fprintf('            %14.10f + %14.10f   %14.10f + %14.10f\n', real(G21_dq(j)), imag(G21_dq(j)), real(G22_dq(j)), imag(G22_dq(j)));
                fprintf('\n');
            end
        end

    end

end
