%> @file UtilsDoc.m
%> @brief The documentation of the @a utils package
%
% ======================================================================
%
%> @namespace BIEPack::utils
%> @brief Helper functions
%>