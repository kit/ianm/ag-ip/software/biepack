%> @file laplaceRobinAnnulus.m
%> @brief Contains the laplaceRobinAnnulus() example function
%
% ======================================================================
%
%> @brief Function for solving a Robin problem for Laplace's equation in an
%>     annulus region. 
%>
%> Consider two balls @f$ B_1 @f$, @f$ B_2 @f$ such that @f$ \overline{B_1} \subseteq B_2 @f$.
%> Let @f$ D = B_2 \setminus \overline{B_1} @f$. We solve a Robin problem 
%> for Laplace's equation in @a D by making an ansatz
%> for the solution as a single layer potential,
%> @f[
%>   u(x) = \int_{\partial D} \Phi(x,y) \, \varphi(y) \, \mathrm{d}s(y) \, , \qquad x \in D \, .
%> @f]
%> If the boundary condition is
%> @f[
%>   \frac{\partial u }{\partial n} + u = f \qquad \text{on } \partial D \, ,
%> @f]
%> this will lead to an integral equation for @f$\varphi@f$ on the boundary
%> of @a D,
%> @f[
%>   \varphi(x) + 2 \, \int_{\partial D} \left[ \frac{\partial \Phi(x,y) }{\partial n(x) }
%>   \varphi(y) + Phi(x,y) \right] \varphi(y) \, \mathrm{d}s(y) = 2 f(x) \, , \qquad x \in \partial D \, .
%> @f]
%>
%> The integral equation is solved using a collocation method. The boundary of @a D
%> is represented as a single CurveUnion object and mesh and boundary element
%> space are generated on it. To make sure that the normal points out of @a D
%> everywhere, the boundary curve of @f$ B_1 @f$ is flipped.
%>
%> @param h maximal diameter of an element (default is 0.1).
%> @param numRefine the number of uniform mesh refinements to be carried out
%> @param evalInside specifies whether the function u is to evaluated on a
%>    grid inside the domain. Optional, default value is false
%>
%> @retval phi a vector with values of the approximate solution
%> @retval theSpace BemSpace representing the function space phi is contained in
%> @retval X1 grid point x1-coordinates for evaluation inside of @a D (emtpy, if evalInside is false)
%> @retval X2 grid point x2-coordinates for evaluation inside of @a D (emtpy, if evalInside is false)
%> @retval u_bem values of the BEM-approximation to the solution of the BVP in the grid points
%> @retval u_exact values of the exact solution of the BVP in the grid points
%>
function [phi, theSpace, X1, X2, u_bem, u_exact] = laplaceRobinAnnulus(h,numRefine,solution,evalInside,quadRules)

import BIEPack.*;

% set the mesh size if not provided
if ( nargin == 0 )
    h = 0.1;
end

% no refinement when second argument is not provided
if ( nargin < 2 )
    numRefine = 0;
end

if ( nargin < 3 )
    solution = @examples.collocation.exampleHarmonicFunction;
end

% no evalutation inside the domain if third argument is not provided
if ( nargin < 4 )
    evalInside = false;
end

% define standard quadrature rules if none are given
if ( nargin < 5 )
    [t_c, w_c] = utils.quadGaussLegendre(1,0,1);
    [t_s, w_s] = utils.quadGeneralSingularity(5);
    quadRules = struct( 't_c', t_c, 'w_c', w_c, 't_s', t_s, 'w_s', w_s );
end

% initialize output variables
phi = cell(numRefine+1,1);
mesh = cell(numRefine+1,1);
theSpace = cell(numRefine+1,1);

% % create B_1, B_2 and the annulus
ball1 = geom.Circle( [0.5; 0.0], 0.75 );
ball2 = geom.Circle( [0.0; 0.0], 1.5 );
boundary = geom.CurveUnion( geom.FlippedCurve(ball1), ball2 );
%boundary = geom.Circle( [0.0; 0.0], 0.25 );

% define the meshes and the BemSpaces, refining the correct number of times.
mesh{1} = meshes.BemMesh( boundary, h );
theSpace{1} = spaces.BemSpace( mesh{1}, spaces.BemSpace.PIECEWISE_LINEAR );
for j=1:numRefine    
    mesh{j+1} = mesh{j}.uniformRefinement();
    theSpace{j+1} = spaces.BemSpace( mesh{j+1}, spaces.BemSpace.PIECEWISE_LINEAR );
end

% define the function for evaluating the right hand side
rhs = @(X,Y,NX,NY) rhsFunc(X,Y,NX,NY, solution);

% set up and solve the integral equation on each mesh
for j=1:numRefine+1
    phi{j} = examples.collocation.laplaceRobinIntegralEquation(theSpace{j},rhs,quadRules);    
end

if ( evalInside )
    
    % create a grid inside the domain
    t = linspace(-pi,pi,25);
    s = linspace(0.1,0.9,15);
    [TT,SS] = meshgrid(t,s);
    [b1_x1, b1_x2] = ball1.eval(TT,0);
    [b2_x1, b2_x2] = ball2.eval(TT,0);
    X1 = SS .* b1_x1 + (1-SS) .* b2_x1;
    X2 = SS .* b1_x2 + (1-SS) .* b2_x2;

    % evaluate the single layer potential on the grid for each solution
    u_bem = cell(numRefine+1,1);
    for j=1:numRefine+1
        SLP = ops.LaplaceSLPot(theSpace{j},theSpace{j});
        u_bem{j} = SLP.evalPotential(X1,X2,phi{j});
    end
    u_exact = solution(X1,X2);
    
else
    
    % no evaluation inside, just return empty matrices
    X1 = [];
    X2 = [];
    u_bem = [];
    u_exact = [];
    
end

end

function f = rhsFunc(X,Y,NX,NY,solution)

[u, dx_u, dy_u] = solution(X,Y);
f = 2 * ( NX.*dx_u + NY.*dy_u + u );

end





