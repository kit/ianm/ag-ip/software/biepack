%> @file TrigPolRelation.m
%> @brief File containing the BIEPack.spaces.TrigPolRelation class.
% ======================================================================
%> @brief Class representing the relation between two TrigPol spaces that
%> are defined on ParamCurveMesh instances where one mesh is derived as a
%> uniform refinement of the other mesh. The relation of the meshes is
%> described by a chain of RefinementRelation objects. This implies that all 
%> nodes of the coarse mesh are also present in the fine mesh.
classdef TrigPolRelation < BIEPack.BIEPackObject
    % BIEPack.spaces.TrigPolRelation < BIEPack.BIEPackObject
    
    properties (SetAccess = protected)
        
        %> @brief The space defined on the coarser mesh.
        smallerSpace = [];
        
        %> @brief The space defined on the finer mesh.
        largerSpace = [];
        
        %> @brief The @link RefinementRelation BIEPack.meshes.RefinementRelation @endlink 
        %> that connects the meshes underlying these spaces.
        meshRefRelation = []
        
    end
    
    methods
        
        %> @brief Initializes the object.
        %
        %> If both arguments are given, the method setSpaces() is called to 
        %> to establish the relation.
        %>
        %> @param smallerSpace The space based on the coarser mesh.
        %> @param largerSpace The space based on the finer mesh.
        %>
        %> @retval The newly constructed TrigPolRelation object.
        function relation = TrigPolRelation( smallerSpace, largerSpace )
            
            if ( nargin > 1 )
                relation.setSpaces( smallerSpace, largerSpace );
            end
            
            relation.randomName;
            relation.name = strcat('TrigPolRelation_',relation.name);
            
        end
        
        %> @brief Sets the spaces and extract the refinement relation. 
        %
        %> This method needs to be called before the object can be used. It 
        %> checks that the two spaces satisfy all restrictions: They both
        %> must be TrigPol instances defined on the same curve with meshes
        %> where there is a chain of RefinementRelation objects leading
        %> the mesh of @a largerSpace to that of @a smallerSpace.
        %>
        %> These relations are merged to have a direct link between the
        %> meshes.
        %>
        %> @param smallerSpace The space based on the coarser mesh.
        %> @param largerSpace The space based on the finer mesh.
        %>
        %> @todo Much of this function is the same as for a
        %> BemSpaceRelation. Derive from this class.
        function setSpaces( relation, smallerSpace, largerSpace )
            
            if ( ~ BIEPack.utils.derivedFrom(smallerSpace, 'BIEPack.spaces.TrigPol' ) )
                error( 'In TrigPolRelation %s, method setSpaces(): argument 1 is not a BIEPack.spaces.TrigPol', relation.name );
            end
            
            if ( ~ BIEPack.utils.derivedFrom(largerSpace, 'BIEPack.spaces.TrigPol' ) )
                error( 'In TrigPolRelation %s, method setSpaces(): argument 2 is not a BIEPack.spaces.TrigPol', relation.name );
            end
                                   
            % Build a chain of RefinementRelation objects that leads from
            % the finest to the coarsest mesh.
            refRelChain = largerSpace.theMesh.refinementRelation;
            
            if ( isempty( refRelChain ) )
                error( 'In TrigPolRelation %s, method setSpaces(): argument 2 does not define a RefinementRelation', relation.name );
            end
            
            while ( refRelChain(end).coarseMesh ~= smallerSpace.theMesh )
            
                if ( isempty( refRelChain(end).coarseMesh.refinementRelation ) )
                    error( 'In TrigPolRelation %s, method setSpaces(): no complete chain of RefinementRelations from finest to coarsest mesh.', relation.name );
                end
                
                refRelChain = [ refRelChain,  refRelChain(end).coarseMesh.refinementRelation ]; %#ok<AGROW>
                
            end
            
            % iteratively merge the RefinementRelation objects            
            while ( length(refRelChain) > 1 )
                refRelChain = [ BIEPack.meshes.RefinementRelation.merge( refRelChain(2), refRelChain(1) ), refRelChain(3:end) ];
            end
            
            relation.meshRefRelation = refRelChain;
            
            relation.smallerSpace = smallerSpace;
            relation.largerSpace = largerSpace;
            
        end
        
        %> @brief Restrict a function from the space based on the fine mesh to a 
        %> function defined on the coarse mesh.
        %
        %> For a TrigPol space, the coefficients represent the node values
        %> of the function. Thus, simply, the node values at the nodes of
        %> coarse mesh have to be extracted.
        %>
        %> @param y_fine Representation of the function in the finer space.
        %>
        %> @retval y_coarse Representation of the function in the coarser mesh.
        function y_coarse = restrict(relation, y_fine)
            
            y_coarse = y_fine(relation.meshRefRelation.nodeMap);
            
        end
        
        %> @brief Extend a function in the space based on the coarse mesh 
        %> to a function in the space defined on the fine mesh.
        %>
        %> For a TrigPol space, the coefficients represent the node values
        %> of the function.
        %>
        %> @param y_coarse Representation of the function in the coarser mesh.
        %> 
        %> @retval y_fine Representation of the function in the finer space.
        function y_fine = embed(relation, y_coarse)
            
            % get the Fourier coefficients
            a = relation.smallerSpace.fourierCoeff(y_coarse);
            
            % pad them with zeros
            Ndiff = relation.largerSpace.N - relation.smallerSpace.N;
            a = [ a(1:relation.smallerSpace.N+1); zeros( 2*Ndiff,1); a(relation.smallerSpace.N+2:end) ];
            
            % evaluate on fine mesh
            y_fine = relation.largerSpace.nodeValues(a);
            
        end
        
    end
    
end

