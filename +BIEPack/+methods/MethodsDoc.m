%> @file MethodsDoc.m
%> @brief File containing the documentation of the @a methods package
%
% ======================================================================
%
%> @namespace BIEPack::methods
%> @brief Classes representing methods for solving integral equations.
%>
%> BIEPack has been written with the specific aim to make it simple to apply
%> different methods for solving integral equations to the same equation.
%> The @c methods package provides the means for this.
%>
%> Each method implemented in the @c methods package corresponds to an abstract
%> class in the package @link BIEPack::ops ops @endlink. We call this class the <i> interface class</i>.
%> The interface class provides the basics of the implementation and all
%> code to check that an operator satisfies the requirements of a method in a
%> specific situation.
%>
%> The usually workflow is to define the operators necessary for a given
%> integral equation and then to construct the method providing the
%> operators (and also the right hand side of the equation) as parameters. 
%> Then the @c apply() method is called to compute the solution of the
%> integral equation by the given method. The @link GetStartedDoc Getting
%> Started Example @endlink shows this workflow.
%>
%> Before the numeric solution of the integral equation takes place, however, the method
%> needs to make sure that it can indeed be applied in this specific situation.
%> To make this clearer, consider the following example. The @link
%> BIEPack.methods.DegenerateKernelApprox DegenerateKernelApprox @endlink
%> method has the interface class @link BIEPack.ops.DegenerateKernelOp DegenerateKernelOp
%> @endlink. A concrete class that implements this interface is @link
%> BIEPack.ops.LaplaceDLOp LaplaceDLOp @endlink, the double layer operator
%> corresponding to the Laplace operator.
%> Mathematically, approximation by degenerate kernels requires at least
%> continuity of the kernel of the integral operator. However, the double
%> layer operator can be defined on quite general curves. Only when the
%> curve is at least twice contiuously differentiable is the
%> kernel of this operator continuous and thus meets this requirement of the
%> degenerate kernel approximation. A check that this is the case can only
%> be carried out when the user has provided the information what method is
%> to be used.
%>
%> A concrete implementation of an integral operator such as @link
%> BIEPack.op.LaplaceDLOp LaplaceDLOp @endlink implements the
%> @c checkRequirements() method to carry out all necessary checks that a
%> method may be applied. When apply() is called on a method, it in turn calls the 
%> @a checkRequirements() method for all the operators involved in the equation to
%> make sure the method can be used.
%>
%> In the following table we give an overview of the methods implemented,
%> the corresponding interface class and the concrete operator classes
%> implementing this interface.
%>
%> | Method Class                                                                 | Interface Classes                                                | Concrete Implementations                                         |
%> | ---------------------------------------------------------------------------- | ---------------------------------------------------------------- | ---------------------------------------------------------------- |
%> | @link BIEPack.methods.DegenerateKernelApprox DegenerateKernelApprox @endlink | @link BIEPack.ops.DegenerateKernelOp DegenerateKernelOp @endlink | @link BIEPack.ops.ContinuousKernelOp ContinuousKernelOp @endlink |
%> | ^                                                                            | ^                                                                | @link BIEPack.ops.LaplaceDLOp LaplaceDLOp @ndlink                |
%> ||||
%> | @link BIEPack.methods.NystroemLog NystroemLog @endlink | @link BIEPack.ops.LogKernelOp LogKernelOp @endlink | @link BIEPack.ops.ConcreteLogKernelOp ConcreteLogKernelOp @endlink |
%> ||||
%> | @link BIEPack.methods.NystroemPeriodicLog NystroemPeriodicLog @endlink | @link BIEPack.ops.PeriodicLogOp PeriodicLogOp @endlink       | @link BIEPack.ops.LaplaceSLOp LaplaceSLOp @endlink   |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzSLOp HemholtzSLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzDLOp HemholtzDLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzADLOp HemholtzADLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzHsingDifOp HemholtzHsingDifOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzNwgSLOp HemholtzSLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzNwgDLOp HemholtzDLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzNwgADLOp HemholtzADLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzNwgHsingDifOp HemholtzNwgHsingDifOp @endlink |
%> | ^                                                                      | @link BIEPack.ops.PeriodicSmoothOp PeriodicSmoothOp @endlink | @link BIEPack.ops.LaplaceDLOp LaplaceDLOp @endlink   |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.LaplaceADLOp LaplaceADLOp @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.LaplaceSLPot LaplaceSLPot @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.LaplaceDLPot LaplaceDLPot @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzSLPot HemholtzSLPot @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzDLPot HemholtzDLPot @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzNwgSLPot HemholtzNwgSLPot @endlink |
%> | ^                                                                      | ^                                                            | @link BIEPack.ops.HelmholtzNwgDLPot HemholtzNwgDLPot @endlink |
%
