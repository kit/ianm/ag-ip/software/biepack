%> @file LogKernelOp.m
%> @brief File containing the BIEPack.ops.LaplaceDLOp class.
% ======================================================================
%> @brief An integral operator on a curve with a logarithmic singularity.
%>
%> This class represents the discretization of some integral operator
%> defined on a single ParamCurve C with parameter interval [a,b] of the 
%> form
%>
%> @f[
%>   K \varphi( \eta(t) ) = \int_a^b \log | t - \tau - z | \, K(t,\tau) 
%>   \, \varphi( \eta( \tau) ) \, | \eta'(\tau) | \, \mathrm{d} \tau , 
%>   t \in [a,b].
%> @f]
%>
%> with @a K a smooth (at least continuous) factor.
%>
%> This is an interface class for the @link BIEPack.methods.NystroemLog
%> NystroemLog @endlink method.
%>
%> The discretized operator's definition space is a @link BIEPack.spaces.BemSpace @endlink 
%> based on a @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink with
%> node points which are uniform in parameter space. The integral is 
%> discretized by product quadrature as described in Atkinson (1997), section 4.2. 
%> The quadrature points on [a,b] are the pull back of the node points. The 
%> range space is the same as the definition space.
classdef LogKernelOp < BIEPack.ops.GeneralOp
    
    properties
        
        %> @brief Translation of the singularity.
        z = 0;
        
    end
    
    methods
        
        %> @brief Construct the operator
        %>
        %> The definition spaces are passed on to super class constructor. Checks
        %> whether the spaces are compatible with a LogKernelOp are carried
        %> out by the checkRequirements() function. The attribute @a z is
        %> also initialized.
        function lko = LogKernelOp( defSpace, rangeSpace, z )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            lko@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
            if ( nargin == 3 )
                lko.z = z;
            end
            
        end
        
                
        %> @brief Compute a matrix that represents the discretized operator
        %> replacing the integration with product quadrature.
        function A = getLogImplementation(lko)                       
            
            % the grid has N equidistant points
            N = lko.defSpace.dimension;
            a = lko.defSpace.theCurve.startParam;
            b = lko.defSpace.theCurve.endParam;
            t = lko.defSpace.theMesh.t;
            [xd,yd] = lko.defSpace.theCurve.eval(t,1);
            normalNorm = sqrt( xd.^2 + yd.^2 );
           
            A = zeros(N);
            K = lko.evalLogKernel(t);
            for j=1:N
                w = BIEPack.utils.quadProdLog(a,b,N-1,t(j) - lko.z);
                A(j,:) = w .* K(j,:) .* normalNorm;
            end
            
        end
        
        %> @brief Checks whether the geometry, mesh and spaces are such that this
        %> operator may be used in a Nyström method for logarithmic kernels.
        %>
        %> Requirements are that the definition and range spaces are equal
        %> and that they are a @link BemSpace BIEPack.spaces.BemSpace @endlink
        %> instances based on a @link BIEPack.meshes.ParamCurveMesh @endlink.
        %> The nodes of the mesh need to be placed uniformly in parameter space. 
        %>
        %> @retval isOk True if the operator meets the requirements.
        %>
        %> @todo properties of functions in space are not checked.
        function isOk = checkRequirementsForLogKernelOp(logKernelOp)
                        
            if ( ~ BIEPack.utils.derivedFrom( logKernelOp.defSpace, 'BIEPack.spaces.BemSpace' ) )
                isOk = false;
                return
            end
            
            if ( logKernelOp.defSpace ~= logKernelOp.rangeSpace )
                isOk = false;
                return
            end       
            
            if ( ~ BIEPack.utils.derivedFrom( logKernelOp.defSpace.theMesh, 'BIEPack.meshes.ParamCurveMesh' ) )
                isOk = false;
                return
            end      
            
            if ( ~ logKernelOp.defSpace.theMesh.uniformInParam )
                isOk = false;
                return
            end
            
            isOk = true;
            
        end
            
        
    end
    
    
    methods ( Abstract )
        
        %> @brief Evaluate the kernel function K. 
        %>
        %> The parameter values of the integration curve are passed as a row 
        %> vector t. The function returns a matrix of values K(t_j,t_k).
        K = evalLogKernel(lko,t);
        
    end
    
end

