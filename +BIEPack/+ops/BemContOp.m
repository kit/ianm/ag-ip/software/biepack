%> @file BemContOp.m
%> @brief Contains the BemContOp class.
% ======================================================================
%> @brief A boundary integral operator on a BemMesh with a continuous
%> kernel.
%>
%> Operator of the form
%> 
%> A phi(x) = a(x) phi(x) + \int_C K(x,y) phi(y) ds(y), x \in C.
%>
%> with a piecewise continuous function K and a function @i a defined on C.
%> 
%> The operator is assumed to be defined on a BIEPack.geom.Curve with a 
%> BIEPack.meshes.BemMesh. Its definition and range spaces are BemSpaces.
%> The kernel function is assumed to be continuous on each element.
%>
%> The mesh is assumed to have elements S_1,...,S_M with local parametrizations 
%> eta_j : [0,1] -> S_j. Then the operator is usually written as
%>
%> A phi(x) = a(x) phi(x) + \sum_{j=1}^M \int_0^1 K(x,eta_j(tau)) phi(eta_j(tau)) | eta_j'(tau) | d tau
%>
%> @todo Write checkRequirementsForBemCont() function.
% ======================================================================
classdef BemContOp < BIEPack.ops.GeneralOp
    
    methods 
        
        %> @brief Construct the operator
        %
        %> The function just passes on the call to the super class constructor. 
        %> Checks whether the spaces are compatible with a BemContOp are 
        %> carried out by the checkRequirementsForBemCont() function.        
        function bemContOp = BemContOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            bemContOp@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
        end
  
        % The collocation matrix is computed by looping over all elements. 
        % On each element, the integrals
        %
        % \int_0^1 K(x,eta(tau)) f(tau) | eta'(tau) | d tau
        %
        % are computed for all form functions f of the element and all 
        % collocation points x. Here, eta is the parametrization of the
        % element over the reference interval [0,1].
        function A = getCollocationImplementation( bemContOp, quadRules )
           
            % collocation points are the nodes of the mesh
            mesh = bemContOp.defSpace.theMesh;
                
            % evaluate initial function
            basisFuncs = bemContOp.defSpace.evalBasisFunctionsInCollocPoints( 1:bemContOp.defSpace.dimension );                
            a = bemContOp.evalBemAFunction( bemContOp.rangeSpace.collocPoints );
            A = (a * ones(1,size(basisFuncs,2))) .* basisFuncs;
            
            for elNum = 1:mesh.numElements
                
                curve = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) };
                
                % evaluate eta and eta' in the quadrature points tau.
                elementLength = mesh.endParameter(elNum) - mesh.startParameter(elNum);
                tau = mesh.startParameter(elNum) + quadRules.contPoints * elementLength;
                [quadX, quadY] = curve.eval( tau, 0 );
                [dX, dY] = curve.eval( tau, 1 );
                weights = elementLength * sqrt(dX.^2 + dY.^2) .* quadRules.contWeights;
                
                % evaluate kernel function K
                K = bemContOp.evalBemContKFunction( bemContOp.rangeSpace.collocPoints, ...
                                                    quadX, quadY, dX, dY, ...                                                    
                                                    curve, tau );
                                              
                % compute the kernel
                kernelCont = K .* ( length(bemContOp.rangeSpace.collocPoints) * weights );
                
                % find degrees of freedom for this element
                shapeFuncs = bemContOp.defSpace.evalShapeFunctions( elNum, quadRules.contPoints.' );
                globalDofs = bemContOp.defSpace.localToGlobalDOFMap( elNum ).globalDOF;

                % compute integrals and add to matrix
                A(:,globalDofs) = A(:,globalDofs) + kernelCont * shapeFuncs;
                
            end
            
        end
        
    end 
        
        
    methods (Abstract)
        
        %> @brief Evaluate the function a in the operator.
        %
        % The function a is evaluated in x. The coordinates of x are
        % passed as xpoints.X, xpoints.Y. 
        a = evalBemAFunction( bemContOp, xpoints );
        
        %> @brief Evaluate the function K in the integral operator.
        %
        % The function K is evaluated in x and y. The coordinates of x are
        % passed as xpoints.X, xpoints.Y. 
        K = evalBemContKFunction( bemContOp, xpoints, Y1, Y2, dY1, dY2, curve, tau );

    end   
    
end
