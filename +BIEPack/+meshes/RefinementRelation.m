%> @file RefinementRelation.m
%> @brief Contains the meshes.RefinementRelation class.
% ======================================================================
%> @brief Representation of the relation between two meshes where one is
%> derived from the other through mesh refinement.
%>
%> 
% ======================================================================
classdef RefinementRelation < BIEPack.BIEPackObject
    % BIEPack.meshes.RefinementRelation < BIEPack.BIEPackObject
    
    properties (SetAccess = protected)
        
        %> @brief The initial coarser mesh
        coarseMesh = [];
        
        %> @brief The finer mesh, that has been generated through a refinement.
        fineMesh = []
        
        %> @brief Row vector of length coarseMesh.numNodes. For every node 
        %> in the coarser mesh, the corresponding node number in the finer 
        %> mesh is stored.
        nodeMap = [];
        
        %> @brief Matrix, where each column represents a node of the finer 
        %> mesh that is not in the coarse mesh.
        %> The first row contains the index of the new node in the fine 
        %> mesh.
        %>
        %> The second row contains the index of the element in the coarse 
        %> mesh, where this new node is located.
        %>
        %> The third and last row contains the parameter value t in the
        %> element parameterisation,
        %>
        %>  \f$ \gamma(t) = \eta( sp + t (ep - sp) ),   t \in [0,1]\f$ 
        %>
        %> where, \f$\eta\f$ is the parametrisation of the ParamCurve. sp and ep
        %> are the starting and ending parameters, respectively.
        newNodeMap = [];
        
        %> @brief This cell array has an entry for every element in the 
        %> coarser mesh. This entry is a row vector that contains the 
        %> indeces of the elements in the fine mesh that make up this 
        %> element. 
        elementMap = {};
        
    end
    
    methods
        
        %> @brief The constructor of the RefinementRelation. This can either 
        %> be called without any arguments at all, or with the two meshes and
        %> their node relation. If all arguments are given, the setMeshesh
        %> function will be called.
        %>
        %> @param coarseMesh The initial mesh.
        %> @param fineMesh The refined mesh.
        %> @param nodeMap The nodes of the coarser mesh as described in the
        %> properties section.
        %> @param newNodeMap The matrix corresponding only to the nodes of
        %> the finer mesh, that are not part of the coarser mesh
        %> @param elementMap Indeces of the elements in the finer mesh as
        %> described in the properties section.
        %>
        %> @retval rr The newly constructed RefinementRelation object.
        function rr = RefinementRelation( coarseMesh, fineMesh, nodeMap, newNodeMap, elementMap )
                        
            rr.randomName;
            rr.name = strcat('refinementRelation_',rr.name);
            
            if ( nargin == 5 )
                rr.setMeshes( coarseMesh, fineMesh, nodeMap, newNodeMap, elementMap );
            end
            
        end
        
        %> @brief Sets the meshes and the node relation for a given 
        %> RefinementRelation object
        %>
        %> @param rr The RefinementRelation obejct, for which the
        %> properties will be set using the other input arguments.
        %> @param coarseMesh The initial mesh.
        %> @param fineMesh The refined mesh.
        %> @param nodeMap The nodes of the coarser mesh as described in the
        %> properties section.
        %> @param newNodeMap The matrix corresponding only to the nodes of
        %> the finer mesh, that are not part of the coarser mesh
        %> @param elementMap Indeces of the elements in the finer mesh as
        %> described in the properties section.
        function setMeshes( rr, coarseMesh, fineMesh, nodeMap, newNodeMap, elementMap )
            
            rr.fineMesh = fineMesh;
            rr.coarseMesh = coarseMesh;
            rr.nodeMap = nodeMap;
            rr.newNodeMap = newNodeMap;
            rr.elementMap = elementMap;
            
        end
        
    end
    
    
    methods (Static)
        
        %> @brief This merges two RefinemenRelation objects rr_c and rr_f
        %> into a new object rr. The fine mesh in rr_c must be the same as 
        %> the coarse mesh in rr_f.
        %>
        %> The coarse mesh of rr will be the coarse mesh of rr_c and the 
        %> fine mesh of rr will be set equal to the fine mesh in rr_f.
        %> 
        %> @param rr_c A RefinemenRelation, where its fineMesh is equal to
        %> the coarseMesh of rr_f.
        %> @param rr_c A RefinemenRelation, where its coarsMesh is equal to
        %> the fineMesh of rr_c.
        %>
        %> @retval rr The merger of the two RefinementRelation objects,
        %> also a RefinementRelation object.
        function rr = merge( rr_c, rr_f )            
            
            if ( ~ BIEPack.utils.derivedFrom(rr_c, 'BIEPack.meshes.RefinementRelation' ) )
                error( 'In RefinementRelation.merge(): argument 1 is not a BIEPack.meshes.RefinementRelation' );        
            end
            
            if ( ~ BIEPack.utils.derivedFrom(rr_f, 'BIEPack.meshes.RefinementRelation' ) )
                error( 'In RefinementRelation.merge(): argument 1 is not a BIEPack.meshes.RefinementRelation' );
            end
            
            if ( rr_c.fineMesh ~= rr_f.coarseMesh )
                error( 'In RefinementRelation.merge(): fine mesh of argument 1 is not equal to coarse mesh in argument 2' );
            end
            
            rr = BIEPack.meshes.RefinementRelation;
            
            rr.coarseMesh = rr_c.coarseMesh;
            rr.fineMesh = rr_f.fineMesh;
            
            rr.nodeMap = rr_f.nodeMap(rr_c.nodeMap);
            
            rr.elementMap = cell( 1, length( rr_c.elementMap ) );
            reverseElementMap = zeros(1, rr_c.fineMesh.numElements );
            for j=1:length( rr_c.elementMap )
                for k=1:length( rr_c.elementMap{j} )
                    rr.elementMap{j} = [ rr.elementMap{j} rr_f.elementMap{rr_c.elementMap{j}(k)} ];                    
                end
                reverseElementMap( rr_c.elementMap{j} ) = j;
            end
            
            nNM1 = rr_c.newNodeMap;            
            nNM2 = rr_f.newNodeMap;
            
            nNM1(1,:) = rr_f.nodeMap(nNM1(1,:));
            
            intermediateElements = nNM2(2,:);
            coarseElements = reverseElementMap( nNM2(2,:) );
            nNM2(2,:) = coarseElements;
            a1 = rr_c.coarseMesh.startParameter( coarseElements );
            b1 = rr_c.coarseMesh.endParameter( coarseElements );
            a2 = rr_c.fineMesh.startParameter( intermediateElements );
            b2 = rr_c.fineMesh.endParameter( intermediateElements );
            t = nNM2(3,:);
            nNM2(3,:) =  ( a2 - a1 + t .* (b2 - a2) ) ./ (b1 - a1);
            
            rr.newNodeMap = [ nNM1, nNM2 ];
            
        end
        
    end
    
end

