%> @file TrigPol.m
%> @brief Space of trigonometric polynomials on the parameter interval.
% =========================================================================
%> @brief A space of trigonometric polynomials requires a geom.ParamCurve 
%> C to be defined on. The period L of the trigonometric polynomials is 
%> equal to the length of the parameter interval so that for a closed 
%> curve, the functions in the space will be smooth on the curve.
%>
%> This type of space further requires a ParamCurveMesh to be defined on
%> C that has the uniformInParam attribute set to true.
%>
%> The dimension of a TrigPol space is always even and equal to 2N, where N
%> is called the degree. If the curve is parameterised over the interval
%> [a, a+L], the orthogonal basis in parameter space  contains the
%> shifted and scaled trigonometric monomials
%>
%> \f$( 1 / L  )^{1/2} exp( i j [ (2\pi/L) (t - a) - \pi ] ) ,  j =
%> -N+1,...,N.\f$
%>
%> For \f$a = -\pi\f$ and \f$L = 2\pi\f$, these are exactly the standard
%> trigonometric monomials.
%>
%> If the parameterisation of the curve C is denoted by \f$\eta\f$, substituting 
%> \f$t = \eta^{-1}(x)\f$ gives an orthogonal basis in a weighted \f$L^2\f$ space on C 
%> (with the  weight \f$| \eta'(\eta^{-1}(x)) |^{-1}\f$ ).
%>
%> A function in the space is specified by a column vector of length 2N
%> of complex coefficients. These are the values of the function in the
%> points
%>
%> \f$x^{(j)} = \eta( a + j L / (2N) ) ,  j = 0,...,2N-1.\f$
%>
%> These are exactly the points in theMesh.t .
%>
%> Alternatively, the coefficients of the basis functions can be
%> specified. These have to be provided in the ordering
%>
%> \f$[a_0; a_1; ... a_N; a_{-N+1}; ... a_{-1} ]\f$
%>

classdef TrigPol < BIEPack.spaces.SpaceOnCurve
    % BIEPack.spaces.TrigPol 

    
    properties (SetAccess = protected)
        
        %> @brief The length of the parameter intervall for this curve.
        L = 1;
        
        %> @brief The highest degree of trigonometric monomials in this
        %> space.
        N = [];
        
        %> @brief Correction factors for Fourier coefficients when applying
        %> FFTs.
        phase_cor = [];
        
    end
    
    methods
        
        %> @brief Creates a space of trigonometric polynomials defined using 
        %> the nodes of the mesh basedOn if this input parameter is provided.
        %>
        %> @param basedOn The mesh that the TrigPol space is based on.
        %>
        %> @retval trigPol The newly constructed TrigPol object.
        function trigPol = TrigPol( basedOn )
            
            if (nargin > 0)
                trigPol.setMesh( basedOn);
            end
            
        end
        
        %> @brief Sets a new mesh. This function invalidates all previous 
        %> uses of this space, so this function should only be called for
        %> initialization purposes.
        %> 
        %> @param space The space for which a new underlying mesh will be
        %> set.
        %> @param mesh The new mesh that the space will be defined on.
        function setMesh( space, mesh )
            
            % TODO Check that the mesh has the right characteristics
            
            space.dimension = mesh.numNodes;
            space.N = space.dimension / 2;
            space.theCurve = mesh.theCurve;
            space.theMesh = mesh;
            space.L = space.theCurve.endParam - space.theCurve.startParam;
            
            jrange = [ 0:space.N , -space.N+1:-1 ].';
            space.phase_cor = exp( 1i * jrange * 2*pi/ space.L * space.theCurve.startParam );
            
        end
        
        %> @brief Evaluates a Lagrange basis function for this space.
        %> The input parameter j should be a single natural number, whereas
        %> t can be a matrix containing parameter values.
        %>
        %> @param space Space for which the basis functions will be
        %> evaluated.
        %> @todo What does j do?
        %> @param j ?
        %> @param t Matrix containing the parameter values.
        %>
        %> @retval The Lagrange basis function.
        function bf = evalLagrangeBasisFunction(space, j, t)
            
            bf = zeros(size(t));            
            t_diff = 2*pi/space.L * ( t - space.theMesh.t(j+1) );
            
            index = abs(t_diff) > 1e-12;
            bf(~index) = 1.0;
            
            bf(index) = 1/(2*space.N) * sin( space.N * t_diff(index) ) .* ( cot( t_diff(index) / 2 ) + 1i );     
           
        end
        
        
        %> @brief Computes the Fourier coefficients \f$a_j\f$ of a trigonometric
        %> polynomial defined by the point values \f$y_k\f$ on the grid.
        %>
        %> @param y The point values on the grid, that define the
        %> polynomial.
        %>
        %> @retval a The Fourier coefficients of the trigonometric
        %> polynomial.
        function a = fourierCoeff(space, y)
            
%            a = fft(y) ./ space.phase_cor  * ( sqrt( space.L ) / (2 * space.N )  ) ;
            a = fft( y ) .* exp( 1i*pi * (0:2*space.N-1).' ) * ( sqrt( space.L ) / (2 * space.N )  ) ;
            
        end
        
        
        %> @brief Computes the point values \f$y_k\f$ on the grid from the 
        %> coefficients \f$a_j\f$ of the trigonometric polynomial.
        %> 
        %> @param a The coefficients of the trigonometric polynomial.
        %>
        %> @retval y The point values \f$y_k\f$ on the grid.
        function y = nodeValues(space, a)
           
%            y = ifft( a .* space.phase_cor ) * ( 2* space.N / sqrt( space.L ) );
            y = ifft( a .* exp( -1i*pi * (0:2*space.N-1).' ) ) * ( 2 * space.N / sqrt( space.L ) );
            
        end        
        
        
        %> @brief Evaluates a function in this space or one of its derivatives in the 
        %> points specified by the parameter values t. The derivative is
        %> taken in the sense of a function of the parameter of the curve.
        %>
        %> The function is defined by the column vector \f$y_k\f$ of its
        %> point values in the mesh points. Alternatively, if the input
        %> parameter fcoeffs is passed and it is true, the funtion is 
        %> specified by the column vector of the coefficients \f$a_j\f$. 
        %> If the input paramter n is passed, it specifies the degree of 
        %> the derivative that is evaluated. The output is of the same size as t.
        %>
        %> @param y Point values of the function in the mesh points.
        %> @param t Array containing the parameter values that specify the 
        %> points in which the function will be evaluated.
        %> @param n Degree of the evaluated derivative.
        %> @param fcoeffs Fourier coefficients that define the function in 
        %> the space.
        %>
        %> @retval f The values of the function f. Of the same size as t.
        %>
        %> @todo check whether this is correct.
        %> @todo implement derivatives
        function f = evalFunction(space, y, t, n, fcoeffs)
            
            if ( nargin <= 4 || fcoeffs == false )
                a = space.fourierCoeff(y);
            else
                a = y;
            end
            
            T = reshape(t,1,length(t(:)));
            degreefactor = 1i * 2*pi / space.L * [ 0:space.N, -space.N+1:-1 ].';
            
            if ( nargin < 3 || n == 0 )
                f = ( a / sqrt(space.L) ).' * exp( degreefactor * T );
            else
                f = ( a .* degreefactor.^n / sqrt(space.L) ).' * exp( degreefactor * T );
            end
            f = reshape(f,size(t));
            
        end
        
    end
    
end

