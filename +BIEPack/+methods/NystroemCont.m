%> @file NystroemCont.m
%> @brief File containing the BIEPack.methods.NystroemCont class.
% ======================================================================
%> @brief Class representing Nyström methods for continuous kernels based
%> on an arbitrary quadrature rule for smooth function
%
%> The method can be applied to an integral equation of the second kind with 
%> an integral operator with a continuous kernel. The integral is replaced
%> by a the quadrature rule. 
%>
%> On construction, the operator @a A and the right hand side @f$\psi@f$ in 
%> the equation
%> @f[
%>     ( I - A ) \, \varphi = \psi
%> @f]
%> and the weights of the quadrature rule have to be provided. This rule must
%> be suitable for continuous functions defined on the parameter interval of
%> curve.
%>
%> The operator is represented as an instance of SimpleContKernelOp which
%> requires it to have a continuous kernel and to map a BemSpace onto
%> itself. The space should also be based on a @link BIEPack.meshes.ParamCurveMesh 
%> ParamCurveMesh @endlink with nodes corresponding to the quadrature points
%> of the quadrature rule.
%>
%> The method uses only nodal values of the functions involved. The
%> BemSpace is used just for convencience. On application a column vector
%> of values of @f$ \psi @f$ in the nodes needs to be provided.
%>
%> Likewise, application of the method returns the solution as a column
%> vector of function values in the nodes.
% ======================================================================
classdef NystroemCont < BIEPack.BIEPackObject
    
    properties ( Constant )
        
        %> @brief name of the method used in requirement checking
        METHOD_NAME = 'NystroemCont';
        
    end
    
    
    properties (SetAccess = protected)
        
        %> @brief integral operator with a continuous kernel
        op = [];
        
        %> @brief row vector of quadrature weights in the nodes of the mesh
        weights = [];
        
        %> @brief matrix of the linear system
        A = [];
        
        %> @brief vector for the right hand side
        b = [];
        
    end
    
    methods
        
        %> @brief Construct the method using an integral operator, a
        %> a right hand side and a vector of quadrature points.        
        %>
        %> @param op an operator suitable for application of the Nyström
        %>       method (instance of @link BIEPack.ops.LogKernelOp
        %>       LogKernelOp @endlink)
        %> @param rhs vector of nodal values in @c op.defSpace.theMesh
        %> @param weights row vector of quadrature weights, of dimension
        %>       equal to the number of nodes in the mesh.
        %> 
        function nystroem = NystroemCont( op, rhs, weights )
                        
            if ( nargin > 0 )
                nystroem.setOperator(op);
                nystroem.setRhs(rhs);  
                nystroem.setWeights(weights);
            end
            
        end
        
        
        %> @brief Set the operator used in the problem.
        %>
        %> The method checks that the integral operator meets the
        %> requirements of the Nystroem method.
        %>
        %> @param op the integral operator, instance of 
        %>    @link BIEPack.ops.ContinuousKernelOp ContinuousKernelOp  @endlink.               
        function setOperator( nystroemMeth, op )
                                    
            if ( ~ op.checkRequirements( nystroemMeth.METHOD_NAME ) )
                error('In NystroemCont method setOperators: operator does not support Nyström method.');
            end
            
            nystroemMeth.op = op;
            
            % set new state for object
            nystroemMeth.A = [];
            nystroemMeth.b = [];
            
        end
        
        
        %> @brief Sets the right hand side vector for the problem.
        %>
        %> This method should be called after setOperator.
        %>
        %> @todo The method should check that rhs has the right dimentsion
        function setRhs( nystroemMeth, rhs )
            
            nystroemMeth.b = rhs;
            
        end
        
        
        %> @brief Sets the weights of the quadrature rule to be used.
        %>
        %> This method should be called after setOperator.
        %>
        %> @todo The method should check that weights has the right dimentsion
        function setWeights( nystroemMeth, weights )
            
            if ( length(weights) ~= nystroemMeth.op.defSpace.theMesh.numNodes )
                error('In NystroemCont method setWeights: number of weights does not match number of nodes.');
            end
            nystroemMeth.weights = weights;
            
        end
        
        
        %> @brief Assembles the linear system matrix for the given problem         
        function assembleMatrix( nystroemMeth )
            
            nystroemMeth.A = nystroemMeth.op.getImplementation(nystroemMeth.METHOD_NAME)...
                * diag(nystroemMeth.weights);
            
        end                

        
        %> @brief apply the method to the given operator and right hand side
        %>
        %> @retval the vector of nodal values of the solution
        function phi = apply( nystroemMeth )
            
            if ( isempty( nystroemMeth.op ) )
                error('In NystroemLog method apply(): operator matrix is unset. ');
            elseif ( isempty( nystroemMeth.b ) )
                error('In NystroemLog method apply(): right hand side vector is unset. ');
            end
            
            if ( isempty(nystroemMeth.A) )
                nystroemMeth.assembleMatrix;
            end
            
            N = nystroemMeth.op.defSpace.dimension;
            phi = (eye(N) - nystroemMeth.A) \ nystroemMeth.b;
            
        end
        
        
    end
    
end
