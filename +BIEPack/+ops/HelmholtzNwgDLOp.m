%> @file HelmholtzNwgDLOp.m
%> @brief File containing the BIEPack.ops.HelmholtzNwgDLOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The double layer operator for the Helmholtz equation in a 
%> waveguide with Neumann boundary conditions.
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself.
% ======================================================================
classdef HelmholtzNwgDLOp < BIEPack.ops.PeriodicLogOp
    % BIEPack.ops.HelmholtzNwgDLOp < BIEPack.ops.PeriodicLogOp
    
    properties
        
        %> the wave number
        waveNum = 1;

        %> the height of the waveguide
        H = 1;
        
    end
    
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with thsi type of operator are
        %> carried out by the checkRequirements function
        function dlOp = HelmholtzNwgDLOp(defSpace, rangeSpace, wavenum, H )
            
            if (nargin == 0)
                defSpace = [];
                rangeSpace = [];
            end
            
            dlOp@BIEPack.ops.PeriodicLogOp(defSpace, rangeSpace);
            
            if ( ~ isempty(wavenum) )
                dlOp.waveNum = wavenum;
            end

            if ( ~ isempty(H) )
                dlOp.H = H;
            end
            
            dlOp.randomName;
            dlOp.name = strcat('HelmholtzNwgDLOp_',dlOp.name);
            
        end
        
        %> @brief Implementation of PeriodicLogOp.evalPeriodicLogKernels
        %
        %> Evaluation of the functions defining the kernel. Implementation
        %> of the function inherited from BIEPack.ops.PeriodicLogOp                
        function [K1, K2] = evalPeriodicLogKernels(dlOp)
                        
            defMesh = dlOp.defSpace.theMesh;
            
            t = defMesh.t;
            
            defMesh.evalDerivs(2);
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
            
            % Obtain values for derivative of parametrization but
            % compensate for the interval [0, 2*pi]
            scaling = dlOp.defSpace.L/(2*pi);
            xip = scaling * defMesh.xDeriv(1,:);
            etap = scaling * defMesh.yDeriv(1,:);
            xi2p = scaling^2 * defMesh.xDeriv(2,:);
            eta2p = scaling^2 * defMesh.yDeriv(2,:);
            
            N = length(t);
            ones_h = ones(1,N);
            ones_v = ones(N,1);
            
            norm_xp = sqrt( xip.^2 + etap.^2 );

            X1 = xi.' * ones_h - ones_v * xi;
            X2 = eta.' * ones_h - ones_v * eta;
            kR = dlOp.waveNum  * sqrt( X1.^2 + X2.^2 );
            
            dot_product = (ones_v * etap) .* X1 - (ones_v * xip) .* X2;
            
            K1 = -0.5 * dlOp.waveNum^2 * dot_product .* besselj(1,kR) ./ kR;
            K1(1:N+1:end) = 0;
            
            r = t.' * ones_h - ones_v * t;
                                  
            % evaluate the gradient of the Greens function. The function g_nwg_gradient_via_periodic
            % computes the gradient with respect to x, we need y here.
            % Thus, the third and fourth arguments in the call are swapped
            % and the sign is changed for G1.            
            import BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic          
            [G1, G2] = g_nwg_gradient_via_periodic(dlOp.waveNum * dlOp.H, dlOp.waveNum * X1, ...
                                                   dlOp.waveNum * ones_v * eta, ...
                                                   dlOp.waveNum * eta' * ones_h, true);
            K2 = dlOp.waveNum^2 * 1i/4 * dot_product .* besselh(1,1, kR) ./ kR ...
                + (-dlOp.waveNum * ones_v * etap) .* G1 - (dlOp.waveNum * ones_v * xip) .* G2 ...
                - 1/(2*pi) * log( 4 * sin( r / 2 ).^2 ) .* K1;
            K2(1:N+1:end) = 1/(4*pi) * (etap .* xi2p - xip .* eta2p) ./ ( norm_xp.^2 ) ...
                + dlOp.waveNum * (-etap .* G1(1:N+1:end) - xip .* G2(1:N+1:end));
            
        end
        
        %> @brief  Implementaton of GeneralOp.checkRequirements().
        %
        %> Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements( dlOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = dlOp.checkRequirementsForPeriodicLogOp;
            else                
                isOk = false;
                % warning('In HelmholtzDLOp method checkRequirements: requested type %s is not supported.', methodName );
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( dlOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = dlOp.getPeriodicLogImplementation();
            else
                error('In HelmholtzDLOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
    end
    
end

