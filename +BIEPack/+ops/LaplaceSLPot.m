%> @file LaplaceSLPot.m
%> @brief File containing the @link BIEPack.ops.LaplaceSLPot LaplaceSLPot @endlink class.
% ======================================================================
%> @brief The single layer potential for Laplace's equation. 
%
%> This class implements discretizations of the operator
%>
%> SL phi (x) = 1/(2pi) \int_C log ( 1/|x - y| ) phi(y) ds(y), x \notin C
%>
%> Implementations as an integral operator include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) into a space of trigonometric 
%>    polynomials defined on a different smooth closed curve D.
%>  - over any curve to any curve when mapping a @link BIEPack.spaces.BemSpace 
%>    BemSpace @endlink onto another one
%>
%> The class may also be used for representing a single layer potential
%> that is to be evaluated on arbitrary points of the plane not in C. See
%> the function evalPotential() for corresponding functionality.
% ======================================================================
classdef LaplaceSLPot < BIEPack.ops.PeriodicSmoothOp
    
    properties (SetAccess = protected)
        
        %> @brief Stores a quadrature rule to be used when evaluating a potential
        %>   defined on a @link BIEPack.spaces.BemSpace BemSpace @endlink
        %>
        %> This quadrature rule is used for every integration over an element
        %> when getPotentialImplementationForBem() is called. The rule can
        %> set using the function setBemPotentialQuadRule(). If it is not
        %> set explicitely, the 5 point Gauss-Legendre rule is used.
        bemPotentialQuadRule = [];
        
    end
    
    methods
        
                
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        function slPot = LaplaceSLPot( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            elseif ( nargin == 1 )
                rangeSpace = [];
            end
            
            slPot@BIEPack.ops.PeriodicSmoothOp(defSpace,rangeSpace);
            
            slPot.randomName;
            slPot.name = strcat('LaplaceSLPot_',slPot.name);
            
        end
        
        %> @brief Matrix generation for evaluation
        %
        %> Evaluation of the kernel function of the potential in the points
        %> (X1,X2) and (Y1,Y2) with the norm of the derivative of the parametrization 
        %> in these points  given by dYNorm. X1, X2 should be column vectors
        %> of equal length, Y1, Y2 and dYNorm row vectors of equal length.
        function K = evalSmoothKernelInX(slpot,X1,X2,Y1,Y2,dYNorm) %#ok<INUSL>
            
            ones_h = ones(1,length(Y1));
            ones_v = ones(length(X1),1);

            diff1 = X1 * ones_h - ones_v * Y1;
            diff2 = X2 * ones_h - ones_v * Y2;
            R = sqrt( diff1.^2 + diff2.^2 );
            
            K = 1/(2*pi) * log( 1 ./ R ) .* ( ones_v * dYNorm );
            
        end
        
        %> @brief Implementation of the
        %> PeriodicSmoothOp.evalPeriodicSmoothKernel()
        %
        %> Obtain the evaluation points and call evalSmoothKernelInX()
        function K = evalPeriodicSmoothKernel(slpot)
            
            X1 = slpot.rangeSpace.theMesh.nodesX.';
            X2 = slpot.rangeSpace.theMesh.nodesY.';
                        
            defMesh = slpot.defSpace.theMesh;            
            defMesh.evalDerivs(1);
            
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
            
            L = slpot.defSpace.theCurve.endParam - slpot.defSpace.theCurve.startParam;
            
            % Obtain values for derivative of parametrization but
            % compensate for the interval [0, 2*pi]
            xip = L/(2*pi) * defMesh.xDeriv(1,:);
            etap = L/(2*pi) * defMesh.yDeriv(1,:);
            
            norm_xp = sqrt( xip.^2 + etap.^2 );
            
            K = slpot.evalSmoothKernelInX(X1,X2,xi,eta,norm_xp);
            
        end
        
        %> @brief Assemble matrix for the potential defined on a BemSpace
        %>
        %> The integral over the curve is split into integrals over
        %> elements. The integrals over each element and each basis function
        %> having support on this element are computed separately and added
        %> to the matrix.
        function A = getPotentialImplementationForBem(slpot,X1,X2)
           
            % collocation points are the nodes of the mesh
            mesh = slpot.defSpace.theMesh;
            
            A = zeros( length(X1), slpot.defSpace.dimension);
            
            % define quadrature points and weights
            if ( isempty(slpot.bemPotentialQuadRule) )
                [quadPoints, quadWeights] = BIEPack.utils.gaussLegendreQuad(4,0,1);
                slpot.setBemPotentialQuadRule(quadPoints,quadWeights);
            else
                quadPoints = slpot.bemPotentialQuadRule.points;
                quadWeights = slpot.bemPotentialQuadRule.weights;
            end
            
            for elNum = 1:mesh.numElements
                
                globalDofs = slpot.defSpace.localToGlobalDOFMap( elNum ).globalDOF;

                % find coordinates and tangent for quadrature points
                elementLength = mesh.endParameter(elNum) - mesh.startParameter(elNum);
                tau = mesh.startParameter(elNum) + quadPoints * elementLength;
                [quadX, quadY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( tau, 0 );
                [dX, dY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( tau, 1 );
                tangNorm = elementLength * sqrt(dX.^2 + dY.^2);

                % evaluate kernel
                K = slpot.evalSmoothKernelInX(X1,X2,quadX,quadY,tangNorm);

                % evaluate the basis functions with support on this element
                shapeFuncs = slpot.defSpace.evalShapeFunctions( elNum, quadPoints.' );

                % compute integrals and add to matrix
                A(:,globalDofs) = A(:,globalDofs) + K * (shapeFuncs .* ( quadWeights.' * ones(1,length(globalDofs),1) ) );
                
            end
            
        end
        
        %> @brief Obtain a discretized version of the potential operator 
        % 
        %> The method returns a matrix representing the approximation of
        %> the integral over C by the composite trapezoidal rule.
        %> Multiplication of this matrix by a column vector of values of
        %> the density in the mesh points of C gives an approximation of
        %> the potential in the points given by the coordinates X1,X2 in 
        %> the plane. These parameters should be column vectors of equal length.
        function A = getPotentialImplementation(slpot,X1,X2)
                        
            if ( BIEPack.utils.derivedFrom(slpot.defSpace, 'BIEPack.spaces.TrigPol' ) )
                % The density is a trigonometric polynomial in parameter
                % space. Compute the integral via composite trapeoidal
                % rule with the nodes as quadrature points.
                        
                defMesh = slpot.defSpace.theMesh;            
                defMesh.evalDerivs(1);
            
                xi = defMesh.nodesX;
                eta = defMesh.nodesY;
            
                % Obtain values for derivative of parametrization but
                % compensate for the interval [0, 2*pi]
                xip = slpot.defSpace.L/(2*pi) * defMesh.xDeriv(1,:);
                etap = slpot.defSpace.L/(2*pi) * defMesh.yDeriv(1,:);
            
                norm_xp = sqrt( xip.^2 + etap.^2 );
                
                A = 2*pi / slpot.defSpace.dimension * slpot.evalSmoothKernelInX(X1,X2,xi,eta,norm_xp);
                        
            elseif ( BIEPack.utils.derivedFrom(slpot.defSpace, 'BIEPack.spaces.BemSpace' ) )
                % The density is a finite element function. Evaluate the
                % potential for all evaluation points and all basis
                % functions as a sum of integrals over the elements.
                
                A = getPotentialImplementationForBem(slpot,X1,X2);
                
            else
                                
                error( 'In LaplaceSLPot.getPotentialImplementation(): no method to evaluate this potential for this type of definition space.' );
                
            end
            
        end
        
        %> @brief Evaluate the potential at arbitrary points in space. 
        %
        %> The parameters X1 and X2 should be of the same size and the result is
        %> also of this size. The argument phi is a column vector of values of
        %> the density in the mesh points of the mesh on C.
        %>
        %> The potential is evaluated by the composite trapezoidal rule.
        function P = evalPotential(slpot,X1,X2,phi)
            
            P_size = size(X1);
            
            x1 = X1(:);
            x2 = X2(:);
            
            A = getPotentialImplementation(slpot,x1,x2); 
            
            P = A * phi;
            P = reshape(P,P_size);
            
        end
        
        %> @brief Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(laplaceSLPot, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = laplaceSLPot.checkRequirementsForPeriodicSmoothOp();
            else         
                isOk = false;
                % warning('In LaplaceSLPot method checkRequirements: requested type %s is not supported.', optype);
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %>
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( laplaceSLPot, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = laplaceSLPot.getPeriodicSmoothImplementation();
            else
                error('In LaplaceSLPot method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
        %> @brief Set the quadrature rule to be used for potential evaluations when
        %>    defined on a BemSpace.
        %>
        %> @param points a row vector of quadrature points in the interval [0,1].
        %> @param weights corresponding quadrature weights, same size as @c points.
        %>
        function setBemPotentialQuadRule( laplaceSLPot, points, weights )
            
            laplaceSLPot.bemPotentialQuadRule = struct('points',points,'weights',weights);
            
        end
                
    end
        
end

