%> @file HelmholtzNwgSLOp.m
%> @brief File containing the BIEPack.ops.HelmholtzNwgSLOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The single layer operator for the Helmholtz equation in a 
%> waveguide with Neumann boundary conditions. 
%
%> This class implements discretizations of the operator
%>
%> S phi (x) = \int_C G(x, y) phi(y) ds(y)
%>
%> where G is the Green's function for the Helmholtz equation in a 
%> waveguide with Neumann boundary condtions on both boundaries. The
%> waveguide is the strip \RR \times (0, H).
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself.
% ======================================================================
classdef HelmholtzNwgSLOp < BIEPack.ops.PeriodicLogOp
    % BIEPack.ops.HelmholtzSLOp < BIEPack.ops.PeriodicLogOp
    
    properties ( Constant )
        
        %> Euler's constant appearing in expressions of the kernel
        eulerC = 0.5772156649;
        
    end
    
    properties
        
        %> the wave number
        waveNum = 1;

        %> the height of the waveguide
        H = 1;

    end
    
    methods 
        
        %> @brief Construct the operator
        %>
        %> Calls the super class constructor to set the spaces. Checks
        %> whether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        %>
        %> Also sets wavenumber, the waveguide height and the number of 
        %> terms to use in Green's function evaluations if provided.
        function slOp = HelmholtzNwgSLOp(defSpace, rangeSpace, wavenum, H)
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            slOp@BIEPack.ops.PeriodicLogOp(defSpace, rangeSpace);
            
            if ( ~ isempty(wavenum) )
                slOp.waveNum = wavenum;
            end

            if ( ~ isempty(H) )
                slOp.H = H;
            end
            
            slOp.randomName;
            slOp.name = strcat('HelmholtzNwgSLOp_', slOp.name);
            
        end
        
        %> @brief Implementation of PeriodicLogOp.evalPeriodicLogKernels
        %
        %> Evaluation of the functions defining the kernel. Implementation
        %> of the function inherited from BIEPack.ops.PeriodicLogOp                
        function [K1, K2] = evalPeriodicLogKernels(slOp)
                        
            curve = slOp.defSpace.theCurve;
            
            t = slOp.defSpace.theMesh.t;
            xi = slOp.defSpace.theMesh.nodesX;
            eta = slOp.defSpace.theMesh.nodesY;
            
            [xip, etap] = curve.eval(t,1);
            % scale for parametrization over [0,2*pi]
            xip = slOp.defSpace.L/(2*pi) * xip;
            etap = slOp.defSpace.L/(2*pi) * etap;
            
            N = length(t);
            ones_h = ones(1,N);
            ones_v = ones(N,1);
            
            norm_xp = sqrt( xip.^2 + etap.^2 );
            norm_xp_matrix = ones_v * norm_xp;

            X1 = xi.' * ones_h - ones_v * xi;
            X2 = eta.' * ones_h - ones_v * eta;
            kR = slOp.waveNum * sqrt( X1.^2 + X2.^2 );
                        
            K1 = -0.5 * besselj(0,kR) .* norm_xp_matrix;
            
            r = t.' * ones_h - ones_v * t;
            
            import BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic

            K2 = g_nwg_via_periodic(slOp.waveNum * slOp.H, slOp.waveNum * X1, ...
                                    slOp.waveNum * eta.' * ones_h,  ...
                                    slOp.waveNum * ones_v * eta) .* norm_xp_matrix ...
                - 1/(2*pi) * log( 4 * sin( r/2 ).^2 ) .* K1;
            K2(1:N+1:end) = ( 1i/4 - slOp.eulerC / (2*pi) ...
                - 1/(2*pi) * log( slOp.waveNum/2 * norm_xp ) ) .* norm_xp...
                + g_nwg_via_periodic(slOp.waveNum * slOp.H, zeros(1, N), ...
                                     slOp.waveNum * eta, slOp.waveNum * eta, true) .* norm_xp;
            
        end
        
        %> @brief  Implementaton of GeneralOp.checkRequirements().
        %
        %> Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements( helmholtzSLOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = helmholtzSLOp.checkRequirementsForPeriodicLogOp;
            else                
                isOk = false;
                % warning('In HelmholtzSLOp method checkRequirements: requested type %s is not supported.', methodName );
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( helmholtzSLOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = helmholtzSLOp.getPeriodicLogImplementation();
            else
                error('In HelmholtzSLOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
    end
    
end

