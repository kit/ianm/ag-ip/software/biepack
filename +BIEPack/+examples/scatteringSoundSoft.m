%> @file scatteringSoundSoft.m
%> @brief Scattering an acoustic wave from a sound soft obtstacle.
% ======================================================================
%> This example function computes the solution to a scattering problem
%>
%>    \Delta u + k^2 u = 0  in R^2 \ \overline{D} <br>
%>                   u = 0  on \partial D         <br>
%>    u^s = u - u^i  is radiating
%>
%> for a given domain D and incident field u^i.
%>
%> An combined potential ansatz for u^s as 
%>
%>  u^s = ( DL - i k SL ) \phi
%>
%> reduces the problem to the boundary integral equation
%>
%>   phi + 2 D phi - 2i k S phi = -2 u^i
%>
%> This equation is solved via the Nyström method for periodic kernels with
%> logarithmic singularity.
%
%> The argument to the function may either be an integer specifying the
%> number of grid points or @link ParamCurveMesh BIEPack.meshes.ParamCurveMesh @endlink.
%> The function returns the coefficient vector @a phi of the solution in the
%> space @a trigPols of class @link TrigPol BIEPack.spaces.TrigPol @endlink
function [phi, trigPols] = scatteringSoundSoft(N)

if ( nargin == 0 )
    N = 32;
end

% import the BIEPack package
import BIEPack.*;

% set the wavenumber
%kappa = 4*sqrt(2);
kappa = 8;

if ( BIEPack.utils.derivedFrom( N, 'BIEPack.meshes.ParamCurveMesh' ) )
    % the mesh has been passed as a parameter
    mesh = N;
else
    % construct a mesh on the curve
    %curve = geom.Kite;
    curve = geom.Ellipse([0;0],1,2);
    mesh = meshes.ParamCurveMesh(curve,N);
end

% construct a space of trigonometric polynomials
trigPols = spaces.TrigPol( mesh );

% define the two boundary operators
S = ops.HelmholtzSLOp( trigPols, trigPols, kappa );
D = ops.HelmholtzDLOp( trigPols, trigPols, kappa );

% define the complete operator on the lefthand side of the equation
theOp = 2 * D + (-2i)*kappa * S;

% evaluate the incident field on the boundary curve. We take a plane wave
d = [ 3; 4 ] / 5;
u_i = exp( 1i * kappa * ( d(1) * mesh.nodesX + d(2) *  mesh.nodesY ) ).';

% define the method
nystroemMethod = methods.NystroemPeriodicLog(theOp,-2*u_i);

% compute the solution
phi = nystroemMethod.apply;

end