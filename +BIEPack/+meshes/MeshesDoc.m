%> @file MeshesDoc.m
%> @brief The documentation of the @a meshes package
%
% ======================================================================
%
%> @namespace BIEPack::meshes
%> @brief Classes representing meshes defined on boundary curves.
%>
%> The package @a meshes provides discretizations of objects from the
%> @link BIEPack.geom geom @endlink package. Additionally, its job is to
%> handle mesh refinement. There is a class RefinementRelation representing
%> relations between meshes where one is derived from the other by
%> refinement.
%>
%> Every mesh consists of nodes and elements connecting the nodes. The 
%> abstract base class Mesh represents such a mesh, but leaves the exact
%> nature of the elements open.
%>
%> The class BemMesh makes this data structure concrete for a curve: The
%> elements are curve segments, the nodes are end points of these elements.
%> Every curve in BIEPack ultimately consists of individual @link BIEPack.geom.ParamCurve
%> ParamCurve @endlink objects. Each element is contained in exactly one 
%> such @link BIEPack.geom.ParamCurve ParamCurve @endlink. The
%> class also provides some support for boundary element methods.
%>
%> If the curve consists just of a single ParamCurve object, a
%> ParamCurveMesh can be used instead. This class also provides access to
%> tangential vectors and higher derivatives of the parametrization in the
%> nodes. Such a mesh can be guaranteed to have uniformly spaced node points
%> in parameter space.
%>
%> A RefinementRelation describes how one mesh has been generated from
%> another mesh by mesh refinement.
%
% ======================================================================

