%> @file laplaceDirichletEllipse.m
%> @brief Contains the laplaceDirichletEllipse() example function
%
% ======================================================================
%
%> @brief Function for solving a Dirichlet problem for Laplace's equation in an ellipse.
%>
%> Consider an ellipse @a D with half axes @a a, @a b. One can try to solve
%> the Dirichlet problem for Laplace's equation in @a D by making an ansatz
%> for the solution as a double layer potential,
%> @f[
%>   u(x) = \int_{\partial D} \frac{\partial \Phi(x,y) }{\partial n(y) } \,
%>   \varphi(y) \, \mathrm{d}s(y) \, , \qquad x \in D \, .
%> @f]
%> This will lead to an integral equation for @f$\varphi@f$ on the boundary
%> of @a D,
%> @f[
%>   \varphi(x) - 2 \, \int_{\partial D} \frac{\partial \Phi(x,y) }{\partial n(y) } \,
%>   \varphi(y) \, \mathrm{d}s(y) = -2 f(x) \, , \qquad x \in \partial D,
%> @f]
%> where @a f denotes the boundary values of @a u.
%>
%> As the boundary of the ellipse is smooth, the kernel of the double layer
%> potential is infinitely often differentiable. It can be explicitely calculated 
%> (see Kress, Linear Integral Equations, Example 11.12 for details). Using 
%> the parametrization of the ellipse in polar coordinates, 
%> @f$x(t) = ( a \cos(t) , b \sin(t))^\top@f$, @f$t \in (-\pi,\pi]@f$, and
%> Fourier expansions of all functions involved, one may calculate explicit
%> solutions. We here use the case discussed in the example in Kress' book
%> cited above where
%> @f[
%>   \varphi(x(t)) = \mathrm{e}^{\cos(t)} \, \cos(\sin(t))
%> @f]
%> and the right hand side in the integral equation is
%> @f[
%>   \psi(t) = \varphi(t) + \mathrm{e}^{c \, \cos(t)} \, \cos( c \, \sin(t) )
%>   \, , \qquad c = \frac{a - b}{a + b} \, .
%> @f]
%>
%> As the integral operator has a continuous kernel, various methods can be
%> applied for solving the integral equation. In this example we show how to
%> switch between two methods, the approximation by degenerate kernels and a
%> Nyström method based on the composite trapezoidal rule for periodic
%> functions.
%>
%> The integral equation is solved by the selected method. A plot of the 
%> numerical and exact solutions in the mesh points and the error in a 
%> logarithmic scale is created. Approximation by degenerate kernels gives
%> convergence order 2 while the Nyström method gives exponential
%> convergence.
%>
%> @param N number of points to use (default is 20). 
%> @param methodname string identifying the method to use. Valid values are
%>     @a degKerApprox or @a nystroem (default is @a degKerApprox).
%>
%> @retval phi a vector with values of the approximate solution
%> @retval phi_exakt a vector with values of exact solution
%> @retval t a vector with the paramerter values of the grid points.
%>

function laplaceDirichletEllipse(N, methodname)

import BIEPack.*;

% set number of points in the mesh if not provided
if ( nargin == 0 )
    N = 20;
end

% size of the ellipse half axes
a = 4;
b = 1;
c = (a - b) / (a + b);

% method to be applied
if ( nargin < 2 )
    methodname = 'degKerApprox';
end
if ( ~ strcmp( methodname, 'degKerApprox' ) ...
        && ~ strcmp( methodname, 'nystroem' ) )
    error('Invalid method name %s\n',methodname);
end

% define an ellipse with a uniform mesh in parameter space
curve = geom.Ellipse( [0;0], a, b );
mesh = meshes.ParamCurveMesh( curve, N );

% define space and operator depending on method
if ( strcmp( methodname, 'degKerApprox' ) )
    theSpace = spaces.BemSpace( mesh, spaces.BemSpace.PIECEWISE_LINEAR );
else
    theSpace = spaces.TrigPol( mesh );
end
intOp = 2 * ops.LaplaceDLOp(theSpace, theSpace);

% definition of right-hand side
t = mesh.t.';
psi = exp(cos(t)) .* cos(sin(t)) + exp( c * cos(t) ) .* cos( c* sin(t) );

% set up method
if ( strcmp( methodname, 'degKerApprox' ) )
    theMethod = methods.DegenerateKernelApprox( intOp, psi );
else
    theMethod = methods.NystroemPeriodicLog( intOp, psi );
end

% solve the problem
phi = theMethod.apply();

figure, plot( t,phi,t, exp(cos(t)) .* cos(sin(t)) )
figure, semilogy( t, abs(phi - exp(cos(t)) .* cos(sin(t)) ) )

end


