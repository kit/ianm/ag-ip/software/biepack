%> @file BemSpace.m
%> @brief Contains the spaces.BemSpace.m class.
% =====================================================================
%> @brief A BemSpace requires a BIEPack.meshes.BemMesh that is defined on some 
%> curve. It can then define a space of finite element functions of various
%> types on this mesh.
%>
%> At the moment, the class supports the following types of functions:
%>
%>   Piecewise constants: constant functions on each element, each degree of 
%>   freedom is associated with one element. The overall function is not
%>   continuous.
%>
%>   Piecewise linears: Overall continous functions, linear on each element. 
%>   Each degree of freedom associated with one node.
%>
%> @todo: implement higher order elements
%>
%> Each element in the mesh can be mapped to the reference intervall [0,1]
%> by the map through the element parametrisation
%>
%> \f$ \gamma(t) = \eta( sp + t (ep - sp) ),   t \in [0,1] \f$
%>
%> Here, \f$\eta\f$ is the parametrisation of the ParamCurve, sp is the
%> start parameter and ep the end parameter of this element. The functions
%> in a BemSpace restricted to one element and pulled back to [0,1] by this 
%> parametrisation are polynomials.
%>    
classdef BemSpace < BIEPack.spaces.SpaceOnCurve

% functionality:
% - construct the space
% - obtain basis functions with support on a certain element
% - evaluate a basis function on certain points in an element
% - project a function defined on the curve onto the space by interpolation
% - evaluate any function from this space on any point on the boundary
% - based on a refined mesh, construct a refined space and a relation
%   between the spaces.
%
    
    properties (SetAccess = protected)
       
        %> @brief Array containing the information for mapping a local 
        %> degree of freedom on a single element of the BemMesh onto a 
        %> global degree of freedom in the space. 
        %>
        %> This structure contains two fields: 
        %>  - the 'order' is the number of global degrees of freedom an
        %>    element is associated with,
        %>  - 'globalDOF' is a vector of length 'order' which for each local
        %>    DOF on the element contains the index of the corresponding 
        %>    global DOF.
        %>
        %> Elements of order 1 are those used in a space of piecewise
        %> constants. Elements of order 2 and higher are used if the
        %> functions in the space are at least continuous piecewise linears.
        localToGlobalDOFMap = struct( 'order', {}, 'globalDOF', {} );
        

        %> @brief Vector containing the global degrees of freedom that are
        %> associated with the nodes of the mesh.
        nodalDOFs = [];
        
        %> @brief Information on collocation points.
        %>
        %> A function in the BemSpace is uniquely defined by its values in
        %> the collocation points. For a space containing only piecewise
        %> constant or piecewise linear functions, each collocation point is
        %> uniquely associated with one degree of freedom. As higher order
        %> elements have not yet been implemented, this issue is as of yet
        %> undecided for higher oder elements.
        %>
        %> For a space of piecewise constants, the collocation points will
        %> be the element mid-points, for a space of piecewise linears, they
        %> are the nodes of the mesh.
        %>
        %> This struct contains the following fields:
        %> - the X and Y coordinates of the collocation points,
        %> - the left-hand (interior) 'angle' of the boundary curve at this
        %>   point,
        %> - 'normalExists' which is true if a normal vector is well-defined
        %>   at this collocation point,
        %> - 'nX' and 'nY', the coordinates of said unit normal vector,
        %> - 'type' is a field with the value 0 for a collocation
        %>   point which is a node or 1 for a collocation point
        %>   in the interior of an element (in case of a nodal point, the
        %>   index of the collocation point is the same as the index of the
        %    node in the mesh),
        %> - 'elnum' contains the element number for a collocation point
        %>   in the interior of an element,
        %> - 'param' is the parameter value on the curve segment for a 
        %>   collocation point in the interior of an element.
        collocPoints = struct( 'X', [], 'Y', [], ...
                               'angle', [], 'normalExists', [], 'nX', [], 'nY', [], ...
                               'type', [], 'elnum', [], 'param', [] );
    end
    
    
    properties (Constant)
        
        %> @brief The value to be used for 'type' if the space is to contain 
        %> piecewise constant functions.
        PIECEWISE_CONST = 1;
                
        %> @brief The value to be used for 'type' if the space is to contain 
        %> continuous, piecewise linear functions.
        PIECEWISE_LINEAR = 2;
        
    end

    
    methods
        
        %> @brief This method constructs a BemSpace object. If an object 
        %> aMesh of class BIEPack.meshes.BemMesh is passed, this will be 
        %> set as the mesh, the space is based on, using the setMesh method.
        %> If additionally, the parameter type is passed, the degrees of 
        %> freedom are distributed by a all to distributeDOFs().
        %>
        %> @param aMesh The mesh that the space will be based on.
        %> @param type Determines how the global degrees of freedom will be
        %> distributed, as described in the distributeDOFs method.
        %>
        %> @retval bemSpace The newly constructed BemSpace object.
        function bemSpace = BemSpace( aMesh, type )
                 
            bemSpace@BIEPack.spaces.SpaceOnCurve( aMesh );
            
            if ( nargin > 1 )
                bemSpace.distributeDOFs( type );
            end
            
            bemSpace.randomName;
            bemSpace.name = strcat('BemSpace_',bemSpace.name);
            
        end
        
        %> @brief Set the mesh, the BemSpace object is based on. 
        %>
        %> The argument must be an object of class BIEPack.meshes.BemMesh
        function setMesh( bemSpace, aMesh )
            
            if ( ~ BIEPack.utils.derivedFrom(aMesh, 'BIEPack.meshes.BemMesh' ) )
                error( 'In BemSpace.setMesh(): argument is not a BIEPack.meshes.BemMesh' );
            end
            
            bemSpace.theCurve = aMesh.theCurve;
            bemSpace.theMesh = aMesh;
            
        end
        
        %> @brief Returns indeces of collocation points in relation to a specific element.
        %>
        %> For a given element with index @c elnum the function returns three
        %> arrays with indeces of collocation points:
        %> - @a interior: collocation points interior to the element.
        %> - @a boundary: collocation points which are nodal points of the mesh and endpoints of the element.
        %> - @a away: collocation points with a positive distance from the element.#
        function [interior, boundary, away] = getCollocPointsForElement(bemSpace, elnum)

            % initialize away with all collocation points. Points on the element
            % need to be removed later.
            away = 1:size(bemSpace.collocPoints.X,1);
                
            % are their nodal DOFs on this element?
            if ( ~ isempty(bemSpace.nodalDOFs) )
                boundary = bemSpace.theMesh.elements(2:bemSpace.theMesh.elements(1,elnum)+1,elnum);
            else
                boundary = [];
            end

            % are their interior collocation points on this element?
            interiorIndex = ( bemSpace.collocPoints.type == 1 & bemSpace.collocPoints.elnum == elnum );
            interior = away(interiorIndex);
            
            % remove nodes on the element from away.
            onThisEl = [ boundary interior ];
            away(onThisEl) = [];
            
        end                   
        
        %> @brief Distributes the global degrees of freedom onto the nodes 
        %> and elements.
        %>
        %> If type is equal to BemSpace.PIECEWISE_CONST, each global degree
        %> of freedom is associated with each element. There are no nodal
        %> DOFs.
        %>
        %> If type is equal to BemSpace.PIECEWISE_LINEAR, each global degree
        %> of freedom is associated with a node. The first node is
        %> in the BemMesh.elements array for that element is associated with
        %> the first local degree of freedom, the second node is associated
        %> with the second degree of freedom.
        %> 
        %> @param type Determines how the global degrees of freedom will be
        %> distributed.
        function distributeDOFs( bemSpace, type )
                        
            if ( isempty( bemSpace.theMesh ) )
                error( 'In BemSpace.distributeDOFs(): Mesh must be set.' );
            end
            
            if ( type == bemSpace.PIECEWISE_CONST )
                
                bemSpace.dimension = bemSpace.theMesh.numElements;
                bemSpace.nodalDOFs = [];
                
                for j=1:bemSpace.theMesh.numElements
                    bemSpace.localToGlobalDOFMap(j).order = 1;
                    bemSpace.localToGlobalDOFMap(j).globalDOF = j;
                end
                
                collocPointsParam = ( bemSpace.theMesh.endParameter + bemSpace.theMesh.startParameter ) / 2;
                normalDir = sign( bemSpace.theMesh.endParameter - bemSpace.theMesh.startParameter );
                bemSpace.collocPoints.X = zeros(bemSpace.dimension,1);
                bemSpace.collocPoints.Y = zeros(bemSpace.dimension,1);
                bemSpace.collocPoints.angle = pi * ones(bemSpace.dimension,1);
                bemSpace.collocPoints.normalExists = true(bemSpace.dimension,1);
                bemSpace.collocPoints.nX = zeros(bemSpace.dimension,1);
                bemSpace.collocPoints.nY = zeros(bemSpace.dimension,1);
                bemSpace.collocPoints.type = ones(bemSpace.dimension,1);
                bemSpace.collocPoints.elnum = (1:bemSpace.theMesh.numElements).';
                bemSpace.collocPoints.param = collocPointsParam.';
                
                for j=1:length( bemSpace.theMesh.theParamCurves )
                    
                    index = ( bemSpace.theMesh.curveIndexOfElement == j );
                    
                    [X,Y] = bemSpace.theMesh.theParamCurves{j}.eval( collocPointsParam(index), 0 );
                    bemSpace.collocPoints.X(index) = X;
                    bemSpace.collocPoints.Y(index) = Y;
                    
                    [nX,nY] = bemSpace.theMesh.theParamCurves{j}.normal( collocPointsParam(index) );
                    normalNorm = sqrt( nX.^2 + nY.^2 );
                    bemSpace.collocPoints.nX(index) = normalDir(index) .* nX ./ normalNorm;
                    bemSpace.collocPoints.nY(index) = normalDir(index) .* nY ./ normalNorm;
                    
                end
                
            elseif (type == bemSpace.PIECEWISE_LINEAR )
                
                bemSpace.dimension = bemSpace.theMesh.numNodes;
                bemSpace.nodalDOFs = 1:bemSpace.dimension;
                
                for j=1:bemSpace.theMesh.numElements
                    bemSpace.localToGlobalDOFMap(j).order = 2;
                    bemSpace.localToGlobalDOFMap(j).globalDOF = ...
                        bemSpace.theMesh.elements(2:3,j).';
                end
                
                bemSpace.collocPoints.X = bemSpace.theMesh.nodesX.';
                bemSpace.collocPoints.Y = bemSpace.theMesh.nodesY.';
                bemSpace.collocPoints.angle = bemSpace.theMesh.angleAtNode.';
                bemSpace.collocPoints.normalExists = bemSpace.theMesh.normalAtNodeExists.';
                bemSpace.collocPoints.nX = bemSpace.theMesh.normalAtNodeX.';
                bemSpace.collocPoints.nY = bemSpace.theMesh.normalAtNodeY.';
                bemSpace.collocPoints.type = zeros(bemSpace.dimension,1);
                bemSpace.collocPoints.elnum = zeros(bemSpace.dimension,1);
                bemSpace.collocPoints.param = zeros(bemSpace.dimension,1);
                
            else
                error( 'In BemSpace.distributeDOFs(): Unknown type of elements.' );
            end
            
        end
        
        %> @brief Evaluate the pullback of a basis function to an element
        %>
        %> A shape function is the restriction of a basis function to an
        %> element applied after the inverse element parametrization, i.e.
        %> as a function on the reference interval [0,1]. The function
        %> evaluates the shape functions for all local degrees of freedom
        %> of the element elNum at the parameter values tau. Here, tau is a
        %> column vector with entries in the interval [0,1].
        %>
        %> The function returns a length(tau) x m matrix, where m is the
        %> number of degrees of freedom associated with the element.
        function u = evalShapeFunctions( bemSpace, elNum, tau )
          
            if ( bemSpace.localToGlobalDOFMap(elNum).order == 1 )
                
                % If the order is 1, only a piecewise costant function needs
                % to be evaluated
                u = ones( length(tau), 1 );
                
            else
                
                u = zeros( length(tau), bemSpace.localToGlobalDOFMap(elNum).order );
                
                % Piecewise linear shape functions
                u(:,1) = tau;
                u(:,2) = 1 - tau;
                
            end
            
        end
        
        %> @brief Evaluate a basis function in a collocation point.
        function u = evalBasisFunctionsInCollocPoints( bemSpace, dofs )
            
            [nodalDofs, nodalIndeces] = intersect(dofs, bemSpace.nodalDOFs);
            [elDofs, elIndeces] = setdiff(dofs, nodalDofs);
            
            u = zeros( bemSpace.dimension, length(dofs) );
            
            if ( ~ isempty( nodalDofs ) )
                u( bemSpace.dimension * (nodalIndeces.' - 1) + nodalDofs ) = 1.0;
            end
            
            % The following lines are correct because the only element dofs
            % implemented are constants. Correct for higher order elements.
            if ( ~ isempty( elDofs ) )
                u( bemSpace.dimension * (elIndeces.' - 1) + elDofs) = 1.0;
            end
            
        end
        
        %> @brief Evaluates a function in the space on points given by the 
        %> element number and the parameter t of element parametrisation.
        %>
        %> The column vector coeffs specifies the values for the degrees of 
        %> freedom for the function to be evaluated. The parameters
        %> elementNum and paramVal are of equal size and define the points
        %> on which evaluation is to take place. The function returns a
        %> matrix y of the same size.
        %>
        %> @param coeff Specifies the values for the degrees of freedom for
        %> the function to be evaluated.
        %> @param elementNum Matrix that contains the numbers of the elements, 
        %> on which the function will be evaluated. elementNum and paramVal 
        %> must be of equal size.
        %> @param paramVal Matrix that contains the values of the parameter 
        %> t at which the respective element parameterisation will be
        %> evaluated. paramVal and elementNum must be of equal size.
        %> 
        %> @retval y Matrix of size equal to the size of elementNum (or 
        %> paramVal respectively). Contains the values of the 
        %> function.
        function y = evalOnElement( bemSpace, coeffs, elementNum, paramVal )
            
            elem = elementNum(:);
            t = paramVal(:);
            y = zeros(size(t));
            
            for j=1:length(elem)
                if ( bemSpace.localToGlobalDOFMap(elem(j)).order == 1 )
                    y(j) = coeffs( bemSpace.localToGlobalDOFMap(elem(j)).globalDOF );
                else
                    vals = coeffs( bemSpace.localToGlobalDOFMap(elem(j)).globalDOF );
                    y(j) = (1.0 - paramVal(j) ) * vals(1) + paramVal(j) * vals(2);
                end
            end
            
            y = reshape(y,size(elementNum));
            
        end
        
        %> @brief Compute a column vector of coefficients representing the
        %> interpolant of the function f in the BemSpace bemSpace. 
        %>
        %> The input parameter f is a function handle for a function of the form
        %>   y = function f(x1,x2)
        %> where x1,x2 max be row vectors of space coordinates and y is a 
        %> row vector of corresponding function values of the same size.
        %>
        %> @todo Check the use of collocation points in this function.
        %> 
        %> @param f Function that will be interpolated in the bemSpace.
        %>
        %> @retval Vector of the coefficients that represent the function f. 
        function a = interpolateSpaceFunction( bemSpace, f)
            
            a = zeros( bemSpace.dimension, 1);
            
            % evaluate f in all nodes, if nodes are associated with degrees
            % of freedom.
            if ( ~ isempty( bemSpace.nodalDOFs) ) 
                a( bemSpace.nodalDOFs ) = f( bemSpace.theMesh.nodesX, bemSpace.theMesh.nodesY).';
            end
            
            % find coefficients for all interior degrees of freedom
            % At the moment, these are only those for piecewise constant
            % functions. If one element has them, all have them.
            if ( bemSpace.localToGlobalDOFMap(1).order == 1 )
                for j=1:bemSpace.theMesh.numElements
                    t = ( bemSpace.theMesh.startParameter(j) + bemSpace.theMesh.endParameter(j) ) / 2;
                    [x1,x2] = bemSpace.theMesh ...
                        .theParamCurves{bemSpace.theMesh.curveIndexOfElement(j) }  ...
                        .eval(t,0);
                    a(j) = f( x1, x2 );
                end
            end
            
        end
        
    end
    
end
