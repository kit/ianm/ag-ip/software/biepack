%> @file Rectangle.m
%> @brief Contains the geom.Rectangle class.
% ======================================================================
%> @brief A rectangular boundary curve formed by four straight lines.
classdef Rectangle < BIEPack.geom.PiecewiseCurve
    
    properties
    end
    
    methods
        
        %> @brief Constructs a paraxial rectangle from two given opposite corner
        %> points as a PiecewiseCurve containing four StraightLine objects.
        %> Lower-left and upper-right corner point are initialized as given.
        %> A random name starting with 'Rectangle_' is set.
        %>
        %> @param lowerleft Bottom-left corner of the rectangle.
        %> @param upperright Top-right corner of the rectangle.
        %>
        %> @retval rectangle The newly constructed Rectangle object.
        function rectangle = Rectangle( lowerleft, upperright ) 
            
            % initialize the four boundary parts
            l_cor = min( [ lowerleft(1), upperright(1) ] );
            r_cor = max( [ lowerleft(1), upperright(1) ] );
            b_cor = min( [ lowerleft(2), upperright(2) ] );
            t_cor = max( [ lowerleft(2), upperright(2) ] );
            
            s1 = BIEPack.geom.StraightLine( [ l_cor; b_cor ], [r_cor; b_cor] );
            s2 = BIEPack.geom.StraightLine( [ r_cor; b_cor ], [r_cor; t_cor] );
            s3 = BIEPack.geom.StraightLine( [ r_cor; t_cor ], [l_cor; t_cor] );
            s4 = BIEPack.geom.StraightLine( [ l_cor; t_cor ], [l_cor; b_cor] );
            
            rectangle@BIEPack.geom.PiecewiseCurve( s1, 1, s2, 1, s3, 1, s4, 1 );
                        
            % set basic properties
            rectangle.randomName;
            rectangle.name = strcat('Rectangle_',rectangle.name);
            
            rectangle.closed = true;
            
        end
    end
    
end

