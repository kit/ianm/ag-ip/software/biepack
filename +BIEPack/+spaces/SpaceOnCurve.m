%> @file SpaceOnCurve.m
%> @brief The documentation of the SpaceOnCurve class.
%
% ======================================================================
%
%> @brief Abstract umbrella class for spaces of functions defined on
%> curves.
%>
%> The class stores the curve and the mesh the space is based on. It also
%> provides a property for the space dimension.
classdef SpaceOnCurve < BIEPack.BIEPackObject
    
    properties
        
        %> @brief The dimension of the space, i.e. the total number of DOFs.
        dimension = [];
        
        %> @brief The geom.Curve object the space is defined on.
        theCurve = [];
        
        %> @brief The @link BIEPack.meshes.BemMesh BemMesh @endlink 
        %> object the space is defined on.
        theMesh = [];
    end
    
    methods
        
        %> @brief Initializes the mesh and the curve, using the setMesh 
        %> function.
        %> 
        %> @param aMesh The mesh that the space is based on.
        %>
        %> @retval spaceOnCurve The newly constructed SpaceOnCurve object.
        function spaceOnCurve = SpaceOnCurve( aMesh )
                                   
            if ( nargin > 0 && ~isempty(aMesh) )
                spaceOnCurve.setMesh( aMesh );
            end
            
        end
        
        %> @brief Sets the properties of the mesh and the curve.
        %>
        %> Sets the @link BIEPack.meshes.BemMesh BemMesh @endlink object the 
        %> SpaceOnCurve object is based on.
        %>
        %> @param aMesh The mesh that the space is based on.
        function setMesh( spaceOnCurve, aMesh )
            
            if ( ~ BIEPack.utils.derivedFrom(aMesh, 'BIEPack.meshes.BemMesh' ) )
                error( 'In SpaceOnCurve.setMesh(): argument is not a BIEPack.meshes.BemMesh' );
            end
            
            spaceOnCurve.theCurve = aMesh.theCurve;
            spaceOnCurve.theMesh = aMesh;
            
        end
        
    end
    
end

