%> @file ContinuousKernelOp.m
%> @brief File containing the @link BIEPack.ops.ContinuousKernelOp 
%> ContinuousKernelOp @endlink class.
% ======================================================================
%> @brief An integral operator with a continuous kernel defined on a single
%> @link BIEPack.geom.ParamCurve ParamCurve @endlink suitable for
%> application of degenerate kernel approximation method.
%>
%> This class provides everything for application of the degenerate 
%> kernel approximation method except for the actual implementation of the
%> kernel function. An implementation is provided by the user by passing 
%> a function handle on construction. The function provided through this
%> handle must have the declaration
%> @code
%>   function K = implementationOfKernel( t, tau )
%> @endcode
%> where @a t is row vector of values of the curve parametr, @a tau is a 
%> column vector of such values and K is a matrix of size length(t) x length(tau)
%> containing the corresponding values of the kernel.
% ======================================================================
classdef ContinuousKernelOp < BIEPack.ops.DegenerateKernelOp & BIEPack.ops.SimpleContKernelOp
    
    properties ( SetAccess = protected )
        
        %> @brief function handle of the implementation of the kernel
        kernel = [];
    end
    
    
    methods
        
        %> @brief Constructs the concrete operator from a BemSpace on a 
        %> ParamCurve and an implementation of the continuous kernel.
        %>
        %> @param defSpace @link BIEPack.spaces.BemSpace BemSpace @endlink
        %>     of linear spline functions on the parameter interval.
        %>     Identical to range space.
        %> @param kernel function handle of the kernel.
        function cko = ContinuousKernelOp( defSpace, kernel )
            
            % if no arguments are given, create an empty instance.            
            if ( nargin == 0 )
                defSpace = [];
                kernel = [];
            end
            
            % call parent constructors
            cko@BIEPack.ops.DegenerateKernelOp(defSpace, defSpace);
            cko@BIEPack.ops.SimpleContKernelOp(defSpace, defSpace);
            
            % initialize kernel
            cko.setKernel( kernel );
            
        end
        
        %> @brief Sets a new implementation of the kernel function
        %>
        %> @param kernel function handle of the kernel.
        function setKernel( cko, kernel )
            
            cko.kernel = kernel;
            
        end
        
        %> @brief Set a new definition and range space
        %>
        %> @param defSpace @link BIEPack.spaces.BemSpace BemSpace @endlink
        %>     of linear spline functions on the parameter interval.
        %>     Identical to range space.
        function setSpace( cko, space )
            
            cko.setDefSpace( space );
            cko.setRangeSpace( space );
            
        end
        
                
        %> @brief Checks whether the method given by methodName can be applied to
        %> this operator. 
        %
        %> Instances of this class only support the degenerate kernel
        %> approximation method, all other methods will fail.
        %>
        %> @retval @c true if the method can be applied, @c false otherwise
        function isOk = checkRequirements( contKernelOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.DegenerateKernelApprox.METHOD_NAME ) )
                isOk = contKernelOp.checkRequirementsForDegenerateKernelOp;
            elseif ( strcmp( methodName, BIEPack.methods.NystroemCont.METHOD_NAME ) )
                isOk = contKernelOp.checkRequirementsForSimpleContKernelOp;
            else                
                isOk = false;
            end
            
        end
        
        %> @brief Obtain the implementation of the operator as a matrix.
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( contKernelOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.DegenerateKernelApprox.METHOD_NAME ) )
                A = contKernelOp.getDegenerateKernelImplementation();
            elseif ( strcmp( methodName, BIEPack.methods.NystroemCont.METHOD_NAME ) )
                A = contKernelOp.getSimpleContKernelImplementation();
            else
                error('In ContKernelOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
        
        %> @brief Evaluate the kernel function for two parameter values
        %>
        %> The call is simply passed through to the implementation of the
        %> kernel provided by the user.
        %>
        %> @param t column vector of parameter values on the curve
        %> @param tau row vector of parameter values on the curve
        %>
        %> @retval K matrix of size length(t) x length(tau)
        %> containing the corresponding values of the kernel.
        function K = evalContinuousKernel( cko, t, tau )
            
            K = cko.kernel(t,tau);
            
        end
        
        
    end
end

