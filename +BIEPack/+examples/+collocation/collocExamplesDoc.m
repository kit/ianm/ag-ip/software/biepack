%> @file collocExamplesDoc.m
%> @brief The documentation of examples for the collocation method
%
% ======================================================================
%
%> @namespace BIEPack::examples::collocation
%> @brief Example scripts and functions for the collocation method