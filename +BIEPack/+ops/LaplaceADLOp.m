%> @file LaplaceADLOp.m
%> @brief File containing the BIEPack.ops.LaplaceADLOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The adjoint double layer operator for Laplace's equation. 
%
%> This class provides discretizations of the operator 
%>
%> D* phi (x) = 1/(2pi) \int_C ( n(x) . (y - x) / |x - y|^2 ) phi(y) ds(y),
%>
%> for x on some curve D where n(x) is the outward drawn unit normal vector 
%> to D at the point x.
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself. The kernel
%>    inherits the smoothness of the curve reduced by 2 degrees.
%>
%> @todo document collocation method use
%>
%> @todo Insert requirement for use in collocation method
classdef LaplaceADLOp < BIEPack.ops.PeriodicSmoothOp & BIEPack.ops.BemContOp
    
    properties
    end
    
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        function adlOp = LaplaceADLOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            adlOp@BIEPack.ops.PeriodicSmoothOp(defSpace,rangeSpace);
            
            adlOp.randomName;
            adlOp.name = strcat('LaplaceADLOp_',adlOp.name);
            
        end
        
        %> @brief Implementation of
        %> PeriodicSmoothOp.evalPeriodicSmoothKernel()
        %
        %> Evaluation of the kernel function for an operator defined
        %> on BIEPack.spaces.TrigPol spaces.
        %> The evaluation points correspond to the grid points on each
        %> mesh.
        function K = evalPeriodicSmoothKernel(intOp)
                        
            defMesh = intOp.defSpace.theMesh;
            
            defMesh.evalDerivs(2);
            xi_t = defMesh.nodesX.';
            eta_t = defMesh.nodesY.';
            
            % Obtain values for derivative of parametrization but
            % compensate for the interval [0, 2*pi]
            xip_t = intOp.defSpace.L/(2*pi) * defMesh.xDeriv(1,:).';
            etap_t = intOp.defSpace.L/(2*pi) * defMesh.yDeriv(1,:).';
            xi2p_t = ( intOp.defSpace.L/(2*pi) )^2 * defMesh.xDeriv(2,:).';
            eta2p_t = ( intOp.defSpace.L/(2*pi) )^2 * defMesh.yDeriv(2,:).';
            
            rangeMesh = intOp.rangeSpace.theMesh;
            xi_tau = rangeMesh.nodesX;
            eta_tau = rangeMesh.nodesY;
            
            rangeMesh.evalDerivs(1);
            xip_tau = intOp.defSpace.L/(2*pi) * rangeMesh.xDeriv(1,:);
            etap_tau = intOp.defSpace.L/(2*pi) * rangeMesh.yDeriv(1,:);
            
            M = length(xi_t);
            N = length(xi_tau);
            ones_h = ones(1,N);
            ones_v = ones(M,1);
            
            norm_xp_t = sqrt( xip_t.^2 + etap_t.^2 );
            norm_xp_tau = sqrt( xip_tau.^2 + etap_tau.^2 );

            X1 = xi_t * ones_h - ones_v * xi_tau;
            X2 = eta_t * ones_h - ones_v * eta_tau;
            R = sqrt( X1.^2 + X2.^2 );
            zero_index = (R == 0);
            [zr, ~] = find(zero_index);
            
            K = 1/(2*pi) * ( xip_t * ones_h .* X2 - etap_t * ones_h .* X1 ) ./ R.^2 ...
                ./ ( norm_xp_t * ones_h ) .* ( ones_v * norm_xp_tau );
            K(zero_index) = 1/(4*pi) * ( etap_t(zr) .* xi2p_t(zr) - xip_t(zr) .* eta2p_t(zr) ) ./ norm_xp_t(zr).^2;
            
        end
        
        
        function a = evalBemAFunction( bemContOp, xpoints ) %#ok<INUSL>
           
            a = zeros(length(xpoints.X),1);
        end

        
        function K = evalBemContKFunction( bemContOp, xpoints, Y1, Y2, dY1, dY2, curve, tau ) %#ok<INUSL>
            
            % some helper variables
            N1 = length(xpoints.X);
            N2 = length(Y1);
            ones_h = ones(1,N2);
            ones_v = ones(N1,1);
            
            % evaluate the kernel
            norm_xp = sqrt( dY1.^2 + dY2.^2 );            

            deltaX1 = xpoints.X * ones_h - ones_v * Y1;
            deltaX2 = xpoints.Y * ones_h - ones_v * Y2;
            R = sqrt( deltaX1.^2 + deltaX2.^2 );
            zero_index = (R == 0);
            [~, zc] = find(zero_index);
            
            [ddY1, ddY2] = curve.eval( tau(zc), 2 );
            
            K = -1/(2*pi) * ( xpoints.nX * ones_h .* deltaX1 + xpoints.nY * ones_h .* deltaX2 ) ./ R.^2;
            K(zero_index) = 1/(4*pi) * ( dY2(zc) .* ddY1 - dY1(zc) .* ddY2 ) ./ (norm_xp(zc).^3);
            
        end
        
        %> @brief Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(laplaceADLOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = laplaceADLOp.checkRequirementsForPeriodicSmoothOp();
            else         
                isOk = false;
                % warning('In LaplaceADLOp method checkRequirements: requested type %s is not supported.', optype);
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( laplaceADLOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = laplaceADLOp.getPeriodicSmoothImplementation();
            elseif ( strcmp( methodName, BIEPack.methods.Collocation.METHOD_NAME ) )
                A = laplaceADLOp.getCollocationImplementation( varargin{:} );
            else
                error('In LaplaceADLOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
            
    end
    
end
