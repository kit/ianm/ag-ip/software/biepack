%> @file ConcreteLogKernelOp.m
%> @brief File containing the @link BIEPack.ops.ConcreteLogKernelOp 
%> ConcreteLogKernelOp @endlink class.
%>
% ======================================================================
%>
%> @brief Implementation of the @link BIEPack.ops.LogKernelOp LogKernelOp
%> @endlink by with a user implementation of the smooth kernel factor.
%>
%> This class provides everything for application of the Nyström method for
%> kernels with logarithmic singularity via product quadrature. An
%> implementation of the smooth kernel function is provided by the user by passing 
%> a function handle on construction. The function provided through this
%> handle must have the declaration
%> @code
%>   function K = implementationOfKernel( t )
%> @endcode
%> where @a t is row vector of values of the curve parameter and K is a matrix 
%> of size length(t) x length(t) containing the corresponding values of the
%> smooth kernel factor.
%>
% ======================================================================
classdef ConcreteLogKernelOp < BIEPack.ops.LogKernelOp
    
    properties ( SetAccess = protected )
        
        %> @brief function handle of the implementation of the smooth
        %> kernel factor
        kernel = [];
    end
    
    
    methods
        
        %> @brief Constructs the concrete operator from a BemSpace defined  
        %> on a ParamCurveMesh with uniform discretization in parameter space
        %> and an implementation of the smooth kernel factor.
        %>
        %> @param defSpace a @link BIEPack.spaces.BemSpace BemSpace @endlink     
        %>     Identical to range space.
        %> @param kernel function handle of the smooth kernel factor.
        %> @param z shift of the singularity
        function cklo = ConcreteLogKernelOp( defSpace, kernel, z )
            
            % if no arguments are given, create an empty instance.            
            if ( nargin == 0 )
                defSpace = [];
                kernel = [];
                z = 0;
            end
            
            % call parent constructor
            cklo@BIEPack.ops.LogKernelOp(defSpace, defSpace, z);
            
            % initialize kernel
            cklo.setKernel( kernel );
            
        end
        
        %> @brief Sets a new implementation of the kernel function
        %>
        %> @param kernel function handle of the kernel.
        function setKernel( cklo, kernel )
            
            cklo.kernel = kernel;
            
        end
        
        %> @brief Set a new definition and range space
        %>
        %> @param defSpace @link BIEPack.spaces.BemSpace BemSpace @endlink
        %>     of linear spline functions on the parameter interval.
        %>     Identical to range space.
        function setSpace( cklo, space )
            
            cklo.setDefSpace( space );
            cklo.setRangeSpace( space );
            
        end
        
                
        %> @brief Checks whether the method given by methodName can be applied to
        %> this operator. 
        %
        %> Instances of this class only support the Nyström method with product
        %> uqadrature, all other methods will fail.
        %>
        %> @retval @c true if the method can be applied, @c false otherwise
        function isOk = checkRequirements( logKernelOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemLog.METHOD_NAME ) )
                isOk = logKernelOp.checkRequirementsForLogKernelOp;
            else                
                isOk = false;
            end
            
        end
        
        %> @brief Obtain the implementation of the operator as a matrix.
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( logKernelOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemLog.METHOD_NAME ) )
                A = logKernelOp.getLogImplementation();
            else
                error('In ContKernelOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
        
        %> @brief Evaluate the kernel function for parameter values
        %>
        %> The call is simply passed through to the implementation of the
        %> smooth kernel factor provided by the user.
        %>
        %> @param t row vector of parameter values on the curve
        %>
        %> @retval K matrix of size length(t) x length(t)
        %> containing the corresponding values of the kernel at (t(j),t(k)).
        function K = evalLogKernel( cklo, t )
            
            K = cklo.kernel(t);
            
        end
        
        
    end
end

