%> @file HelmholtzNwgSLPot.m
%> @brief File containing the @link HelmholtzNwgSLPot BIEPack.ops.HelmholtzNwgSLPot @endlink class.
% ======================================================================
%> @brief The single layer potential for Helmholtz's equation. 
%
%> This class implements discretizations of the operator
%>
%> SL phi (x) = \int_C \frac{i}{4}H^{(1)}_0(k|x-y|) phi(y) ds(y), x \notin C
%>
%> Implementations as an integral operator include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) into a space of trigonometric 
%>    polynomials defined on a different smooth closed curve D.
%>
%> The class may also be used for representing a single layer potential
%> that is to be evaluated on arbitrary points of the plane not in C. See
%> the function evalPotential() for corresponding functionality.
% ======================================================================
classdef HelmholtzNwgSLPot < BIEPack.ops.HelmholtzSLPot
    
    properties
        
        %> waveguide height
        H = 1;
        
    end
    
    methods
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> wether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        function slPot = HelmholtzNwgSLPot( defSpace, rangeSpace, wavenum, H)
    
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            if (nargin < 3)
                wavenum = 1;
            end

            if (nargin < 4)
                H = 1;
            end            
                        
            slPot@BIEPack.ops.HelmholtzSLPot(defSpace, rangeSpace, wavenum);
            
            slPot.H = H;
            
            slPot.randomName;
            slPot.name = strcat('HelmholtzNwgSLPot_',slPot.name); 
        end
        
        %> @brief Matrix generation for evaluation
        %
        %> Evaluation of the kernel function of the potential in the mesh
        %> points for given coordinates (X1_e, X2_e) of the evaluation
        %> points. These should be matrices of equal size.
        function K = evalSmoothKernelInX(slPot, X1_e, X2_e)
            
            defMesh = slPot.defSpace.theMesh;
            defMesh.evalDerivs(1);
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
            
            L = slPot.defSpace.theCurve.endParam - slPot.defSpace.theCurve.startParam;
            % Compensation for the interval [0, 2pi]
            xip = L / (2*pi) * defMesh.xDeriv(1,:);
            etap = L / (2*pi) * defMesh.yDeriv(1,:);
            norm_xp = sqrt(xip.^2 + etap.^2);
            
            N = length(xi);
            ones_h = ones(1,N);
            ones_v = ones(length(X1_e),1);
            
            X1 = X1_e * ones_h - ones_v * xi;
            
            import BIEPack.utils.PeriodicGreensFunc.g_nwg_via_periodic            
            K = g_nwg_via_periodic(slPot.waveNum*slPot.H, slPot.waveNum*X1, ...
                   slPot.waveNum * X2_e * ones_h, slPot.waveNum * ones_v * eta) ...
                .* (ones_v * norm_xp );
        end
        
        %> @brief Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(slPot, methodName )
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = slPot.checkRequirementsForPeriodicSmoothOp();
            else         
                isOk = false;
                % warning('In HelmholtzNwgSLPot method checkRequirements: requested type %s is not supported.', optype);
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( slPot, methodName, varargin)
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = slPot.getPeriodicSmoothImplementation();
            else
                error('In HelmholtzNwgSLPot method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
    end
end