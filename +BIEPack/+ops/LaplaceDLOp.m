%> @file LaplaceDLOp.m
%> @brief File containing the BIEPack.ops.LaplaceDLOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The double layer operator for Laplace's equation. 
%
%> This class provides discretizations of the operator 
%>
%> @f[
%> D \varphi (x) = \frac{1}{2\pi} \int_C \frac{ n(y) . (x - y) }{|x - y|^2 } \, \varphi(y) \, ds(y),
%> @f]
%>
%> for x on C. Here n(y) denotes the unit normal vector to C at y.
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself. The kernel
%>    inherits the smoothness of the curve reduced by 2 degrees.
%>  - by a degenerate kernel approximiation obtained by interpolation with 
%>    piecewise linear splines when the integral is over a closed C^2 curve C 
%>    The discrete operator maps a BIEPack.spaces.BemSpace onto itself.
%>  - over a C^2 smooth closed curve mapping, discretized by a quadrature
%>    rule for continuous functions (simpleContKernelOp)
classdef LaplaceDLOp < BIEPack.ops.PeriodicSmoothOp & BIEPack.ops.DegenerateKernelOp & BIEPack.ops.SimpleContKernelOp & BIEPack.ops.BemContOp
    
    properties
    end
    
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with thsi type of operator are
        %> carried out by the checkRequirements function
        function dlOp = LaplaceDLOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            dlOp@BIEPack.ops.PeriodicSmoothOp(defSpace,rangeSpace);
            dlOp@BIEPack.ops.DegenerateKernelOp(defSpace,rangeSpace);
            
            dlOp.randomName;
            dlOp.name = strcat('LaplaceDLOp_',dlOp.name);
            
        end
        
        %> @brief Implementation of PeriodicSmoothOp.evalPeriodicSmoothKernel
        %
        %> Evaluation of the kernel function for an operator mapping
        %> a BIEPack.spaces.TrigPol space onto itself.
        %> The evaluation points correspond to the grid points on each
        %> mesh.
        function K = evalPeriodicSmoothKernel(intOp)
                        
            defMesh = intOp.defSpace.theMesh;
            
            defMesh.evalDerivs(2);
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
            
            % Obtain values for derivative of parametrization but
            % compensate for the interval [0, 2*pi]
            xip = intOp.defSpace.L/(2*pi) * defMesh.xDeriv(1,:);
            etap = intOp.defSpace.L/(2*pi) * defMesh.yDeriv(1,:);
            xi2p = ( intOp.defSpace.L/(2*pi) )^2 * defMesh.xDeriv(2,:);
            eta2p = ( intOp.defSpace.L/(2*pi) )^2 * defMesh.yDeriv(2,:);
            
            N = length(xi);
            ones_h = ones(1,N);
            ones_v = ones(N,1);
            
            norm_xp2 = xip.^2 + etap.^2 ;

            X1 = xi.' * ones_h - ones_v * xi;
            X2 = eta.' * ones_h - ones_v * eta;
            R = sqrt( X1.^2 + X2.^2 );
            zero_index = (R == 0);
            [~, zc] = find(zero_index);
            
            K = 1/(2*pi) * ( ones_v * etap .* X1 - ones_v * xip .*X2 ) ./ R.^2;
            K(zero_index) = 1/(4*pi) * ( etap(zc) .* xi2p(zc) - xip(zc) .* eta2p(zc) ) ./ norm_xp2(zc);
            
        end

        %> @brief Implementation of DegenerateKernelOp.evalContinuousKernel()
        %
        %> Evaluate the kernel function which is approximated by a degenerate 
        %> kernel. Each entry in t or tau is a parameter value of the ParamCurve.
        %
        %> The function returns a matrix K of size length(t) x length(tau)
        %> containing the corresponding values of the kernel.
        function K = evalContinuousKernel( intOp, t, tau )
            
            % obtain the handle of the BemMesh for easy reference
            mesh = intOp.defSpace.theMesh;
            
            % evaluate coordinates and derivatives
            curve = mesh.theCurve;

            [xi_t, eta_t] = curve.eval(t,0);

            [xi_tau, eta_tau] = curve.eval(tau,0);
            [xip, etap] = curve.eval(tau,1);
            [xi2p, eta2p] = curve.eval(tau,2);
            
            % some helper variables
            N1 = length(t);
            N2 = length(tau);
            ones_h = ones(1,N2);
            ones_v = ones(N1,1);
            
            % evaluate the kernel
            norm_nu = sqrt( xip.^2 + etap.^2 );

            X1 = xi_t * ones_h - ones_v * xi_tau;
            X2 = eta_t * ones_h - ones_v * eta_tau;
            R = sqrt( X1.^2 + X2.^2 );
            zero_index = (R == 0);
            [~, zc] = find(zero_index);
            
            K = 1/(2*pi) * ( ones_v * etap .* X1 - ones_v * xip .*X2 ) ./ R.^2 ./ ( ones_v * norm_nu );
            K(zero_index) = 1/(4*pi) * ( etap(zc) .* xi2p(zc) - xip(zc) .* eta2p(zc) ) ./ norm_nu(zc).^3;
        end
        
        
        function a = evalBemAFunction( bemContOp, xpoints ) %#ok<INUSL>
            
            a = -0.5 + xpoints.angle/(2*pi);
            
        end

        
        function K = evalBemContKFunction( bemContOp, xpoints, Y1, Y2, dY1, dY2, curve, tau ) %#ok<INUSL>
            
            % some helper variables
            N1 = length(xpoints.X);
            N2 = length(Y1);
            ones_h = ones(1,N2);
            ones_v = ones(N1,1);
            
            % evaluate the kernel
            norm_xp = sqrt( dY1.^2 + dY2.^2 );            

            deltaX1 = xpoints.X * ones_h - ones_v * Y1;
            deltaX2 = xpoints.Y * ones_h - ones_v * Y2;
            R = sqrt( deltaX1.^2 + deltaX2.^2 );
            zero_index = (R == 0);
            [~, zc] = find(zero_index);
            
            [ddY1, ddY2] = curve.eval( tau(zc), 2 );
            
            K = 1/(2*pi) * ( ones_v * (dY2./norm_xp) .* deltaX1 - ones_v * (dY1./norm_xp) .* deltaX2 ) ./ R.^2;
            K(zero_index) = 1/(4*pi) * ( dY2(zc) .* ddY1 - dY1(zc) .* ddY2 ) ./ (norm_xp(zc).^3);
            
        end
        
        
        %> @brief  Implementaton of GeneralOp.checkRequirements().
        %
        %> Check is passed on to the corresponding super classes of a
        %> Nyström method or a degenerate kernel approximation.
        function isOk = checkRequirements(laplaceDLOp,methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                
                isOk = laplaceDLOp.checkRequirementsForPeriodicSmoothOp;
                
            elseif ( strcmp( methodName, BIEPack.methods.DegenerateKernelApprox.METHOD_NAME ) )                
                
                % check that the we have a closed curve at least in C^2
                % this implies that the kernel of the DL Operator is indeed
                % continuous
                curve = laplaceDLOp.defSpace.theCurve;
                if ( curve.closed && curve.globalSmoothness >= 2 )
                    isOk = laplaceDLOp.checkRequirementsForDegenerateKernelOp;  
                else
                    isOk = false;
                end
                
            elseif ( strcmp( methodName, BIEPack.methods.NystroemCont.METHOD_NAME ) )                
                
                % check that the we have a closed curve at least in C^2
                % this implies that the kernel of the DL Operator is indeed
                % continuous
                curve = laplaceDLOp.defSpace.theCurve;
                if ( curve.closed && curve.globalSmoothness >= 2 )
                    isOk = laplaceDLOp.checkRequirementsForSimpleContKernelOp;  
                else
                    isOk = false;
                end
                
            elseif ( strcmp( methodName, BIEPack.methods.Collocation.METHOD_NAME ) )
                
                % check that the operator is defined on a valid BemSpace
                if ( derivedFrom(laplaceDLOp.defSpace, 'BIEPack.spaces.BemSpace') )
                    isOk = true;
                else
                    isOk = false;
                end
                
            else
                
                % unknown method
                isOk = false;
                
            end
                        
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( laplaceDLOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                
                A = laplaceDLOp.getPeriodicSmoothImplementation();
                
            elseif ( strcmp( methodName, BIEPack.methods.Collocation.METHOD_NAME ) )
                
                A = laplaceDLOp.getCollocationImplementation( varargin{:} );
                
            elseif ( strcmp( methodName, BIEPack.methods.DegenerateKernelApprox.METHOD_NAME ) )
                
                A = laplaceDLOp.getDegenerateKernelImplementation();
                
            elseif ( strcmp( methodName, BIEPack.methods.NystroemCont.METHOD_NAME ) )
                
                A = laplaceDLOp.getSimpleContKernelImplementation();
                
            else
                error('In LaplaceDLOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
            
    end
    
end
