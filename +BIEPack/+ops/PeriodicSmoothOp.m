%> @file PeriodicSmoothOp.m
%> @brief File containing the BIEPack.ops.PeriodicSmoothOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief Class representing an integral operator with a smooth kernel,
%> periodic with respect to both arguments.
%
%> The class represents the discretization of an integral operator defined 
%> on spaces of 2pi-periodic functions on the real line. The kernel
%> function is assumed to be smooth and periodic with respect to both
%> arguments.
%>
%> The discrete definition and range spaces must be instances of TrigPol. 
%> The discrete operator is obtained by applying the composite trapeoidal.
%> The quadrature points are -pi + j pi / N,  j = 0, ..., 2N-1. In this way, 
%> the operator can be applied in a Nyström or a collocation method for 
%> trigonometric polynomials.
%>
%> This abstract class may be implemented by any integral operator
%> defined on a closed smooth curve and mapping a TrigPol space on this 
%> curve on a TrigPol space on a possibly different curve. The class uses 
%> an implementation of the kernel function to provide a discrete 
%> realization of the operator. For a parameter interval of 
%> length 2pi, the quadrature points correspond exactly to the points
%> t in the underlying @link ParamCurveMesh BIEPack.meshes.ParamCurveMesh. @endlink
%>
%> A general smooth closed curve may have a different length L of the
%> parameter interval than 2pi. In this case, the appropriate scaling of
%> the derivatives of the parametrization must be taken care of in
%> implementation of the kernel function.
% ======================================================================
classdef PeriodicSmoothOp < BIEPack.ops.GeneralOp
    % BIEPack.ops.PeriodicSmoothOp < BIEPack.ops.GeneralOp
    
    methods 
        
        %> @brief Initialize the object.
        %
        %> Just passes on the call to the super class constructor. All
        %> arguments are optional.
        function perSmoothOp = PeriodicSmoothOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            perSmoothOp@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
            perSmoothOp.randomName;
            perSmoothOp.name = strcat('PeriodicSmoothOp_',perSmoothOp.name);
            
        end
        
        %> @brief Compute a matrix that represents this operator mapping basis
        %> coefficients in the definition space to those in the range space.
        function A = getPeriodicSmoothImplementation(perSmoothOp)                      
            
            % Obtain the number N corresponding to half the number of grid
            % points in the definition space.
            N = perSmoothOp.defSpace.N;
                                    
            % evaluate the kernels
            A = pi/N * perSmoothOp.evalPeriodicSmoothKernel;
            
        end
        
        %> @brief Checks whether the geometry, mesh and spaces are such that 
        %> this operator may be used in a method that uses a PeriodicSmoothOp
        %> object.
        %
        %> The reqirements are that both definition and range space are
        %> derived from TrigPol.
        function result = checkRequirementsForPeriodicSmoothOp(perSmoothOp)
            
            if ( ~ BIEPack.utils.derivedFrom( perSmoothOp.defSpace, 'BIEPack.spaces.TrigPol' ) )
                result = false;
                % warning('In PeriodicSmoothOp method checkRequirements: definition space is not BIEPack.spaces.TrigPol');
                return
            end
            
            if ( ~ BIEPack.utils.derivedFrom( perSmoothOp.rangeSpace, 'BIEPack.spaces.TrigPol' ) )
                result = false;
                % warning('In PeriodicSmoothOp method checkRequirements: range space is not BIEPack.spaces.TrigPol');
                return
            end
            
            result = true;
            
        end
        
    end
    
    
    methods ( Abstract )
        
        % @brief Evaluate the kernel function in the grid points.
        %
        %> The function evaluates K(t_k,tau_l) for the quadrature
        %> points tau_l, l= 0,...,2N-1, and the evaluation points t_k,
        %> k = 0,...,2M-1, with N = defSpace.N, M = rangeSpace.N. These 
        %> points correspond to the nodes of the underlying ParamCurveMesh 
        %> objects scaled to the interval [-pi,pi].
        %
        %> The function returns a matrix of dimension 2M x 2N.
        %
        %> Note that this class assumes that the parameter intervals are
        %> [-pi,pi]. If the underlying curves usesa different parameter
        %> interval, the implementation of this function must take care of
        %> the rescaling.
        K = evalPeriodicSmoothKernel(perSmoothOp);        
        
    end
    
end