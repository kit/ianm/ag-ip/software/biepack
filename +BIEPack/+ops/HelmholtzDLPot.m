%> @file HelmholtzDLPot.m
%> @brief File containing the @link HelmholtzDLPot BIEPack.ops.HelmholtzDLPot @endlink class.
% ======================================================================
%> @brief The double layer potential for Helmholtz's equation. 
%
%> This class implements discretizations of the operator
%>
%> DL phi (x) = \frac{i}{4} \int_C \frac{\partial}{\partial n(y)}H^{(1)}_0(k|x-y|) phi(y) ds(y), x \notin C
%>
%> Implementations as an integral operator include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) into a space of trigonometric 
%>    polynomials defined on a different smooth closed curve D.
%>
%> The class may also be used for representing a double layer potential
%> that is to be evaluated on arbitrary points of the plane not in C. See
%> the function evalPotential() for corresponding functionality.
% ======================================================================
classdef HelmholtzDLPot < BIEPack.ops.PeriodicSmoothOp
    
    properties (SetAccess = protected)
        
        waveNum = 1;
        
    end
    
    methods
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        function dlPot = HelmholtzDLPot( defSpace, rangeSpace, wavenum)
    
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
                        
            dlPot@BIEPack.ops.PeriodicSmoothOp(defSpace,rangeSpace);
            
            if ( ~isempty(wavenum))
                dlPot.waveNum = wavenum;
            end
            dlPot.randomName;
            dlPot.name = strcat('HelmholtzDLPot_',dlPot.name); 
        end
        
        %> @brief Matrix generation for evaluation
        %
        %> Evaluation of the kernel function of the potential in the mesh points for given
        %> coordinates (X1_e,X2_e) of the evaluation points. These should be
        %> column vectors of equal length.
        function K = evalSmoothKernelInX(dlpot, X1_e, X2_e)
            defMesh = dlpot.defSpace.theMesh;
            defMesh.evalDerivs(1);
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
            L = dlpot.defSpace.theCurve.endParam - dlpot.defSpace.theCurve.startParam;
            xip = L / (2*pi) * defMesh.xDeriv(1,:);
            etap = L / (2*pi) * defMesh.yDeriv(1,:);
            N = length(xi);
            ones_h = ones(1,N);
            ones_v = ones(length(X1_e),1);
            X1 = X1_e * ones_h - ones_v * xi;
            X2 = X2_e * ones_h - ones_v * eta;
            kR = dlpot.waveNum * sqrt(X1.^2 + X2.^2);
            dot_prod = (ones_v * etap) .* X1 - (ones_v * xip) .* X2;
            K = 1i / 4 * dlpot.waveNum^2 * besselh(1,kR) .* dot_prod ./ kR;
        end
        
        %> @brief Implementation of the
        %> PeriodicSmoothOp.evalPeriodicSmoothKernel()
        %
        %> Obtain the evaluation points and call evalSmoothKernelInX()
        function K = evalPeriodicSmoothKernel(dlpot)
            X1 = dlpot.rangeSpace.theMesh.nodesX.';
            X2 = dlpot.rangeSpace.theMesh.nodesY.';
            K = dlpot.evalSmoothKernelInX(X1, X2);
        end
        
        %> @brief Implementation of the
        %> PeriodicSmoothOp.evalPeriodicSmoothKernel()
        %
        %> Obtain the evaluation points and call evalSmoothKernelInX()
        function A = getPotentialImplementation(dlpot, X1, X2)
            A = 2*pi / dlpot.defSpace.dimension * dlpot.evalSmoothKernelInX(X1,X2);
        end
        
        %> @brief Evaluate the potential at arbitrary points in space. 
        %
        %> The parameters X1 and X2 should be of the same size and the result is
        %> also of this size. The argument phi is a column vector of values of
        %> the density in the mesh points of the mesh on C.
        %>
        %> The potential is evaluated by the composite trapezoidal rule.
        function P = evalPotential(slpot, X1, X2, phi)
            P_size = size(X1);
            
            x1 = X1(:);
            x2 = X2(:);
            
            A = getPotentialImplementation(slpot, x1, x2);
            P = A * phi;
            P = reshape(P, P_size);
        end
        
        %> @brief Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(helmholtzDLPot, methodName )
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = helmholtzDLPot.checkRequirementsForPeriodicSmoothOp();
            else         
                isOk = false;
                % warning('In HelmholtzSLPot method checkRequirements: requested type %s is not supported.', optype);
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( helmholtzDLPot, methodName, varargin)
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = helmholtzDLPot.getPeriodicSmoothImplementation();
            else
                error('In HelmholtzSLPot method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
        
        
    end
end