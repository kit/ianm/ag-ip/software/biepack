%> @file SimpleContKernelOp.m
%> @brief File containing the @link BIEPack.ops.SimpleContKernelOp SimpleContKernelOp @endlink class.
% ======================================================================
%> @brief An integral operator with a continuous kernel
%>
%> The requirements for such an operator are
%>   - it has a continuous kernel,
%>   - it is defined on a @link BIEPack.spaces.BemSpace BemSpace @endlink,
%>   - the range space is the same as the space of definition,
%>   - the BemSpace is based on a @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink.
%>
%> The SimpleContKernelOp is supposed to be used in a Nyström method with an
%> arbitrary quadrature rule as implement in the method @link BIEPack.methods.NystroemCont NystroemCont @endlink.
%> The implementation of such an operator amounts simply to an evaluation of
%> the kernel.
%>
%> The purpose of providing an implementation of this method is purely
%> educational: Applying the Nyström method with various quadrature rules with
%> differing order of convergence shows that the method inherits that order
%> of convergence. The last restriction above (ParamCurveMesh) in particular
%> is just there to keep the implementation as simple as possible.
% ======================================================================
classdef SimpleContKernelOp < BIEPack.ops.GeneralOp
        
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. The 
        %> restrictions on the parameters listed below have to be satisfied for an
        %> operator used in a @link BIEPack.method.NystroemCont NystroemCont @endlink method. 
        %> Checks whether this is actually
        %> the case are carried out by the checkRequirements function of
        %> the method.
        %>
        %> @param defSpace a @link BIEPack.spaces.BemSpace BemSpace @endlink 
        %>     based on a @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink
        %> @param rangeSpace identical to @a defSpace
        function sckOp = SimpleContKernelOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            sckOp@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
        end
        
        %> @brief Compute a matrix that represents point evaluations of the 
        %> kernel function.
        %>
        %> The function simply evaluates the kernel function and multiplies
        %> with the scaling factor due to the definition of the line integral.
        %>
        %> @retval A the matrix representation of the operator
        function A = getSimpleContKernelImplementation(sckOp)
            
            % the mesh we are using
            mesh = sckOp.defSpace.theMesh;
            
            % obtain the parameter values of the nodes of the mesh
            t = mesh.t;
            
            % Evaluate the kernel function. Additionally, we have to
            % multiply with a term from inserting the parametrization of
            % the curve
            mesh.evalDerivs(1);
            A = sckOp.evalContinuousKernel( t.', t ) ...
                * diag( sqrt( mesh.xDeriv.^2 + mesh.yDeriv.^2 ) );
            
        end
        
        %> @brief Checks whether the geometry, mesh and spaces are such that this
        %> operator may be used in a Nyström method for a contiuous kernel function.
        %>
        %> Requirements are that the definition and range spaces are equal
        %> and that they are a @link BemSpace BIEPack.spaces.BemSpace @endlink. The space has to
        %> be based on a @link BIEPack.meshes.ParamCurveMesh ParamCurveMesh @endlink
        %>
        %> @retval isOk True if the operator meets the requirements.
        function isOk = checkRequirementsForSimpleContKernelOp(sckOp)
                        
            if ( ~ BIEPack.utils.derivedFrom( sckOp.defSpace, 'BIEPack.spaces.BemSpace' ) )
                isOk = false;
                return
            end
            
            if ( sckOp.defSpace ~= sckOp.rangeSpace )
                isOk = false;
                return
            end       
            
            if ( ~ BIEPack.utils.derivedFrom( sckOp.defSpace.theMesh, 'BIEPack.meshes.ParamCurveMesh' ) )
                isOk = false;
                return
            end
            
            isOk = true;
            
        end
        
    end
    
    
    methods ( Abstract )

        %> @brief Evaluate the kernel function which is approximated by a degenerate 
        %> kernel. 
        %>
        %> @param t column vector of parameter values on the curve
        %> @param tau row vector of parameter values on the curve
        %>
        %> @retval K matrix of size length(t) x length(tau)
        %> containing the corresponding values of the kernel.
        K = evalContinuousKernel( sckOp, t, tau );        
        
    end
    
end
