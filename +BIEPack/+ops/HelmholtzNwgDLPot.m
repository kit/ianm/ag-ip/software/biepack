%> @file HelmholtzNwgDLPot.m
%> @brief File containing the @link HelmholtzNwgDLPot BIEPack.ops.HelmholtzNwgDLPot @endlink class.
% ======================================================================
%> @brief The double layer potential for Helmholtz's equation. 
%
%> This class implements discretizations of the operator
%>
%> DL phi (x) = \frac{i}{4} \int_C \frac{\partial}{\partial n(y)}H^{(1)}_0(k|x-y|) phi(y) ds(y), x \notin C
%>
%> Implementations as an integral operator include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) into a space of trigonometric 
%>    polynomials defined on a different smooth closed curve D.
%>
%> The class may also be used for representing a double layer potential
%> that is to be evaluated on arbitrary points of the plane not in C. See
%> the function evalPotential() for corresponding functionality.
% ======================================================================
classdef HelmholtzNwgDLPot < BIEPack.ops.HelmholtzDLPot
    
    properties
        
        %> waveguide height
        H = 1;
        
    end
    
    methods
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        function dlPot = HelmholtzNwgDLPot(defSpace, rangeSpace, wavenum, H)
    
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            if (nargin < 3)
                wavenum = 1;
            end
            
            if (nargin < 4)
                H = 1;
            end
                        
            dlPot@BIEPack.ops.HelmholtzDLPot(defSpace, rangeSpace, wavenum);
            
            dlPot.H = H;
            
            dlPot.randomName;
            dlPot.name = strcat('HelmholtzNwgDLPot_',dlPot.name); 
        end
        
        %> @brief Matrix generation for evaluation
        %
        %> Evaluation of the kernel function of the potential in the mesh points for given
        %> coordinates (X1_e,X2_e) of the evaluation points. These should be
        %> column vectors of equal aize.
        function K = evalSmoothKernelInX(dlPot, X1_e, X2_e)
            
            defMesh = dlPot.defSpace.theMesh;
            
            defMesh.evalDerivs(1);
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
            L = dlPot.defSpace.theCurve.endParam - dlPot.defSpace.theCurve.startParam;
            xip = L / (2*pi) * defMesh.xDeriv(1,:);
            etap = L / (2*pi) * defMesh.yDeriv(1,:);
            
            N = length(xi);
            ones_h = ones(1,N);
            ones_v = ones(length(X1_e),1);
            X1 = X1_e * ones_h - ones_v * xi;
                                  
            % evaluate the gradient of the Greens function. The function g_nwg_gradient_via_periodic
            % computes the gradient with respect to x, we need y here.
            % Thus, the third and fourth arguments in the call are swapped
            % and the sign is changed for G1.
            import BIEPack.utils.PeriodicGreensFunc.g_nwg_gradient_via_periodic          
            [G1, G2] = g_nwg_gradient_via_periodic(dlPot.waveNum * dlPot.H, dlPot.waveNum * X1, ...
                                                   dlPot.waveNum * ones_v * eta, ...
                                                   dlPot.waveNum * X2_e * ones_h);
            K = (-dlPot.waveNum * ones_v * etap) .* G1 - (dlPot.waveNum * ones_v * xip) .* G2;
        end
        
        %> @brief Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(dlPot, methodName )
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = dlPot.checkRequirementsForPeriodicSmoothOp();
            else         
                isOk = false;
                % warning('In HelmholtzNwgDLPot method checkRequirements: requested type %s is not supported.', optype);
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation(dlPot, methodName, varargin)
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = dlPot.getPeriodicSmoothImplementation();
            else
                error('In HelmholtzNwgDLPot method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
                        
    end
end