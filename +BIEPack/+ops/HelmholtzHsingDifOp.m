%> @file HelmholtzHsingDifOp.m
%> @brief File containing the BIEPack.ops.HelmholtzHsingDifOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The difference of two hypersingular operators for the Helmholtz equation. 
%
%> This class implements discretizations of the operator
%>
%> T_{k_1} - T_{k_2}
%>
%> for an outer medium with wavenumber k_1 and an inner medium with
%> wavenumber k_2.
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself.
% ======================================================================
classdef HelmholtzHsingDifOp < BIEPack.ops.PeriodicLogOp
    % BIEPack.ops.HelmholtzHsingDifOp < BIEPack.ops.PeriodicLogOp
    
    properties ( Constant )
        
        %> Euler's constant appearing in expressions of the kernel
        eulerC = 0.5772156649;
        
    end
    
    properties
        
        %> the outer wave number
        waveNumOut = 1;

        %> the inner wave number
        waveNumIn = 2;
        
    end
    
    methods 
        
        %> @brief Construct the operator
        %>
        %> Passes on the call to the super class constructor. 
        %> Also sets the wavenumbers for both media.
        %>
        %> Checks whether the spaces are compatible with thsi type of operator are
        %> carried out by the checkRequirements function
        function hsingDifOp = HelmholtzHsingDifOp(defSpace, rangeSpace, wavenum_1, wavenum_2)
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end

            if (nargin < 4)
                wavenum_1 = 1;
                wavenum_2 = 2;
            end
            
            hsingDifOp@BIEPack.ops.PeriodicLogOp(defSpace,rangeSpace);
            
            hsingDifOp.waveNumOut = wavenum_1;
            hsingDifOp.waveNumIn = wavenum_2;            
            
            hsingDifOp.randomName;
            hsingDifOp.name = strcat('HelmholtzHsingDifOp_',hsingDifOp.name);
            
        end
        
        %> @brief Implementation of PeriodicLogOp.evalPeriodicLogKernels
        %
        %> Evaluation of the functions defining the kernel. Implementation
        %> of the function inherited from BIEPack.ops.PeriodicLogOp                
        function [K1, K2] = evalPeriodicLogKernels(hsingDifOp)
                        
            curve = hsingDifOp.defSpace.theCurve;
            
            t = hsingDifOp.defSpace.theMesh.t;
            xi = hsingDifOp.defSpace.theMesh.nodesX;
            eta = hsingDifOp.defSpace.theMesh.nodesY;
            
            [xip, etap] = curve.eval(t,1);
            % scale for parametrization over [0,2*pi]
            xip = hsingDifOp.defSpace.L/(2*pi) * xip;
            etap = hsingDifOp.defSpace.L/(2*pi) * etap;
            
            N = length(t);
            ones_h = ones(1,N);
            ones_v = ones(N,1);
            
            norm_xp = sqrt( xip.^2 + etap.^2 ).';
            norm_xp_matrix = norm_xp * ones_h;

            X1 = xi.' * ones_h - ones_v * xi;
            X2 = eta.' * ones_h - ones_v * eta;
            R = sqrt( X1.^2 + X2.^2 );

            dot_product_x = (etap.' * ones_h) .* X1 - (xip.' * ones_h) .* X2;
            dot_product_y = (ones_v * etap) .* X1 - (ones_v * xip) .* X2;            
            normal_terms_1 = dot_product_x .* dot_product_y ./ R.^2;
            normal_terms_1(1:N+1:end) = 0;
            normal_terms = xip.' * xip + etap.' * etap - 2 * normal_terms_1;

            K1 = 0.5 * ((hsingDifOp.waveNumIn * besselj(1, hsingDifOp.waveNumIn * R) ...
                         - hsingDifOp.waveNumOut * besselj(1, hsingDifOp.waveNumOut * R)) .* normal_terms ./ R...
                        - (hsingDifOp.waveNumOut^2* besselj(0, hsingDifOp.waveNumOut * R) ...
                               - hsingDifOp.waveNumIn^2* besselj(0, hsingDifOp.waveNumIn * R)) ...
                         .* normal_terms_1) ./ norm_xp_matrix;
            K1(1:N+1:end) = -0.25 * (hsingDifOp.waveNumOut^2 - hsingDifOp.waveNumIn^2) * norm_xp;
            
            r = t.' * ones_h - ones_v * t;

            K2 = 0.25i * ((hsingDifOp.waveNumOut * besselh(1, 1, hsingDifOp.waveNumOut * R) ...
                           - hsingDifOp.waveNumIn * besselh(1, 1, hsingDifOp.waveNumIn * R)) ...
                        .* normal_terms ./ R ...
                        + (hsingDifOp.waveNumOut^2 * besselh(0, 1, hsingDifOp.waveNumOut * R) ...
                          - hsingDifOp.waveNumIn^2 * besselh(0, 1, hsingDifOp.waveNumIn * R)) ...
                       .* normal_terms_1) ./ norm_xp_matrix ...
                - 1/(2*pi) * log( 4 * sin( r/2 ).^2 ) .* K1;

            K2(1:N+1:end) = (0.25/pi * (hsingDifOp.waveNumOut^2 - hsingDifOp.waveNumIn^2) * (0.5i*pi - hsingDifOp.eulerC + 0.5) ...
                - 0.25/pi * (hsingDifOp.waveNumOut^2 * log(hsingDifOp.waveNumOut/2 * norm_xp) ...
                          - hsingDifOp.waveNumIn^2 * log(hsingDifOp.waveNumIn/2 * norm_xp))) .* norm_xp;
            
        end
        
        %> @brief  Implementaton of GeneralOp.checkRequirements().
        %
        %> Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(hsingDifOp, methodName)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = hsingDifOp.checkRequirementsForPeriodicLogOp;
            else                
                isOk = false;
                % warning('In HelmholtzSLOp method checkRequirements: requested type %s is not supported.', methodName );
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation(hsingDifOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = hsingDifOp.getPeriodicLogImplementation();
            else
                error('In HelmholtzHsingDifOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
    end
    
end

