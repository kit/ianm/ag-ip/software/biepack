%> @file quadGeneralSingularity.m
%> @brief Contains the quadGeneralSingularity() function for computing a quadrature
%> rule for general weakly singular integrands.
%
% ======================================================================
%
%> @brief The function provides quadrature points and weights for computing
%> integrals for integrands for general weak singularities in the interval end points.
%>
%> The integral
%> @f[
%>    \int_0^1 f(x) \mathrm{D} x
%> @f]
%> is transformed onto an integral over @f$[-\pi,\pi]@f$ with a transformation
%> that has infinitely many vanishing derivatives at the end points. Thus the
%> transformed integrand becomes a smooth periodic function and the composite
%> trapezoidal rule can be used to compute the integral to high accuracy. Ther
%> method is described in detail in Kress, Numerical Analysis, Section 9.6. Here,
%> the transform suggested by Sag and Szekeres (Math. Comp. 1964) is used.
%>
%> @param N parameter specifying the number of quadrature points
%>
%> @retval x quadrature points, row vector of length 2N-2
%> @retval w quadrature weights, size equal to that of x
function [x, w] = quadGeneralSingularity(N)

t = linspace(-pi,pi,2*N+1);
t = t(2:2*N+1);

% compute the transformation chi: [-\pi, \pi] -> [0,1] and its derivative
[x, d_g] = chi(t);

% transform weights
w = pi/N * d_g;

% the integrand is zero in the end points, so these can be removed
index = ( x > 0 & x < 1 );
x = x(index);
w = w(index);

end

% The transform is a based on the transform u -> tanh( u / (1-u^2) ), but
% appropriately scaled. The formula given by Kress, which is implemented here,
% is
% chi(t) = exp( -2*pi/t) / [ exp(-2*pi/t) + exp(-2*pi/(2*pi - t)) ]
function [c, d_c] = chi(t)

c = zeros(size(t));
d_c = zeros(size(t));

index1 = ( t >= pi );
index2 = ( t > -pi & t <= pi );

c(index1) = 1.0;

% map intervall (-pi, pi) to (0, 1)
s = ( t + pi ) / (2*pi);

% use standard mollifier
z1 = exp( -1.0 ./ s );
z2 = exp( -1.0 ./ (1.0 - s) );

c(index2) = z1 ./ ( z1 + z2 );
d_c(index2) = ( 1 - 2*s + 2*s.^2 ) ./ s.^2 ./ (1-s).^2 .* z1 .* z2 ./ (z1 + z2).^2 / (2*pi);

end