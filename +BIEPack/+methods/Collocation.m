%> @file Collocation.m
%> @brief Contains the methods.Collocation class.
% ======================================================================
%> @ingroup MethodsGroup 
%> @brief Collocation boundary element method
%>
%> For now, this method supports a simple boundary integral equation where
%> every operator is defined on the same BIEPack.spaces.BemSpace. The space
%> may either use purely piecewise linears or piecewise constants as local 
%> basis functions.
% 
classdef Collocation < BIEPack.BIEPackObject
    
    
    properties ( Constant )
        
        %> @brief name of the method used in requirement checking
        METHOD_NAME = 'Collocation';
        
    end
    
    properties (SetAccess = protected)

        %> @brief The operator in the equation
        theOp = [];
        
        %> @brief The right hand side in the equation
        theRhs = [];
        
        %> @brief The linear system matrix
        A = [];
        
        %> @brief The right hand side of the linear system
        b = [];
        
        %> @brief The quadrature rules to use for integration over elements
        %>
        %> The struct has four fields, all row vectors:
        %>  - contPoints, contWeights are the points and weights to use for 
        %>    an integral with a continuous integrand
        %>  - singPoints, singWeights are the points and weights to use for
        %>    an integrand with weak endpoint singularities.
        quadRules = struct( ...
            'contPoints', [], ...
            'contWeights', [], ...
            'singPoints', [], ...
            'singWeights', [] ...
            );
        
    end
    
    methods
        
        %> @brief Initialises the object.
        %> 
        %> Construct the method using an integral operator and
        %> a right hand side. Also assigns the quadrature rules.       
        function colloc = Collocation( op, rhs )
                        
            if ( nargin > 0 )
                colloc.setOperators(op);
                colloc.setRhs(rhs);                
            end
            
            % define the quadrature rules to use
            [t, w] = BIEPack.utils.gaussLegendreQuad(4,0,1);
            colloc.quadRules.contPoints = t;
            colloc.quadRules.contWeights = w;
%             colloc.quadRules.contPoints = linspace(0,1,6);
%             colloc.quadRules.contWeights = 0.2 * ones(1,6);
%             colloc.quadRules.contWeights(1) = 0.1;
%             colloc.quadRules.contWeights(6) = 0.1;
            
            [t, w] = BIEPack.utils.quadGeneralSingularity(5);
            colloc.quadRules.singPoints = t;
            colloc.quadRules.singWeights = w;
            
        end
    
        %> @todo Implement properly
        function setOperators( colloc, ops )
            
            colloc.theOp = ops;
            
        end
        
        %> @todo Implement properly        
        function setRhs(colloc, rhs )
            
            colloc.theRhs = rhs;
            
        end
        
        %> @brief Set the quadrature rules to be used for integrations
        %>
        %> Provides quadrature points and weights for integration of
        %> the integrals
        %> @f[
        %>  \int_0^1 f(t) \, \mathrm{D} t
        %> @f]
        %>
        %> where the function @f$f@f$ is either continuous (t_c,w_c) or has arbitrary
        %> weak singularities at the end points 0 and 1 of the interval (t_s,w_s).
        function setQuadRules( colloc, t_c, w_c, t_s, w_s)
            
            colloc.quadRules.contPoints = t_c;
            colloc.quadRules.contWeights = w_c;            
            colloc.quadRules.singPoints = t_s;
            colloc.quadRules.singWeights = w_s;
            
        end
        
        %> @brief assemble the collocation method matrix
        %>
        %> The method defines the quadrature rules to be used for computing
        %> integrals over elements. There are rules for continuous
        %> functions and for those with a logarithmic singularity in the end
        %> points.
        function assembleMatrix( colloc )
            
            % Obtain the implementation
            colloc.A = colloc.theOp.getImplementation( colloc.METHOD_NAME, colloc.quadRules );
            colloc.b = colloc.theRhs;
            
        end
        
        
        function phi = apply( colloc )
            
            if ( isempty( colloc.theOp ) )
                error('In Collocation method apply(): operator is unset. ');
            elseif ( isempty( colloc.theRhs ) )
                error('In Collocation method apply(): right hand side of operator equation is unset. ');
            end
            
            if ( isempty(colloc.A) )
                colloc.assembleMatrix;
            end
            
            phi = colloc.A \ colloc.b;
        end
        
    end
        
end