%> @file FlippedCurve.m
%> @brief Contains the geom.FlippedCurve class.
% ======================================================================
%> @brief Parametrized curve with inverted orientation.
%>
%> A FlippedCurve is generated from another ParamCurve object by inverting
%> its orientation. The parametrization interval is moved through in the
%> opposite direction and all derivatives (and the tangential and normal
%> vectors) change their sign.
classdef FlippedCurve < BIEPack.geom.ParamCurve
    
    properties (SetAccess = protected)
        
        %> @brief The ParamCurve this curve is based on
        originalCurve = [];
        
        %> @brief Offset for evaluating the original curve parametrization
        %>
        %> If the original curve has parameter interval [a,b], an evaluation
        %> of the flipped curve at t means an evaluation of the original
        %> curve at a+b-t. The offset is a+b.
        evalOffset = 0;
        
    end

    
    methods(Access = protected)
        
        %> @brief Implementation of the copyElement function to create a deep
        %> copy of the original curve.
        %>
        %> The orginal curve must be truly copied.
        function cpObj = copyElement(obj)
            
            % Make a shallow copy of all properties
            cpObj = copyElement@matlab.mixin.Copyable(obj);
            
            % Make a deep copy of original curve
            cpObj.originalCurve = copy( obj.originalCurve );
            
        end
        
   end
    
    methods
        
        
        %> @brief Constructs a Colton/Kress kite. 
        %> A random name starting with 'Kite_' is also set.
        %>
        %> @retval kite The newly constructed Kite object.
        function flippedCurve = FlippedCurve(origCurve)
            
            if ( nargin > 0 )
                flippedCurve.originalCurve = origCurve;
                
                flippedCurve.name = strcat('flipped_version_of_',origCurve.name);
                
                flippedCurve.startParam = origCurve.startParam;
                flippedCurve.endParam = origCurve.endParam; 
                flippedCurve.evalOffset = origCurve.startParam + origCurve.endParam;

                flippedCurve.closed = origCurve.closed;
                flippedCurve.smoothness = origCurve.smoothness;
                flippedCurve.globalSmoothness = origCurve.globalSmoothness;
                flippedCurve.highestImplementedDerivative = origCurve.highestImplementedDerivative;
            end
            
        end
        
        %> @brief Evaluate parametrization of flipped curve
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. 
        %> @retval y2 2nd coordinate of the curve points.
        function [y1, y2] = paramEval(flippedCurve, t, n)
            
            [y1, y2] = flippedCurve.originalCurve.paramEval(flippedCurve.evalOffset-t, n);
            y1 = (-1)^n * y1;
            y2 = (-1)^n * y2;
            
        end
    end
end

