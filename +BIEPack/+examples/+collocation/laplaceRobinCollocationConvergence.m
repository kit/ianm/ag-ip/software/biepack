%> @file laplaceRobinCollocationConvergence.m
%> @brief Example script to demonstrate convergence properties of
%> collocation method
%

import BIEPack.*;

% number of grids used for convergence demonstration
numGrids = 7;

% initialize the h variable
h = 0.15.^(1:numGrids);

% solve the integral equation on 5 grids obtained by uniform refinement
[phi, theSpace] = examples.collocation.laplaceRobinAnnulus(h(1), numGrids-1);

% create a space based on a mesh which is two steps finer
fineMesh = theSpace{numGrids}.theMesh.uniformRefinement().uniformRefinement();
fineSpace = spaces.BemSpace(fineMesh,spaces.BemSpace.PIECEWISE_LINEAR);

% define the function for evaluating the right hand side
rhs = @(X,Y,NX,NY) rhsFunc( X,Y,NX,NY, @examples.collocation.exampleHarmonicFunction );

% compute approximation in the fine space (uses 4 point Gauss-Legendre-Quadrature)
phiFine = examples.collocation.laplaceRobinIntegralEquation(fineSpace,rhs);

% Restrict solutions to the coarsest grid
phiOnCoarse = cell(numGrids,1);
phiOnCoarse{1} = phi{1};
for j=2:numGrids
    relation = spaces.BemSpaceRelation(theSpace{1},theSpace{j});
    phiOnCoarse{j} = relation.restrict(phi{j});
end
fineRelation = spaces.BemSpaceRelation(theSpace{1},fineSpace);
phiFineOnCoarse = fineRelation.restrict(phiFine);

% compute errors on coarse grid and plot
errOnCoarse = zeros(1,numGrids);
for j=1:numGrids
    errOnCoarse(j) = max(abs(phiOnCoarse{j} - phiFineOnCoarse));
end

loglog(h,errOnCoarse,'xr');


function f = rhsFunc(X,Y,NX,NY,solution)

[u, dx_u, dy_u] = solution(X,Y);
f = 2 * ( NX.*dx_u + NY.*dy_u + u );

end

