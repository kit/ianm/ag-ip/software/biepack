%> @file laplaceRobinIntegralEquation.m
%> @brief Contains the laplaceRobinIntegralEquation() example function
%
% ======================================================================
%
%> @brief Function for solving the boundary integral equation for a Robin 
%>     problem for Laplace's equation in a given BemSpace using the collocation method.
%>
%> @param theSpace BemSpace in which the integral equation is solved.
%>
%> @param rhsFunc function for evaluation the right hand side. It has the
%>    interface u = rhsFunc(X,Y,NX,NY) where (X,Y) are coordinates of boundary curve
%>    points and (NX,NY) are the normal vectors in these points (each variable
%>    a vector of equal length).
%>
%> @param quadRules struct to define quadrature rules.
function phi = laplaceRobinIntegralEquation(theSpace,rhsFunc,quadRules)

import BIEPack.*;

% define the operator I + 2*Ktilde + 2*S
I = ops.IdentityOp(theSpace);
Ktilde = ops.LaplaceADLOp(theSpace, theSpace);
S = ops.LaplaceSLOp(theSpace, theSpace);

A = I + 2*Ktilde + 2*S;

% evaluate the right-hand side in the collocation points
X = theSpace.collocPoints.X;
Y = theSpace.collocPoints.Y;
NX = theSpace.collocPoints.nX;
NY = theSpace.collocPoints.nY;
f = rhsFunc(X,Y,NX,NY);

% set up method, fix quadrature rules
theMethod = methods.Collocation( A, f );
if (nargin > 2)
    theMethod.setQuadRules( quadRules.t_c, quadRules.w_c, quadRules.t_s, quadRules.w_s );
end

% solve the problem
phi = theMethod.apply();

end

