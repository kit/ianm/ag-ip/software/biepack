%> @file IdentityOp.m
%> @brief File containing the BIEPack.ops.IdentityOp class.
% ======================================================================
%> @brief Class representing an identity operator
%
%> At the moment this class is pretty useless as it supports not methods.
%> Support for collocation and Galerkin methods is planned.
% ======================================================================
classdef IdentityOp < BIEPack.ops.GeneralOp
    
    properties
    end
    
    methods
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. 
        %>
        %> Only one space may be passed, definition and range space are 
        %> always equal for the identity. 
        function idOp = IdentityOp( defSpace )
            
            if ( nargin == 0 )
                defSpace = [];
            end
            
            idOp@BIEPack.ops.GeneralOp(defSpace,defSpace);
            
            idOp.randomName;
            idOp.name = strcat('IdentityOp_',idOp.name);
            
        end
        
        function A = getCollocationImplementation( idOp )
            
            A = eye( idOp.defSpace.dimension );
            
        end
        
        %> @brief Implementation of GeneralOp.checkRequirements()
        %
        %> At the moment, this class supports only the collocation method,
        %> so it returns false. for all other methods
        function isOk = checkRequirements( idOp, methodName ) %#ok<INUSL>
            
            if ( methodName == BIEPack.methods.Collocation.METHOD_NAME )
                isOk = true;
            else
                isOk = false;
            end
            
        end
        
        %> @brief Implementation of GeneralOp.getImplementation()
        %
        %
        %> At the moment, this class supports no methods so it always
        %> returns an empty matrix. 
        function A = getImplementation(idOp, methodName, varargin) 
            
            if ( methodName == BIEPack.methods.Collocation.METHOD_NAME )
                A = idOp.getCollocationImplementation();
            else            
                A = [];
            end
            
        end
        
    end
    
end

