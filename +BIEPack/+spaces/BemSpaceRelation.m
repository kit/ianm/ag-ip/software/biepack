%> @file BemSpaceRelation.m
%> @brief File containing the BIEPack.spaces.BemSpaceRelation class.
% ======================================================================
%> @brief A class to describe the relation of two BemSpace space defined on the
%> same curve based on related meshes. The class provides
%> interpolation/embedding functionality between both spaces.
%>
%> A BIEPack.meshes.RefinementRelation must exist between the two meshes
%> used to define the space. Both spaces also must use elements of the same
%> type (either only piecewise linears or piecewise constants).

classdef BemSpaceRelation < BIEPack.BIEPackObject


    properties (SetAccess = protected)
        
        %> @brief The space defined on the coarser mesh.
        smallerSpace = [];
        
        %> @brief The space defined on the finer mesh.
        largerSpace = [];
        
        %> @brief The BIEPack.meshes.RefinementRelation that connects the meshes
        %> underlying these spaces.
        meshRefRelation = [];
        
    end
    
    methods
        
        %> Inititalises the object. The parameters are optional and if they 
        %> are specified the function setSpaces() will be called.
        %>
        %> @param smallerSpace The space based on the coarser mesh.
        %> @param largerSpace The space based on the finer mesh.
        %>
        %> @retval relation The newly constructed BemSpaceRelation object.
        function relation = BemSpaceRelation( smallerSpace, largerSpace )
            
            if ( nargin > 1 )
                relation.setSpaces( smallerSpace, largerSpace );
            end
            
            relation.randomName;
            relation.name = strcat('BemSpaceRelation_',relation.name);
            
        end
        
        %> @brief Sets the spaces, both must be of type BemSpace. The underlying
        %> meshes must be connected through a RefinementRelation, that will
        %> be extracted by this method.
        %>
        %> @param smallerSpace The space based on the coarser mesh.
        %> @param largerSpace The space based on the finer mesh.
        function setSpaces( relation, smallerSpace, largerSpace )
            
            if ( ~ BIEPack.utils.derivedFrom(smallerSpace, 'BIEPack.spaces.BemSpace' ) )
                error( 'In BemSpaceRelation %s, method setSpaces(): argument 1 is not a BIEPack.spaces.BemSpace', relation.name );
            end
            
            if ( ~ BIEPack.utils.derivedFrom(largerSpace, 'BIEPack.spaces.BemSpace' ) )
                error( 'In BemSpaceRelation %s, method setSpaces(): argument 2 is not a BIEPack.spaces.BemSpace', relation.name );
            end
                                   
            % Build a chain of RefinementRelation objects that leads from
            % the finest to the coarsest mesh.
            refRelChain = largerSpace.theMesh.refinementRelation;
            
            if ( isempty( refRelChain ) )
                error( 'In BemSpaceRelation %s, method setSpaces(): argument 2 does not define a RefinementRelation', relation.name );
            end
            
            while ( refRelChain(end).coarseMesh ~= smallerSpace.theMesh )
            
                if ( isempty( refRelChain(end).coarseMesh.refinementRelation ) )
                    error( 'In BemSpaceRelation %s, method setSpaces(): no complete chain of RefinementRelations from finest to coarsest mesh.', relation.name );
                end
                
                refRelChain = [ refRelChain,  refRelChain(end).coarseMesh.refinementRelation ]; %#ok<AGROW>
                
            end
            
            % iteratively merge the RefinementRelation objects            
            while ( length(refRelChain) > 1 )
                refRelChain = [ BIEPack.meshes.RefinementRelation.merge( refRelChain(2), refRelChain(1) ), refRelChain(3:end) ];
            end
            
            relation.meshRefRelation = refRelChain;
            
            % TODO: Check that element types are compatible
            
            relation.smallerSpace = smallerSpace;
            relation.largerSpace = largerSpace;
            
        end
        
        
        %> @brief Restrict a function from the larger space to the smaller space.
        %>
        %> For a piecewise constant function, the mean value of the values
        %> at the collocation points of the fine mesh gives the value
        %> at the collocation point of the coarse map.
        %>
        %> For a piecewise linear function, the interpolation spline in the
        %> nodes of the smaller space is computed.
        %> 
        %> @param y_fine Representation of the function in the finer space.
        %>
        %> @retval y_coarse Representation of the function in the coarser mesh.
        function y_coarse = restrict(relation, y_fine)
            
            if ( isempty( relation.smallerSpace.nodalDOFs) )
                % we are dealing with piecewise constants                               
                
                y_coarse = zeros( relation.smallerSpace.dimension, 1);
                
                for j=1:length(y_coarse)
                    newElements = relation.meshRefRelation.elementMap{j};
                    y_coarse(j) = sum( y_fine( newElements ) ) / length(newElements);
                end
                
            else
                y_coarse = y_fine( relation.meshRefRelation.nodeMap );
            end
            
        end
        
        %> @brief Embed a function from the smaller space in the larger space.
        %>
        %> If the function is piecewise continuous, set function value
        %> in the new nodes. If it is piecewise constant, set the value from
        %> the old element also on the new elements.
        %>
        %> @param y_coarse Representation of the function in the coarser mesh.
        %> 
        %> @retval y_fine Representation of the function in the finer space.
        function y_fine = embed(relation, y_coarse)
            
            y_fine = zeros( relation.largerSpace.dimension, 1);
            
            if ( isempty( relation.smallerSpace.nodalDOFs) )
                % we are dealing with piecewise constants
                for j = 1:relation.smallerSpace.dimension
                    newElements = relation.meshRefRelation.elementMap{j};
                    y_fine( newElements ) = y_coarse(j);
                end
            else
                % we are dealing with a space of continuous functions
                
                % copy values from nodes in the coarse mesh
                y_fine( relation.meshRefRelation.nodeMap ) = y_coarse;
                
                % evaluate the function on new nodes
                y_fine ( relation.meshRefRelation.newNodeMap(1,:) ) ...
                    = relation.smallerSpace.evalOnElement( ...
                        y_coarse, ...
                        relation.meshRefRelation.newNodeMap(2,:), ...
                        relation.meshRefRelation.newNodeMap(3,:) );
                
            end
            
        end
        
    end
    
end

