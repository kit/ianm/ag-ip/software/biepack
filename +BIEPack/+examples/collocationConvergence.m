%> @file collocationConvergence.m
%> @brief skript for testing convergence rates of a collocation method

import BIEPack.*;

a = 4;
b = 1;
c = (a - b) / (a + b);

% define an ellipse and a BemMesh on it
curve = geom.Ellipse( [0;0], a, b );

% define a series of meshes and spaces
numMeshes = 5;
mesh{numMeshes} = [];
theSpace{numMeshes} = [];

mesh{1} = meshes.BemMesh( curve, 0.5 );
%mesh{1} = meshes.ParamCurveMesh( curve, 60 );

for j = 2:numMeshes
    mesh{j} = mesh{j-1}.uniformRefinement;
end

% Create the table head
fprintf('  N     max. error       EOC\n');
fprintf('=================================\n');

% solve the problems
phi{numMeshes} = [];
e = zeros(numMeshes,1);
for j = 1:numMeshes
    theSpace{j} = spaces.BemSpace( mesh{j}, spaces.BemSpace.PIECEWISE_LINEAR );
%    theSpace{j} = spaces.BemSpace( mesh{j}, spaces.BemSpace.PIECEWISE_CONST );

    intOp = ops.IdentityOp(theSpace{j}) + (-2) * ops.LaplaceDLOp(theSpace{j}, theSpace{j});

    % definition of right-hand side
    t = acos( theSpace{j}.collocPoints.X / a );
    negs = ( theSpace{j}.collocPoints.Y < 0 );
    t(negs) = - t(negs);
    psi = exp(cos(t)) .* cos(sin(t)) + exp( c * cos(t) ) .* cos( c* sin(t) );

    theMethod = methods.Collocation( intOp, psi );

    % solve the problem
    phi{j} = theMethod.apply(); 
    
    % compute the error to the exact solution
    e(j) = max( abs( phi{j} - exp(cos(t)) .* cos(sin(t)) ) );
    fprintf('%4d   %e  ', theSpace{j}.dimension, e(j) );
    if (j > 1)
        fprintf(' %f', log( e(j-1) ./ e(j) ) / log(2) );
    end
    fprintf('\n');    
end 
fprintf('\n\n\n');

% produce a plot of the solution on the finest grid an the error on the
% finest grid. We need to sort t because for a BemMesh, collocation points
% are not in order.
[t_sort, index] = sort(t);

figure,
plot(t_sort, phi{numMeshes}(index) );

figure
semilogy( t_sort, abs( phi{numMeshes}(index) - exp(cos(t_sort)) .* cos(sin(t_sort)) ) );

