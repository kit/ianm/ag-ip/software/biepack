%> @file Curve.m
%> @brief Contains the geom.Curve class.
% ======================================================================
%> @brief Abstract base class for all curves.
%>
%> The class provides storage for basic topological properties (smoothness, 
%> closedness) of the curve as well as an interface for applying geometrical
%> transforms to a curve. A plot function is also available.
%
classdef Curve < BIEPack.BIEPackObject
    % BIEPack.geom.Curve < BIEPack.BIEPackObject

    properties (SetAccess = protected)
        %> @brief Global order of smoothness of the curve. While individual 
        %> curve segments may be smooth, this property reflects smoothness 
        %> across points connecting segments or accross the starting/ending
        %> points of closed curves.
        globalSmoothness = 0;
        
        %> @brief True whenever starting and ending points of the curve 
        %> are the same.
        closed = false;
        
    end
    
    methods (Abstract)
        
        %> @brief Obtains a list of several properties of the curve.
        %>
        %> Each Curve ultimately is made up of individual segments which
        %> each are a @link BIEPack.geom.ParamCurve ParamCurve @endlink
        %> instance. This function provides a list of these objects,
        %> without any information of their relation to each other.
        %>
        %> @retval listOfCurves An array containing the ParamCurve objects.
        listOfCurves = getParamCurveList(curve);
        
        
        %> @brief Translates a Curve object in direction of a given vector
        %> d.
        %>
        %> @param d Two component vector, direction by which the curve is 
        %> translated.
        %>
        %> @retval the Curve object itself.
        h = translate(curve,d);
        
        
        %> @brief Rotates a Curve object from given angle and central point. 
        % p may be omitted to rotate around the origin.
        %>        
        %> @param phi Angle of rotation.
        %> @param p Vector containing the coordinates of the center of rotation. 
        %>   Can be omitted to rotate around the origin.
        %>
        %> @retval the Curve object itself.
        h = rotate(curve, phi, p);
        
        
        %> @brief Reflection of a curve at either a point or a line.
        %>        
        %> @param arg1 Two component vector. If both arg1 and arg2 are omitted reflect at origin.
        %> @param arg2 Two component vector. When arg2 is omitted reflect
        %> at point arg1. Otherwise reflect at the line arg1 + t * arg2.
        %>
        %> @retval the Curve object itself.
        h = reflect(curve, arg1, arg2);
        
        %> @brief Plots the curve. For each parameterised segment draws a line
        %> segment with n points.
        %>        
        %> @param n Number of points in each segment.
        plot(curve,n);
    end
    
end

