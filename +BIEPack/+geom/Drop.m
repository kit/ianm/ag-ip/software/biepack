%> @file Drop.m
%> @brief Contains the geom.Drop class.
% ======================================================================
%> @brief A drop shaped obstacle of Elschner/Graham, Numer. Math 1995
%>
%> The obstacle has a single corner in the origin which has an exterior
%> angle of @f$(1 + \chi) \,pi@f$ between the tangents
classdef Drop < BIEPack.geom.ParamCurve    
    
    properties
        
        %> @brief parameter in (0,1) defining the exterior angle in the 
        %> corner point
        chi = 0.5;
        
    end
    
    methods
        
        %> @brief Constructs a Colton/Kress kite. 
        %> A random name starting with 'Kite_' is also set.
        %>
        %> @retval kite The newly constructed Kite object.
        function drop = Drop(chi)
            
            drop.chi = chi;
            
            drop.randomName;
            drop.name = strcat('Drop_',drop.name);
            
            drop.startParam = 0;
            drop.endParam = pi;
            
            drop.closed = false;
            drop.smoothness = inf;
            drop.globalSmoothness = 0;
            drop.highestImplementedDerivative = 2;
        end
        
        %> @brief Implementation of basic parametrization 
        %>
        %> The parametrization of the teardrop shaped region is
        %> @f[
        %>   \gamma(t) = \sin(t) \, (\cos( (1-\chi) \, t), \sin( (1-\chi) \, t ) )^\top, \qquad t \in [0,pi]
        %> @f]
        %>
        %> Derivatives of the parametrization up to second order are
        %> implemented.
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. 
        %> @retval y2 2nd coordinate of the curve points.
        function [y1, y2] = paramEval(drop, t, n)
            
            if ( n == 0 )
                y1 = sin(t) .* cos( (1-drop.chi) * t );
                y2 = sin(t) .* sin( (1-drop.chi) * t );
            elseif ( n == 1 )
                y1 = cos(t) .* cos( (1-drop.chi) * t ) - (1-drop.chi) * sin(t) .* sin( (1-drop.chi) * t );
                y2 = cos(t) .* sin( (1-drop.chi) * t ) + (1-drop.chi) * sin(t) .* cos( (1-drop.chi) * t );
            else
                y1 = -sin(t) .* cos( (1-drop.chi) * t ) - 2*(1-drop.chi) * cos(t) .* sin( (1-drop.chi) * t ) ...
                    - (1-drop.chi)^2 * sin(t) .* cos( (1-drop.chi) * t );
                y2 = -sin(t) .* sin( (1-drop.chi) * t ) + 2*(1-drop.chi) * cos(t) .* cos( (1-drop.chi) * t ) ...
                    - (1-drop.chi)^2 * sin(t) .* sin( (1-drop.chi) * t );
            end
            
        end
        
    end
    
end

