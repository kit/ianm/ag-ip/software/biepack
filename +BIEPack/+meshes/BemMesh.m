%> @file BemMesh.m
%> @brief Contains the meshes.BemMesh class.
% ======================================================================
%> @brief A mesh defined on a general Curve, to be used for a BEM scheme.
%>
%> The mesh can be based on any curve that is either a ParamCurve or
%> made up of ParamCurve objects via CurveUnion objects. The curve
%> segments are divided into elements, each separated by two nodes.
%> Thus, each element is associated with one ParamCurve, a node can be 
%> associated with two ParmaCurve objects if it is an end point and 
%> connectivity is known (via a PiecewiseCurve).
%>
%> Association of an element to a curve segment can be obtained
%> through the curveIndexOfElement property. This array contains, for
%> each element, the index of the corresponding ParamCurve in the 
%> theParamCurves cell array.
%>
%> Information about the association of nodes is provided by the
%> nodeToLocalDOFMap property.
%>
%> Before a node can be generated, the curve the mesh is based on has 
%> to be defined via the setCurve() method.Subsequently, a call to
%> the generateElements() method can be used to construct the elements
%> and nodes with a prescribed maximum mesh width. These steps can
%> also be performed direcly on construction by passing the
%> corresponding arguments to the constructor.
%>
%> @todo add an hmax property containing the maximal diameter of an
%>   element. This could also be placed in the Mesh class.
% ======================================================================
classdef BemMesh < BIEPack.meshes.Mesh
    % BIEPack.meshes.BemMesh < BIEPack.meshes.Mesh
    %   
    
    properties (SetAccess = protected)
        
        %> @brief The original Curve object that the mesh is defined on.
        theCurve = [];
        
        %> @brief Cell array containing all ParamCurve objects that 
        %> theCurve is made up of.
        theParamCurves = {};
        
        %> @brief Row vector of length numElements. For each element it
        %> contains the index of the corresponding ParamCurve object in
        %> theParamCurves.
        curveIndexOfElement = [];
        
        %> @brief Row vector of length numElements. For each element, it
        %> contains its start parameter value. This parameter value
        %> yields the first node in the attribute elements inherited from
        %> the BIEPack.meshes.Mesh class.
        startParameter = [];       

        %> @brief Row vector of length numElements. For each element
        %> contains its end parameter value. This parameter value
        %> yields the second node in the attribute elements inherited from
        %> the BIEPack.meshes.Mesh class.
        endParameter = [];
        
        %> @brief Matrix with numNodes columns. For each node, this array 
        %> contains a column. The odd rows contain the indeces of the 
        %> elements this node is part of, the even rows cotain the number 
        %> of the degree of freedom the node is associated with. For each 
        %> element, degrees of freedom 1 and 2 are associated with its 
        %> start and end point, respectively. 
        %
        % Probably, we do not want this here.
%        nodeToLocalDOFMap = [];
        
        %> Array of length @a numNodes. Size of the left hand side angle of
        %> the curve at this point. For a smooth point, the value is pi.
        angleAtNode = [];
        
        %> Array of length @a numNodes. The value is true, if the curve has
        %> a normal at this node point, otherwise false.
        normalAtNodeExists = [];
        
        %> Array of length @numNodes. X-component of the unit normal
        %> pointing to the right (i.e. outward for a positively oriented closed
        %> curve) at this node. The value will only be set if the normal
        %> exists (i.e. no corner point).
        normalAtNodeX = [];
        
        %> Array of length @numNodes. Y-component of the unit normal
        %> pointing to the right (i.e. outward for a positively oriented closed
        %> curve) at this node. The value will only be set if the normal
        %> exists (i.e. no corner point).
        normalAtNodeY = [];
    end
    
    
    methods (Static)
        
        %> @brief Generate nodes on a ParamCurve with given mesh size.
        %>
        %> The function computes the parameter values and coordinates for 
        %> nodes on a ParamCurve such that adjacent nodes are no more than a
        %> distance of h apart.
        %>
        %> The function first computes an estimate LEst for the length of the
        %> curve using a 5 point Simpson rule. From this it obtains an
        %> initial number of elements NInit and distributes the nodes
        %> uniformly in parameter space on the curve.
        %>
        %> The elements are then checked whether they satisfy the mesh size
        %> constraint. Elements that are too large are marked and refined by
        %> placing a new node in the middle. This process is repeated for
        %> the refined elements until all elements satisfy the constraint.
        %>
        %> @param curve ParamCurve object on which the nodes are generated.
        %> @param h Maximum distance that two adjacent nodes will be apart
        %> from.
        %>
        %> @retval nodeParams The parameter values of the Curve at the
        %> nodes.
        %> @retval nodeCoordsX The x1-coordinates of the newly generated
        %> nodes.
        %> @retval nodeCoordsY The x2-coordinates of the newly generated
        %> nodes.
        function [ nodeParams, nodeCoordsX, nodeCoordsY ] = generateNodesForParamCurve( curve, h )
            
            if ( ~ BIEPack.utils.derivedFrom( curve, 'BIEPack.geom.ParamCurve' ) )
                error( 'In BemMesh.generateNodesForParamCurve: argument 1 is not a BIEPack.geom.ParamCurve');
            end
            
            % Estimate the length of the curve by a five point Simpson rule
            t = linspace(0,1,5);
            CLength = curve.endParam - curve.startParam;
            [dy1, dy2] = curve.eval( curve.startParam + t * CLength ,1);
            normDy = sqrt( dy1.^2 + dy2.^2 );   
            LEst = CLength / 12 * normDy * [1; 4; 2; 4; 1];
            
            % Initial number of elements
            NInit = ceil ( LEst / (0.9*h) );
            
            % Initial elements
            nodeParams = linspace(curve.startParam, curve.endParam, NInit+1);
            [ nodeCoordsX, nodeCoordsY ] = curve.eval( nodeParams, 0);
            diameters = sqrt( (nodeCoordsX(2:NInit+1) - nodeCoordsX(1:NInit)).^2 ...
                + (nodeCoordsY(2:NInit+1) - nodeCoordsY(1:NInit)).^2 );
            marks = diameters > h;
            
            totalNumNodes = length(nodeParams);
            numNewNodes = sum(marks);
            
            % refine until all elements are small enough
            while ( numNewNodes > 0 )
                
                % compute old to new index map
                oldToNewIndeces = (1:totalNumNodes) + marks*triu(ones(totalNumNodes-1,totalNumNodes),1);                             
                 
                totalNumNodes = totalNumNodes + numNewNodes;
                
                % initialize new lists
                newParams = zeros(1,totalNumNodes);
                newNodeCoordsX = newParams;
                newNodeCoordsY = newParams;
                
                newParams(oldToNewIndeces) = nodeParams;
                newNodeCoordsX(oldToNewIndeces) = nodeCoordsX;
                newNodeCoordsY(oldToNewIndeces) = nodeCoordsY;
                
                newNodeIndeces = true(1,totalNumNodes);
                newNodeIndeces(oldToNewIndeces) = false;
                
                % mark all elements with a new node
                marks = [newNodeIndeces 0 ] | [0 newNodeIndeces];
                marks = marks(2:end-1);
                
                % compute new node coordinates
                newParams(newNodeIndeces) = ( newParams(circshift(newNodeIndeces,1,2)) ...
                    + newParams(circshift(newNodeIndeces,-1,2)) ) / 2;
                [newX, newY] = curve.eval( newParams(newNodeIndeces), 0 );
                newNodeCoordsX(newNodeIndeces) = newX;
                newNodeCoordsY(newNodeIndeces) = newY;
                
                % store in original variables
                nodeParams = newParams;
                nodeCoordsX = newNodeCoordsX;
                nodeCoordsY = newNodeCoordsY;
                
                % compute the diameters of the refined elements and mark
                % the ones needing further refinement.                
                diameters = sqrt( ...
                        ( nodeCoordsX([marks false]) - nodeCoordsX([false marks ]) ).^2 ...
                        + ( nodeCoordsY([marks false]) - nodeCoordsY([false marks ]) ).^2 ...
                    );
                stillToLarge = diameters > h;
                marks(marks) = stillToLarge;
                numNewNodes = sum(stillToLarge);
            end
        end        
        
    end
    
    
    methods
        
        %> @brief Set a new curve to base the mesh on.
        %>
        %> This invalidates all data, so the bemMesh is cleared.
        %>
        %> Note theat calling this function invalidates all mesh refinement
        %> relations for this mesh.
        %>
        %> @param curve The Curve object on which the mesh will be
        %> generated.
        function setCurve( bemMesh, curve )
            
            if ( ~ BIEPack.utils.derivedFrom( curve, 'BIEPack.geom.Curve' ) )
                error( 'In BemMesh.setCurve: argument 1 is not a BIEPack.geom.Curve');
            end
            
            bemMesh.theCurve = curve;
            bemMesh.theParamCurves = {};
            bemMesh.curveIndexOfElement = [];
            bemMesh.startParameter = [];
            bemMesh.endParameter = [];
            bemMesh.numNodes = 0;
            bemMesh.nodesX = [];
            bemMesh.nodesY = [];
            bemMesh.angleAtNode = [];
            bemMesh.normalAtNodeExists = [];
            bemMesh.normalAtNodeX = [];
            bemMesh.normalAtNodeY = [];
            bemMesh.numElements = 0;
            bemMesh.elements = [];
            bemMesh.refinementRelation = [];   
            
        end
               
        %> @brief Generate the elements with a maximal mesh size of h.
        %>
        %> If the curve  has not been set when this function is called, an
        %> eror is generated.
        %>
        %> If the function is called with just the h argument, the elements
        %> are generated for the entire curve. All previously stored 
        %> information is lost. The curve is broken into its segments and
        %> elements are constructed for each segment in turn, respecting
        %> connectivity and orientation information in any PiecewiseCurve
        %> segment.
        %>
        %> The additional arguments are used for recursive calls to the
        %> function during the generating process.
        %>
        %> - Initially, curveSegment is the entire curve the mesh is based
        %>   on.
        %>
        %> - If curveSegment is a CurveUnion object, the function is called
        %>   recursively for each segement of the curve (passed as parameter
        %>   curveSegment). The generated elements and nodes are added to
        %>   existing lists of elements and nodes, respectively.
        %>
        %> - If curveSegment is a PiecewiseCurve object, the function is
        %>   called recursively for each ParamCurve segment. The index of
        %>   the last node of the previous segment is passed as startNode
        %>   and reversed is set to true if the orientation is -1. If the
        %>   PiecewiseCurve is closed, the last generated node is replaced
        %>   by the very first node of the PiecewiseCurve.
        %>
        %> - If the curveSegement is a ParamCurve, the nodes are generated
        %>   with the static generateNodesForParamCurve() function. Elements
        %>   are place between adjacent nodes. There ordering is flipped if 
        %>   reversed was true. If the ParamCurve is closed, the last 
        %>   generated node is replaced by the first node.
        %>
        %> @todo Throw an eroor if the property @a theCurve is not set.
        %>
        %> @param h The maximum distance that two adjacent nodes will be apart
        %> from each other.
        %> @param curveSegment The current segment of the curve, for which
        %> the elements are generated
        %> @param startNode The index of the last node of the previous
        %> curveSegment.
        %> @param reversed True, whenever the orientation of the current
        %> ParamCurve object is -1.
        function generateElements( bemMesh, h, curveSegment, startNode, reversed)
            
            % Initialize optional parameters
            if (nargin < 5)
                reversed = false;
            end
            if (nargin < 4)
                startNode = 0;
            end
            if (nargin < 3)
                curveSegment = bemMesh.theCurve;
            end
            
            % Are we dealing with the entire curve? In that case reset all
            % data.
            if ( curveSegment == bemMesh.theCurve )
                bemMesh.numElements = 0;
                bemMesh.elements = [];
                bemMesh.theParamCurves = {};
                bemMesh.curveIndexOfElement = [];
                bemMesh.startParameter = [];
                bemMesh.endParameter = [];
                bemMesh.numNodes = 0;
                bemMesh.nodesX = [];
                bemMesh.nodesY = [];
                bemMesh.angleAtNode = [];
                bemMesh.normalAtNodeExists = [];
                bemMesh.normalAtNodeX = [];
                bemMesh.normalAtNodeY = [];
%                bemMesh.nodeToLocalDOFMap = [];
%                bemMesh.curveIndecesOfNode = [];
                bemMesh.refinementRelation = []; 
            end
            
            % If the segment we are generating for is some ParamCurve,
            % generate the nodes and elements and append to existing lists
            
            if ( BIEPack.utils.derivedFrom( curveSegment, 'BIEPack.geom.ParamCurve' ) )
                
                % set the curve list
                bemMesh.theParamCurves{end+1} = curveSegment;
                curveIndex = length( bemMesh.theParamCurves );
                
                % Compute nodes on this curve
                [ nodeParams, nodeCoordsX, nodeCoordsY ] ...
                    = BIEPack.meshes.BemMesh.generateNodesForParamCurve( curveSegment, h );
                
                % if orientation is reversed, flip these arrays
                if ( reversed )
                    nodeParams = fliplr(nodeParams);
                    nodeCoordsX = fliplr(nodeCoordsX);
                    nodeCoordsY = fliplr(nodeCoordsY);
                    normalDir = -1;
                else
                    normalDir = 1;
                end
                
                % Append new data to existing mesh data 
                bemMesh.startParameter = [ bemMesh.startParameter nodeParams(1:end-1) ];
                bemMesh.endParameter = [ bemMesh.endParameter nodeParams(2:end) ];
                
                numNewElements = length(nodeParams)-1;
                bemMesh.curveIndexOfElement = [ bemMesh.curveIndexOfElement curveIndex * ones(1,numNewElements) ];
                
                % check whether the first node has already been created
                if ( startNode == 0 )
                    
                    % no start node exists
                    newElements = [ 2*ones(1,numNewElements); ...
                                    bemMesh.numNodes + (1:numNewElements); ...
                                    bemMesh.numNodes + (2:numNewElements+1) ];                
                    bemMesh.elements = [ bemMesh.elements newElements ];
                
                    numNewNodes = numNewElements+1;
                    bemMesh.nodesX = [ bemMesh.nodesX nodeCoordsX ];
                    bemMesh.nodesY = [ bemMesh.nodesY nodeCoordsY ];
                    
                    % initialize corner point information
                    bemMesh.angleAtNode = [bemMesh.angleAtNode pi * ones(1,numNewNodes) ];
                    bemMesh.normalAtNodeExists = [ bemMesh.normalAtNodeExists true(1,numNewNodes) ]; 
                    [normalX, normalY] = curveSegment.normal(nodeParams);
                    normalNorm = sqrt( normalX.^2 + normalY.^2 );
                    bemMesh.normalAtNodeX =   [ bemMesh.normalAtNodeX normalDir * normalX ./ normalNorm ];
                    bemMesh.normalAtNodeY = [ bemMesh.normalAtNodeY normalDir * normalY ./ normalNorm ];
                    
                    % Correct starting point
                    bemMesh.angleAtNode(bemMesh.numNodes + 1) = 2*pi;
                    bemMesh.normalAtNodeExists(bemMesh.numNodes + 1) = false;
                    
%                     newDOFMap = zeros(4,numNewNodes);
%                     newDOFMap(1,1:end-1) = bemMesh.numElements + (1:numNewElements);
%                     newDOFMap(2,1:end-1) = 1;
%                     newDOFMap(3,2:end-1) = bemMesh.numElements + (1:numNewElements-1);
%                     newDOFMap(4,2:end-1) = 2;
%                     newDOFMap(1,end) = bemMesh.numElements + numNewElements;
%                     newDOFMap(2,end) = 2;
%                    bemMesh.nodeToLocalDOFMap = [ bemMesh.nodeToLocalDOFMap newDOFMap ];

                    bemMesh.numElements = bemMesh.numElements + numNewElements;
                    bemMesh.numNodes = bemMesh.numNodes + numNewNodes;

                    if ( curveSegment.closed )
                        % The last node is the same as the first node
                        bemMesh.numNodes = bemMesh.numNodes - 1; 
                        numNewNodes = numNewNodes - 1;
                        bemMesh.nodesX = bemMesh.nodesX(1:end-1);
                        bemMesh.nodesY = bemMesh.nodesY(1:end-1);
                        
                        nXend = bemMesh.normalAtNodeX(end);
                        nYend = bemMesh.normalAtNodeY(end);
                        
                        bemMesh.angleAtNode = bemMesh.angleAtNode(1:end-1);
                        bemMesh.normalAtNodeExists = bemMesh.normalAtNodeExists(1:end-1); 
                        bemMesh.normalAtNodeX = bemMesh.normalAtNodeX(1:end-1);
                        bemMesh.normalAtNodeY = bemMesh.normalAtNodeY(1:end-1);
                        
                        % check whether the normal exists at last/first
                        % node
                        if ( curveSegment.globalSmoothness > 0 )
                            bemMesh.angleAtNode(bemMesh.numNodes - numNewNodes + 1) = pi;
                            bemMesh.normalAtNodeExists(bemMesh.numNodes -numNewNodes + 1) = true;
                        else
                            cosAngle = nXend * bemMesh.normalAtNodeX( bemMesh.numNodes - numNewNodes + 1 ) ...
                                + nYend * bemMesh.normalAtNodeY( bemMesh.numNodes - numNewNodes + 1 );
                            sinAngle = nXend * bemMesh.normalAtNodeY( bemMesh.numNodes - numNewNodes + 1 ) ...
                                - nYend * bemMesh.normalAtNodeX( bemMesh.numNodes - numNewNodes + 1 );
                            angleBetweenNormals = atan2(sinAngle,cosAngle);
                            bemMesh.angleAtNode(bemMesh.numNodes - numNewNodes + 1) = pi - angleBetweenNormals;
                            bemMesh.normalAtNodeExists(bemMesh.numNodes -numNewNodes + 1) = false;
                        end 
                        
                        bemMesh.elements(3,end) = bemMesh.numNodes - numNewNodes + 1;
                        
%                         bemMesh.nodeToLocalDOFMap = bemMesh.nodeToLocalDOFMap(:,1:end-1);
%                         bemMesh.nodeToLocalDOFMap(3,bemMesh.numNodes - numNewNodes + 1) = bemMesh.numElements;
%                         bemMesh.nodeToLocalDOFMap(4,bemMesh.numNodes - numNewNodes + 1) = 2;
                    elseif ( curveSegment == bemMesh.theCurve )
                       % if where dealing with the entire curve and it is
                       % not closed, the normal does not exist at the last
                       % node
                        bemMesh.angleAtNode(end) = 2*pi;
                        bemMesh.normalAtNodeExists(end) = false;
                       
                    end
%                    bemMesh.curveIndecesOfNode = [ bemMesh.curveIndecesOfNode curveIndex * ones(1,numNewNodes) ];
                    
                else
                    
                    % the start node has been created before 
                    newElements = [ 2*ones(1,numNewElements); ...
                                    startNode, bemMesh.numNodes + (1:numNewElements-1); ...
                                    bemMesh.numNodes + (1:numNewElements) ];                
                    bemMesh.elements = [ bemMesh.elements newElements ];
                
                    numNewNodes = numNewElements;
                    bemMesh.nodesX = [ bemMesh.nodesX nodeCoordsX(2:end) ];
                    bemMesh.nodesY = [ bemMesh.nodesY nodeCoordsY(2:end) ];
                    
                    % initialize corner point information
                    bemMesh.angleAtNode = [bemMesh.angleAtNode pi * ones(1,numNewNodes) ];
                    bemMesh.normalAtNodeExists = [ bemMesh.normalAtNodeExists true(1,numNewNodes) ]; 
                    [normalX, normalY] = curveSegment.normal(nodeParams);
                    normalNorm = sqrt( normalX.^2 + normalY.^2 );
                    bemMesh.normalAtNodeX =   [ bemMesh.normalAtNodeX normalDir * normalX(2:end) ./ normalNorm(2:end) ];
                    bemMesh.normalAtNodeY = [ bemMesh.normalAtNodeY normalDir * normalY(2:end) ./ normalNorm(2:end) ];
                    
                    % Correct starting point: is it a corner point or not?
                    nXstart = normalDir * normalX(1) ./ normalNorm(1);
                    nYstart = normalDir * normalY(1) ./ normalNorm(1);
                    cosAngle = nXstart * bemMesh.normalAtNodeX(end-numNewNodes) ...
                        + nYstart * bemMesh.normalAtNodeY(end-numNewNodes) ;
                    sinAngle = -nXstart * bemMesh.normalAtNodeY(end-numNewNodes) ...
                        + nYstart * bemMesh.normalAtNodeX(end-numNewNodes);
                    angleBetweenNormals = atan2(sinAngle,cosAngle);
                    if ( abs( angleBetweenNormals ) > 1e-14 )
                        % there is an angle different from pi here.
                        bemMesh.angleAtNode(end-numNewNodes) = pi- angleBetweenNormals;
                        bemMesh.normalAtNodeExists(end-numNewNodes) = false;
                    end
                    
%                     newDOFMap = zeros(4,numNewNodes);
%                     newDOFMap(1,1:end-1) = bemMesh.numElements + (2:numNewElements);
%                     newDOFMap(2,1:end-1) = 1;
%                     newDOFMap(3,1:end-1) = bemMesh.numElements + (1:numNewElements-1);
%                     newDOFMap(4,1:end-1) = 2;
%                     newDOFMap(1,end) = bemMesh.numElements + numNewElements;
%                     newDOFMap(2,end) = 2;
%                     bemMesh.nodeToLocalDOFMap = [ bemMesh.nodeToLocalDOFMap newDOFMap ];
%                     bemMesh.nodeToLocalDOFMap(3,startNode) = bemMesh.numElements + 1;
%                     bemMesh.nodeToLocalDOFMap(4,startNode) = 1;

                    bemMesh.numElements = bemMesh.numElements + numNewElements;
                    bemMesh.numNodes = bemMesh.numNodes + numNewNodes;
                    
                    % if a start node existed before, this is not a closed curve, so no code for that 
                    
%                    bemMesh.curveIndecesOfNode = [ bemMesh.curveIndecesOfNode curveIndex * ones(1,numNewNodes) ];
                    
                end
                
            elseif ( BIEPack.utils.derivedFrom( curveSegment, 'BIEPack.geom.PiecewiseCurve' ) )
                
                % store the index of the first node to be created to be
                % used if the curveSegment is closed
                veryFirstNode = bemMesh.numNodes + 1;
                
                % iterate over all subcurves, providing start node except
                % for the first
                % also observe orientations
                bemMesh.generateElements( h, curveSegment.segments{1}, 0, ...
                    logical(curveSegment.orientations(1)-1) );
                
                for j=2:length(curveSegment.segments)
                    bemMesh.generateElements( h, curveSegment.segments{j}, bemMesh.numNodes, ...
                        logical(curveSegment.orientations(j)-1) );
                end
                
                % if the PiecewiseCurve is closed, the last node is
                % identical to the first one
                if ( curveSegment.closed )
                    bemMesh.numNodes = bemMesh.numNodes - 1; 
                    bemMesh.nodesX = bemMesh.nodesX(1:end-1);
                    bemMesh.nodesY = bemMesh.nodesY(1:end-1); 
                    
                    % save normal for last node on last segment
                    nXend = bemMesh.normalAtNodeX(end);
                    nYend = bemMesh.normalAtNodeY(end);                    
                                           
                    bemMesh.angleAtNode = bemMesh.angleAtNode(1:end-1);
                    bemMesh.normalAtNodeExists = bemMesh.normalAtNodeExists(1:end-1); 
                    bemMesh.normalAtNodeX = bemMesh.normalAtNodeX(1:end-1);
                    bemMesh.normalAtNodeY = bemMesh.normalAtNodeY(1:end-1);
                    
                    % Correct starting point: is it a corner point or not?
                    cosAngle = nXend * bemMesh.normalAtNodeX(veryFirstNode) ...
                        + nYend * bemMesh.normalAtNodeY(veryFirstNode) ;
                    sinAngle = nXend * bemMesh.normalAtNodeY(veryFirstNode) ...
                        - nYend * bemMesh.normalAtNodeX(veryFirstNode);
                    angleBetweenNormals = atan2(sinAngle,cosAngle);
                    if ( abs( angleBetweenNormals ) > 1e-14 )
                        % there is an angle different from pi at the first node.
                        bemMesh.angleAtNode(veryFirstNode) = pi - angleBetweenNormals;
                        bemMesh.normalAtNodeExists(veryFirstNode) = false;
                    end                    
                    
                    bemMesh.elements(3,end) = veryFirstNode;
%                     bemMesh.nodeToLocalDOFMap = bemMesh.nodeToLocalDOFMap(:,1:end-1);
%                     bemMesh.nodeToLocalDOFMap(3,veryFirstNode) = bemMesh.numElements;
%                     bemMesh.nodeToLocalDOFMap(4,veryFirstNode) = 2;
                else
                    % if the PieceWiseCurve is not closed, the normal does
                    % not exist at the last point.
                    bemMesh.angleAtNode(end) = 2*pi;
                    bemMesh.normalAtNodeExists(end) = false;
                end
                
                
            elseif ( BIEPack.utils.derivedFrom( curveSegment, 'BIEPack.geom.CurveUnion' ) )

                % iterate over all subcurves and call this function
                % recursively for each                
                for j=1:length(curveSegment.segments)
                    bemMesh.generateElements( h, curveSegment.segments{j} );
                    
                    if ( BIEPack.utils.derivedFrom( curveSegment.segments{j}, 'BIEPack.geom.ParamCurve' ) ...
                            && ~ curveSegment.segments{j}.closed )
                        % The normal does not exist at the last point.
                        bemMesh.angleAtNode(end) = 2*pi;
                        bemMesh.normalAtNodeExists(end) = false;
                        
                    end
                end
                
            else                
                error( 'In BemMesh.generateElements: argument curveSegment is not a curve type I can handle');
            end
            
        end

        
        %> @brief Construction. All parameters are optional.
        %>
        %> If the BIEPack.geom.Curve object 'curve' is provided, this is set
        %> as the curve the mesh is based on. If additionally, the parameter
        %> h is provided, the elements are generated.
        %> @param curve Curve that the mesh will be defined on.
        %> @param h Maximum distance that the nodes will be apart from each
        %> other.
        %>
        %> @retval bemMesh The newly constructed BemMesh object.
        function bemMesh = BemMesh( curve, h )
            
            if ( nargin > 0 && ~isempty(curve) )
                bemMesh.setCurve( curve );
            end
            
            if ( nargin > 1 )
                bemMesh.generateElements( h );
            end
            
            bemMesh.randomName;
            bemMesh.name = strcat('BemMesh_',bemMesh.name);
        end
        
        %> @brief Generates a uniform refinement of the mesh, such that inbetween
        %> two nodes, a new node is added in the middle of the parameter
        %> interval. The finer mesh will be a BemMesh object aswell.
        %>
        %> @param coarsMesh Initial mesh that will be refined.
        %>
        %> @retval fineMesh The newly refined mesh, also a BemMesh object.
        %> @retval relation Object of the class RefinementRelation,
        %> describing the refinement.
        function [ fineMesh, relation ] = uniformRefinement( coarseMesh )
            
            if ( coarseMesh.numNodes == 0 )
                error( 'In BemMesh.uniformRefinement(): elements must have been generated for refinement.');
            end
            
            fineMesh = BIEPack.meshes.BemMesh( coarseMesh.theCurve );            
            fineMesh.name = strcat('unif_ref_of_',coarseMesh.name);
            
            fineMesh.numElements = 2*coarseMesh.numElements;
            fineMesh.curveIndexOfElement = zeros(1, fineMesh.numElements );
            fineMesh.theParamCurves = coarseMesh.theParamCurves;
            fineMesh.startParameter = zeros(1, fineMesh.numElements );
            fineMesh.endParameter = zeros(1, fineMesh.numElements );
            
            fineMesh.curveIndexOfElement(1:2:2*coarseMesh.numElements-1) = coarseMesh.curveIndexOfElement;
            fineMesh.curveIndexOfElement(2:2:2*coarseMesh.numElements) = coarseMesh.curveIndexOfElement;
            fineMesh.startParameter(1:2:2*coarseMesh.numElements-1) = coarseMesh.startParameter;
            newT = 0.5 * ( coarseMesh.startParameter + coarseMesh.endParameter );
            fineMesh.startParameter(2:2:2*coarseMesh.numElements) = newT;
            fineMesh.endParameter(1:2:2*coarseMesh.numElements-1) = newT;
            fineMesh.endParameter(2:2:2*coarseMesh.numElements) = coarseMesh.endParameter;
            
            elementMap = cell(1,coarseMesh.numElements);
            for j=1:coarseMesh.numElements
                elementMap{j} = [ 2*j-1 2*j ];
            end
            
            % For every element in the coarse mesh, we obtain its mid point
            % as a new node. These automatically are smooth points at which
            % the normal exists
            fineMesh.numNodes = coarseMesh.numNodes + coarseMesh.numElements;
            fineMesh.nodesX = zeros(1,fineMesh.numNodes);
            fineMesh.nodesY = zeros(1,fineMesh.numNodes);
            fineMesh.nodesX(1:coarseMesh.numNodes) = coarseMesh.nodesX;
            fineMesh.nodesY(1:coarseMesh.numNodes) = coarseMesh.nodesY;
            fineMesh.angleAtNode(1:coarseMesh.numNodes) = coarseMesh.angleAtNode;
            fineMesh.normalAtNodeExists(1:coarseMesh.numNodes) = coarseMesh.normalAtNodeExists;
            fineMesh.normalAtNodeX(1:coarseMesh.numNodes) = coarseMesh.normalAtNodeX;
            fineMesh.normalAtNodeY(1:coarseMesh.numNodes) = coarseMesh.normalAtNodeY;
            newX = zeros(1, coarseMesh.numElements);
            newY = zeros(1, coarseMesh.numElements);
            newNX = zeros(1, coarseMesh.numElements);
            newNY = zeros(1, coarseMesh.numElements);
            for j=1:length( fineMesh.theParamCurves )
                index = ( coarseMesh.curveIndexOfElement == j );
                [ X, Y ] = fineMesh.theParamCurves{j}.eval( newT(index), 0 );
                newX(index) = X;
                newY(index) = Y;
                [ nX, nY ] = fineMesh.theParamCurves{j}.normal( newT(index) );
                nNorm = sqrt( nX.^2 + nY.^2 );
                newNX(index) = nX ./ nNorm;
                newNY(index) = nY ./ nNorm;
            end
            newNodes = coarseMesh.numNodes + (1:coarseMesh.numElements);
            fineMesh.nodesX( newNodes ) = newX;
            fineMesh.nodesY( newNodes ) = newY;
            fineMesh.angleAtNode( newNodes ) = pi;
            fineMesh.normalAtNodeExists( newNodes ) = true;
            fineMesh.normalAtNodeX( newNodes ) = newNX;
            fineMesh.normalAtNodeY( newNodes ) = newNY;
            
            newNodeMap = [ newNodes; 1:coarseMesh.numElements; 0.5*ones(size(newNodes)) ];
            
            fineMesh.elements = zeros(3,fineMesh.numElements);
            fineMesh.elements(1,:) = 2;  % each element has two nodes
            fineMesh.elements(2,1:2:2*coarseMesh.numElements-1) = coarseMesh.elements(2,:);
            fineMesh.elements(3,1:2:2*coarseMesh.numElements-1) = newNodes;
            fineMesh.elements(2,2:2:2*coarseMesh.numElements) = newNodes;
            fineMesh.elements(3,2:2:2*coarseMesh.numElements) = coarseMesh.elements(3,:);
            
%             % assemble fineMesh.nodeToLocalDOFMap
%             fineMesh.nodeToLocalDOFMap = zeros(4,fineMesh.numNodes);
%             oldMap = reshape(coarseMesh.nodeToLocalDOFMap, 2, 2*coarseMesh.numNodes);
%             index_dof1 = ( oldMap(2,:) == 1 );
%             oldMap(1,index_dof1) = 2*oldMap(1,index_dof1)-1;
%             oldMap(1,~index_dof1) = 2*oldMap(1,~index_dof1);
%             fineMesh.nodeToLocalDOFMap(:,1:coarseMesh.numNodes) = reshape(oldMap, 4, coarseMesh.numNodes );
%             fineMesh.nodeToLocalDOFMap(1,coarseMesh.numNodes+1:end) = 2:2:fineMesh.numElements;
%             fineMesh.nodeToLocalDOFMap(2,coarseMesh.numNodes+1:end) = 1;
%             fineMesh.nodeToLocalDOFMap(3,coarseMesh.numNodes+1:end) = 1:2:fineMesh.numElements-1;
%             fineMesh.nodeToLocalDOFMap(4,coarseMesh.numNodes+1:end) = 2;
            
            nodeMap = 1:coarseMesh.numNodes;
            relation = BIEPack.meshes.RefinementRelation(coarseMesh, fineMesh, nodeMap, newNodeMap, elementMap);
            fineMesh.refinementRelation = relation;
            
        end
        
                
        %> @brief Finds the index of the element containing the point on
        %> the ParamCurve with index curveIndex and parameter value
        %> curveParam.
        %>
        %> For each entry in curveIndex, a complete search over the mesh is
        %> carried out. This may be time-consuming.
        %>
        %> @param curveIndex a vector of indeces of ParamCurves of the
        %> BemMesh.
        %> @param cureParam vector of the same size as curveIndex
        %> containing the corresponding parameter values.
        %> @retval index a vector of  the same size as curveIndex containing the 
        %> element indeces.
        %> @retval param the vector of parameter values in the
        %> corresponding element.
        function [index, param] = elementCoordForParamValue(bemMesh, curveIndex, curveParam)
            
            index = zeros(size(curveIndex));
            for j=1:length(curveIndex)
                logicalIndex = (bemMesh.curveIndexOfElement == curveIndex(j)) ...
                    & (bemMesh.startParameter <= curveParam(j)) ...
                    & (bemMesh.endParameter >= curveParam(j));
                index(j) = find(logicalIndex, 1);                
            end
            
            param = (curveParam - bemMesh.startParameter(index)) ...
                ./ (bemMesh.endParameter(index) - bemMesh.startParameter(index));
        end
        
    end
    
end

