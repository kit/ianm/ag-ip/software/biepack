function laplaceDirectFormulation( curve, N )
%
% function laplaceDirectFormulation( curve, N )
%
% This function demonstrates the Green representation theorem for the
% harmonic function f(x_1,x_2) = x_1^2 - x_2^2
%
% The parameter curve is a closed BIEPack.geom.Curve object. For points x 
% on this curve C, the function compares the value of the integral
%
%  S (\partial f / \partial n) (x) - D f (x) 
%
% with 1/2 f(x), where S is the single layer and D the double layer boundary 
% operator associated with the Laplace operator. The integral operators are
% discretized on a mesh with the number of points specified by N and the
% equation is colloacted in these meshpoints.
%
% The function prints the maximum norm of the difference between the two
% values.
%

% import the BIEPack package
import BIEPack.*;

% construct a mesh on the curve
aMesh = meshes.ParamCurveMesh(curve,N);

% construct a space of trigonometric polynomials
%trigs = spaces.TrigPol( aMesh );
spaceLin = spaces.BemSpace( aMesh, spaces.BemSpace.PIECEWISE_LINEAR );
spaceCon = spaces.BemSpace( aMesh, spaces.BemSpace.PIECEWISE_CONST );
spaceTrig = spaces.TrigPol( aMesh );

% define the operators
I = ops.IdentityOp( spaceLin );
S = ops.LaplaceSLOp( spaceCon, spaceLin );
D = ops.LaplaceDLOp( spaceLin, spaceLin );
S_trig = ops.LaplaceSLOp( spaceTrig, spaceTrig );
D_trig = ops.LaplaceDLOp( spaceTrig, spaceTrig );

% alpha = 2/3;
% 
% [theta1, rho1] = cart2pol( spaceLin.collocPoints.X, spaceLin.collocPoints.Y );
% u = rho1.^alpha .* cos( alpha * theta1);
% 
% [theta2, rho2] = cart2pol( spaceCon.collocPoints.X, spaceCon.collocPoints.Y );
% grad_u_1 = ( (alpha - 1) * cos(alpha*theta2) .* cos(theta2) + alpha * sin(alpha*theta2) .* sin(theta2) ) .* rho2.^(alpha-1);
% grad_u_2 = ( (alpha - 1) * cos(alpha*theta2) .* sin(theta2) - alpha * sin(alpha*theta2) .* cos(theta2) ) .* rho2.^(alpha-1);
% du_dn = spaceCon.collocPoints.nX .* grad_u_1 + spaceCon.collocPoints.nY .* grad_u_2;

du_dn = 2 * ( spaceCon.collocPoints.nX .* spaceCon.collocPoints.X ...
    - spaceCon.collocPoints.nY .* spaceCon.collocPoints.Y );

u_trig = ( aMesh.nodesX.^2 - aMesh.nodesY.^2 ).';
[nX,nY] = aMesh.theCurve.normal(aMesh.t);
nnorm = sqrt( nX.^2 + nY.^2 );
du_dn_trig = ( 2 * ( nX .* aMesh.nodesX - nY .* aMesh.nodesY ) ./ nnorm ).';

% figure
% plot(theta1,u,'x', theta2,du_dn,'o');
% 
% figure
% plot( spaceLin.collocPoints.X, spaceLin.collocPoints.Y,'x', spaceCon.collocPoints.X, spaceCon.collocPoints.Y,'o' );

% Solve IE of second order
method = methods.Collocation;
D_discrete = D.getCollocationImplementation( method.quadRules );
rhs = u_trig/2 + D_discrete * u_trig;
method.setOperators(S);
method.setRhs(rhs);
du_dn_appr = method.apply;

% op = I + 2 * D;
% S_discrete = S.getCollocationImplementation( method.quadRules );
% D_discrete = D.getCollocationImplementation( method.quadRules );

%semilogy( aMesh.t, abs( u_trig/2 + D_discrete * u_trig - S_discrete * du_dn ) );

% op_t = 2 * D_trig;
% S_disc_t = S_trig.getPeriodicLogImplementation;
% D_disc_t = D_trig.getPeriodicSmoothImplementation;
% rhs_t = 2 * S_disc_t * du_dn_trig;
% 
% plot( aMesh.t, u_trig/2 + D_disc_t * u_trig, aMesh.t, S_disc_t * du_dn_trig );

% 
%
figure
h_over_2 = (aMesh.t(2) - aMesh.t(1)) / 2;
%plot( aMesh.t, S_disc_c * du_dn, aMesh.t , S_disc_t * du_dn_trig );
plot( aMesh.t + h_over_2, du_dn_appr, aMesh.t + h_over_2, du_dn );

% 
% figure
% plot( spaceLin.theMesh.t, rhs );



%residual = S_discrete * dfdn - D_discrete * f - 0.5* f;
%
%fprintf('Max-Norm of the resiudal is %10.6e.\n', norm(residual,Inf) );

end

