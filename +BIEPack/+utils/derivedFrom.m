function yesno = derivedFrom( obj, classname )
% function yesno = derivedFrom( obj, classname )
%
% Check whether the Object obj is of a class that is derived from the class
% classname

metainfo = metaclass(obj);
yesno = derivedFromMeta( metainfo, classname );

end


function yesno = derivedFromMeta( metainfo, classname )
%
% Check whether the class described by the meta.class object metainfo is
% derived from a class called classname

if ( strcmp(metainfo.Name, classname) )
    yesno = true;
else
    yesno = false;
    j = 1;
    
    while ( j <= length(metainfo.SuperclassList) &&  ~yesno )
    
        yesno = derivedFromMeta( metainfo.SuperclassList(j), classname );
        j = j + 1;
        
    end
    
end

end
   
