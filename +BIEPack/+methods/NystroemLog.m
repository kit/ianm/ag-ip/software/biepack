%> @file NystroemLog.m
%> @brief File containing the BIEPack.methods.NystroemLog class.
% ======================================================================
%> @brief Class representing a Nyström method for logarithmic kernels based
%> on a product quadrature rule as described in Atkinson (1997), section 4.2..
%
%> The method can be applied to an integral equation of the second kind with 
%> an integral operator with a logarithmic kernel. The integral is replaced
%> by a product quadrature rule. The quadrature points need to be placed
%> uniformly in parameter space.
%>
%> On construction, the operator @a A and the right hand side @f$\psi@f$ in 
%> the equation
%> @f[
%>     ( I - A ) \, \varphi = \psi
%> @f]
%> have to be provided. 
%>
%> The operator is represented as an instance of LogKernelOp which
%> requires it to have a logarithmic kernel and to map a BemSpace onto
%> itself. The space should also be based on a @link BIEPack.meshes.ParamCurveMesh 
%> ParamCurveMesh @endlink with nodes distributed uniformly in parameter
%> space.
%>
%> The method uses only nodal values of the involved functions. The
%> BemSpace is used just for convencience. On application a column vector
%> of values of @f$ \psi @f$ in the nodes needs to be provided.
%>
%> Likewise, application of the method returns the solution as a column
%> vector of function values in the nodes.
% ======================================================================
classdef NystroemLog < BIEPack.BIEPackObject
    
    properties ( Constant )
        
        %> @brief name of the method used in requirement checking
        METHOD_NAME = 'NystroemLog';
        
    end
    
    
    properties (SetAccess = protected)
        
        %> @brief integral operator with a logarithmic kernel
        op = [];
        
        %> @brief matrix of the linear system
        A = [];
        
        %> @brief vector for the right hand side
        b = [];
        
    end
    
    methods
        
        %> @brief Construct the method using an integral operator and
        %> a right hand side.        
        %>
        %> @param op an operator suitable for application of the Nyström
        %>       method (instance of @link BIEPack.ops.LogKernelOp
        %>       LogKernelOp @endlink)
        %> @param rhs vector of nodal values in @c op.defSpace.theMesh
        %> 
        function nystroem = NystroemLog( op, rhs )
                        
            if ( nargin > 0 )
                nystroem.setOperator(op);
                nystroem.setRhs(rhs);                
            end
            
        end
        
        
        %> @brief Construct the method using an integral operator and
        %> a right hand side.
        %>
        %> The method checks that the integral operator meets the
        %> requirements of the Nystroem method.
        %>
        %> @param op the integral operator, instance of 
        %>    @link BIEPack.ops.LogKernelOp LogKernelOp  @endlink.               
        function setOperator( nystroemMeth, op )
                                    
            if ( ~ op.checkRequirements( nystroemMeth.METHOD_NAME ) )
                error('In NystroemLog method setOperators: operator does not support Nyström method.');
            end
            
            nystroemMeth.op = op;
            
            % set new state for object
            nystroemMeth.A = [];
            nystroemMeth.b = [];
            
        end
        
        
        %> @brief Sets the right hand side vector for the problem.
        %>
        %> This method should be called after setOperator.
        %>
        %> @todo The method should check that rhs has the right dimentsion
        function setRhs( nystroemMeth, rhs )
            
            nystroemMeth.b = rhs;
            
        end
        
        
        %> @brief Assembles the linear system matrix for the given problem         
        function assembleMatrix( nystroemMeth )
            
            nystroemMeth.A = nystroemMeth.op.getImplementation(nystroemMeth.METHOD_NAME);
            
        end                

        
        %> @brief apply the method to the given operator and right hand side
        %>
        %> @retval the vector of nodal values of the solution
        function phi = apply( nystroemMeth )
            
            if ( isempty( nystroemMeth.op ) )
                error('In NystroemLog method apply(): operator matrix is unset. ');
            elseif ( isempty( nystroemMeth.b ) )
                error('In NystroemLog method apply(): right hand side vector is unset. ');
            end
            
            if ( isempty(nystroemMeth.A) )
                nystroemMeth.assembleMatrix;
            end
            
            N = nystroemMeth.op.defSpace.dimension;
            phi = (eye(N) - nystroemMeth.A) \ nystroemMeth.b;
            
        end
        
        
    end
end
