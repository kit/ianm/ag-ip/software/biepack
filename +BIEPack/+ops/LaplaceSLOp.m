%> @file LaplaceSLOp.m
%> @brief File containing the BIEPack.ops.LaplaceSLOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The single layer operator for Laplace's equation. 
%
%> This class implements discretizations of the operator
%>
%> S phi (x) = 1/(2pi) \int_C log ( 1/|x - y| ) phi(y) ds(y)
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself.
% ======================================================================
classdef LaplaceSLOp < BIEPack.ops.PeriodicLogOp & BIEPack.ops.BemLogOp
    % BIEPack.ops.LaplaceSLOp < BIEPack.ops.PeriodicLogOp
    
    properties
    end
    
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with thsi type of operator are
        %> carried out by the checkRequirements function
        function slOp = LaplaceSLOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            slOp@BIEPack.ops.PeriodicLogOp(defSpace,rangeSpace);
            
            slOp.randomName;
            slOp.name = strcat('LaplaceSLOp_',slOp.name);
            
        end
        
        %> @brief Implementation of PeriodicLogOp.evalPeriodicLogKernels
        %
        %> Evaluation of the functions defining the kernel. Implementation
        %> of the function inherited from BIEPack.ops.PeriodicLogOp                
        function [K1, K2] = evalPeriodicLogKernels(laplaceSLOp)
                        
            curve = laplaceSLOp.defSpace.theCurve;
            
            t = laplaceSLOp.defSpace.theMesh.t;
            xi = laplaceSLOp.defSpace.theMesh.nodesX;
            eta = laplaceSLOp.defSpace.theMesh.nodesY;
            
            [xip, etap] = curve.eval(t,1);
            % scale for parametrization over [0,2*pi]
            xip = laplaceSLOp.defSpace.L/(2*pi) * xip;
            etap = laplaceSLOp.defSpace.L/(2*pi) * etap;
            
            N = length(t);
            ones_h = ones(1,N);
            ones_v = ones(N,1);
            
            norm_xp = sqrt( xip.^2 + etap.^2 );
            K1 = -1/2 * ones_v * norm_xp;

            X1 = xi.' * ones_h - ones_v * xi;
            X2 = eta.' * ones_h - ones_v * eta;
            R = sqrt( X1.^2 + X2.^2 );
            
            r = t.' * ones_h - ones_v * t;
            
            K2 = 1/(2*pi) * log( 1 ./ R ) .* ( ones_v * norm_xp ) ...
                - 1/(2*pi) * log( 4 * sin( r/2 ).^2 ) .* K1;
            K2(1:N+1:end) = -norm_xp/(2*pi) .* log( norm_xp);
            
        end
        
        %> @brief Implementation of BIEPack.ops.BemLogOp.evalBemLogKFunction()
        function K = evalBemLogKFunction( bemLogOp, xpoints, index, Y1, Y2 ) %#ok<INUSD,INUSL>
           
            K = -1/(2*pi) * ones( length(xpoints.X(index)), length(Y1) );
            
        end
        
        %> @brief  Implementaton of GeneralOp.checkRequirements().
        %
        %> Check whether requirements for application of a numerical
        %> method are satisfied
        %>
        %> @todo Check requirements for collocation method.
        function isOk = checkRequirements( laplaceSLOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = laplaceSLOp.checkRequirementsForPeriodicLogOp;
            elseif ( strcmp( methodName, BIEPack.methods.Collocation.METHOD_NAME ) )
                isOk = true;
            else                
                isOk = false;
                % warning('In LaplaceSLOp method checkRequirements: requested type %s is not supported.', methodName );
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( laplaceSLOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = laplaceSLOp.getPeriodicLogImplementation();
            elseif ( strcmp( methodName, BIEPack.methods.Collocation.METHOD_NAME ) )
                A = laplaceSLOp.getCollocationImplementation( varargin{:} );
            else
                error('In LaplaceSLOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
    end
    
end

