%> @file CurveUnion.m
%> @brief Contains the geom.CurveUnion class.
% ======================================================================
%> @brief A union of curve segments with no described geometrical relation.
%>
%> A CurveUnion represents a curve made up of a number of seperate curve
%> segments. No knowledge is available in this class on connectivity of
%> the individual segments, this is up to specific derived classes.
%
classdef CurveUnion < BIEPack.geom.Curve
    
    properties (SetAccess = protected)
        
        %> @brief Cell array that contains the seperate segments of the
        %> CurveUnion, each a Curve object.
        segments;
        
    end

    
    methods(Access = protected)
        
        %> @brief Implementation of the copyElement function to create deep
        %> copies of the segements of the CurveUnion.
        % The segments forming the CurveUnion must be truly copied. Override 
        % copyElement method, as shown in MATLAB documentation:
        function cpObj = copyElement(obj)
            
            % Make a shallow copy of all properties
            cpObj = copyElement@matlab.mixin.Copyable(obj);
            
            % Make a deep copy of the segments
            for j=1:length( obj.segments )
                cpObj.segments{j} = copy( obj.segments{j} );
            end
            
        end
        
   end

    
    methods

        %> @brief Constructs a CurveUnion from a list of curves. The curves 
        %> must each be derived from the Curve class.
        %>
        %> @param varargin List  (curve1, curve2, ...)  of the curves.
        %>
        %> @retval The newly construted CurvenUnion object
        function curveUnion = CurveUnion(varargin)
            
            % initialize the segments property
            curveUnion.segments = cell(nargin,1);
            
            % Assign each component, checking whether it is the correct
            % class.
            for j=1:nargin
                
                if ( ~ BIEPack.utils.derivedFrom( varargin{j}, 'BIEPack.geom.Curve' ) )
                    error( 'In CurveUnion constructor: argument %d is not a BIEPack.geom.Curve', j );
                end
                
                curveUnion.segments{j} = varargin{j};
                
            end
            
        end
        
        %> @brief Returns a list of all @link BIEPack.geom.ParamCurve ParamCurve @endlink
        %> objects that form this CurveUnion.
        function listOfCurves = getParamCurveList(curve)
            
            listOfCurves = [];
            for j=1:length(curve.segments)
                listOfCurves = [ listOfCurves curve.segments{j}.getParamCurveList ]; %#ok<AGROW>
            end
            
        end
        
        
        %> @brief Implement translation by applying it to all segments in the CurveUnion.
        function h = translate(curve,d)
            
            for j=1:length(curve.segments)
                curve.segments{j}.translate(d);
            end
            
            h = curve;
            
        end
        
        %> @brief Implement rotation by applying it to all segments in the CurveUnion.
        function h = rotate(curve, phi, p)
      
            if ( nargin == 2 )                
                for j=1:length(curve.segments)
                    curve.segments{j}.rotate(phi);
                end
            else
                for j=1:length(curve.segments)
                    curve.segments{j}.rotate(phi,p);
                end
            end
            
            h = curve;
        end
        
        %> @brief Implement reflection by applying it to all segments in the CurveUnion.
        function h = reflect(curve, arg1, arg2)
            
            for j=1:length(curve.segments)
                curve.segments{j}.reflect(arg1,arg2);
            end
            
            h = curve;
        end
        
        
        %> @brief Implements plot by plotting each segment, respectively.
        function plot(curve, n)
            
            holdwason = ishold;
            
            for j=1:length(curve.segments)
                
                curve.segments{j}.plot(n);
                hold on
                
            end
            
            if ( ~ holdwason )
                hold off
            end
            
        end
                
    end    
    
end
