function [x, w] = gaussLegendreQuad(N,a,b)
%
% function [x, w] = gaussLegendreQuad(N,a,b)
%
% Punkte und Gewichte für die Gauss-Legendre-Quadratur mit N+1
% Punkten auf dem Intervall [a,b].
%

j = 1:N;
alpha_j = zeros(1,N+1);
beta_j = j ./ sqrt( (2*j-1) .* (2*j+1) );

% Berechnung der Gauss-Legendre-Formel für das Intervall [-1,1]
[ x, w ] = BIEPack.utils.gaussPointsWeights( alpha_j, beta_j, 2 );

% Skalierung auf [a,b]
x = a + (b-a)/2*(1+x);
w = (b-a)/2*w;
