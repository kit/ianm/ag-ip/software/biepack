%> @file quadCompGaussLegendre.m
%> @brief The quadCompGaussLegendre() function.
% ======================================================================
%> @brief Computation of the points and weights of a composite Gauss-Legendre
%> quadrature rule for a general interval [a,b]
%>
%> The function takes arguments @a N, @a a, @a b, @a M where @a a, @a b are the
%> interval end points. The interval is divided into @a N subintervals and for
%> each subinterval an @a M + 1 point Gauss-Legendre quadrature formula is
%> computed. Thus, the total number of quadrature points in <i>N (M + 1)</i>
% ======================================================================
function [x,w] = quadCompGaussLegendre(N,a,b,M)

% Length of a subinterval
h = (b-a) / N;

% M+1 point Gauss-Legendre rule for the interval  (0,1)
[x_M, w_M] = BIEPack.utils.quadGaussLegendre(M,0,1);

x = zeros(1,N*(M+1));
w = zeros(1,N*(M+1));

% put together composite rule
for j=0:N-1
    index = ( j*(M+1) + 1 ):( (j+1)*(M+1) );
    x(index) = a + (j + x_M)*h;
    w(index) = h*w_M;
end