%> @file Kite.m
%> @brief Contains the geom.Kite class.
% ======================================================================
%> @brief The Colton/Kress kite (Section 3.5 of the Inv.Scat.Theo. Book)
classdef Kite < BIEPack.geom.ParamCurve
    % BIEPack.geom.Kite < BIEPack.geom.ParamCurve
    
    properties
        
    end
    
    methods
        
        %> @brief Constructs a Colton/Kress kite. 
        %> A random name starting with 'Kite_' is also set.
        %>
        %> @retval kite The newly constructed Kite object.
        function kite = Kite
            
            kite.randomName;
            kite.name = strcat('Kite_',kite.name);
            
            kite.startParam = -pi;
            kite.endParam = pi;
            
            kite.closed = true;
            kite.smoothness = inf;
            kite.globalSmoothness = inf;
            kite.highestImplementedDerivative = 2;
        end
        
        %> @brief Implementation of basic parametrization (−0.65 + cos(t) + 
        %> 0.65 * cos(2*t), 1.5 * sin(t)) over the interval [-pi,pi]
        %>
        %> Derivatives of the parametrization up to second order are
        %> implemented.
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. 
        %> @retval y2 2nd coordinate of the curve points.
        function [y1, y2] = paramEval(~, t, n)
            
            if ( n == 0 )
                y1 = -0.65 + cos(t) + 0.65*cos(2*t);
                y2 = 1.5 * sin(t);
            elseif ( n == 1 )
                y1 = -sin(t) - 1.3*sin(2*t);
                y2 = 1.5 * cos(t);
            else
                y1 = -cos(t) - 2.6*cos(2*t);
                y2 = -1.5 * sin(t);
            end
            
        end
        
    end
    
end

