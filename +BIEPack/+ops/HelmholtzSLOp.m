%> @file HelmholtzSLOp.m
%> @brief File containing the BIEPack.ops.HelmholtzSLOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief The single layer operator for the Helmholtz equation. 
%
%> This class implements discretizations of the operator
%>
%> S phi (x) = \int_C i/4 H^{(1)}_0 ( k |x - y| ) phi(y) ds(y)
%>
%> Implementations include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) on itself.
% ======================================================================
classdef HelmholtzSLOp < BIEPack.ops.PeriodicLogOp
    % BIEPack.ops.HelmholtzSLOp < BIEPack.ops.PeriodicLogOp
    
    properties ( Constant )
        
        %> Euler's constant appearing in expressions of the kernel
        eulerC = 0.5772156649;
        
    end
    
    properties
        
        %> the wave number
        waveNum = 1;
        
    end
    
    methods 
        
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with thsi type of operator are
        %> carried out by the checkRequirements function
        function slOp = HelmholtzSLOp( defSpace, rangeSpace, wavenum )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            slOp@BIEPack.ops.PeriodicLogOp(defSpace,rangeSpace);
            
            if ( ~ isempty(wavenum) )
                slOp.waveNum = wavenum;
            end
            
            slOp.randomName;
            slOp.name = strcat('HelmholtzSLOp_',slOp.name);
            
        end
        
        %> @brief Implementation of PeriodicLogOp.evalPeriodicLogKernels
        %
        %> Evaluation of the functions defining the kernel. Implementation
        %> of the function inherited from BIEPack.ops.PeriodicLogOp                
        function [K1, K2] = evalPeriodicLogKernels(helmholtzSLOp)
                        
            curve = helmholtzSLOp.defSpace.theCurve;
            
            t = helmholtzSLOp.defSpace.theMesh.t;
            xi = helmholtzSLOp.defSpace.theMesh.nodesX;
            eta = helmholtzSLOp.defSpace.theMesh.nodesY;
            
            [xip, etap] = curve.eval(t,1);
            % scale for parametrization over [0,2*pi]
            xip = helmholtzSLOp.defSpace.L/(2*pi) * xip;
            etap = helmholtzSLOp.defSpace.L/(2*pi) * etap;
            
            N = length(t);
            ones_h = ones(1,N);
            ones_v = ones(N,1);
            
            norm_xp = sqrt( xip.^2 + etap.^2 );
            norm_xp_matrix = ones_v * norm_xp;

            X1 = xi.' * ones_h - ones_v * xi;
            X2 = eta.' * ones_h - ones_v * eta;
            kR = helmholtzSLOp.waveNum * sqrt( X1.^2 + X2.^2 );
                        
            K1 = -0.5 * besselj(0,kR) .* norm_xp_matrix;
            
            r = t.' * ones_h - ones_v * t;
            
            K2 = 1i/4 * besselh( 0,1, kR ) .* norm_xp_matrix ...
                - 1/(2*pi) * log( 4 * sin( r/2 ).^2 ) .* K1;
            K2(1:N+1:end) = ( 1i/4 - helmholtzSLOp.eulerC / (2*pi) ...
                - 1/(2*pi) * log( helmholtzSLOp.waveNum/2 * norm_xp ) ) .* norm_xp;
            
        end
        
        %> @brief  Implementaton of GeneralOp.checkRequirements().
        %
        %> Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements( helmholtzSLOp, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = helmholtzSLOp.checkRequirementsForPeriodicLogOp;
            else                
                isOk = false;
                % warning('In HelmholtzSLOp method checkRequirements: requested type %s is not supported.', methodName );
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( helmholtzSLOp, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = helmholtzSLOp.getPeriodicLogImplementation();
            else
                error('In HelmholtzSLOp method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
        
    end
    
end

