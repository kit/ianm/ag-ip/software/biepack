%> @file OneCorner.m
%> @brief Contains the geom.OneCorner class.
% ======================================================================
%> @ingroup GeomGroup 
%> @brief Wedge with a corner with a specified angle. Away from the wedge
%> corner, the boundary is smooth.
%
%> The parameters for this type of curve are the opening angle alpha of the wedge
%> and a radius R. The curve leaves the wedge corner (0,0) with a straight line of
%> direction (cos alpha/2, - sin alpha/2), then turns onto a circle of radius
%> R and eventually turns back to a straight line of direction ( - cos alpha/2, 
%> - sin alpha/2) to end at the wedge corner. The parameter interval is
%> [- (alpha+1)/2, (alpha+1)/2 ]. Except for the corner point, the curve is
%> smooth.
%>
%> The parametrization is obtained by adding three seperate
%> parametrizations with a partition of unity.
% ======================================================================
classdef OneCorner < BIEPack.geom.ParamCurve
    % BIEPack.geom.Circle < BIEPack.geom.ParamCurve
    
    properties
        
        %> interior angle at the wedge corner
        alpha = pi/2;
        
        %> radius of the circular curve section
        R = 1;
   
        %> length of the interval where only straight line segments are
        %> used
        L1 = 0.75;
        
        %> length of the intervals where two parametrizations overlap
        L2 = 0.5;
        
    end
    
    methods
        
        function oC = OneCorner( alpha, R )
            
            oC.randomName;
            oC.name = strcat('OneCorner_',oC.name);
            oC.alpha = alpha;
            oC.R = R;
            
            oC.startParam = -(alpha+1)/2 ;
            oC.endParam = (alpha+1)/2;
            oC.L2 = 0.25 + min([0.1, alpha/6]);
            
            oC.closed = true;
            oC.smoothness = inf;
            oC.globalSmoothness = 0;
            oC.highestImplementedDerivative = 2;
        end
        
        % Implementation of basic parametrization
        function [y1, y2] = paramEval(oC, t, n)

            chi = oC.partOfUnity(t);
            
            if ( n == 0 )
                y1 = chi{1,1} .* (t - oC.startParam) * cos(oC.alpha/2) ...
                    + chi{1,2} .* ( oC.R * cos(t) ) ...
                    + chi{1,3} .* (oC.endParam - t) * cos(oC.alpha/2);
                y2 = - chi{1,1} .* (t - oC.startParam) * sin(oC.alpha/2) ...
                    + chi{1,2} .* ( oC.R * sin(t) ) ...
                    + chi{1,3} .* (oC.endParam - t) * sin(oC.alpha/2);
            elseif ( n == 1 )
                y1 = chi{2,1} .* (t - oC.startParam) * cos(oC.alpha/2) ...
                    + chi{1,1} * cos(oC.alpha/2) ...
                    + chi{2,2} .* ( oC.R * cos(t) ) ...
                    + chi{1,2} .* ( -oC.R * sin(t) ) ...
                    + chi{2,3} .* (oC.endParam - t) * cos(oC.alpha/2) ...
                    + chi{1,3} * ( - cos(oC.alpha/2) );
                y2 = - chi{2,1} .* (t - oC.startParam) * sin(oC.alpha/2) ...
                    - chi{1,1} * sin( oC.alpha/2 ) ...
                    + chi{2,2} .* ( oC.R * sin(t) ) ...
                    + chi{1,2} .* ( oC.R * cos(t) ) ...
                    + chi{2,3} .* (oC.endParam - t) * sin(oC.alpha/2) ...
                    - chi{1,3} * sin(oC.alpha/2);
            else
                y1 = chi{3,1} .* (t - oC.startParam) * cos(oC.alpha/2) ...
                    + 2* chi{2,1} * cos(oC.alpha/2) ...
                    + chi{3,2} .* ( oC.R * cos(t) ) ...
                    + 2*chi{2,2} .* ( -oC.R * sin(t) ) ...
                    + chi{1,2} .* ( -oC.R * cos(t) ) ...
                    + chi{3,3} .* (oC.endParam - t) * cos(oC.alpha/2) ...
                    + 2*chi{1,3} * ( - cos(oC.alpha/2) );
                y2 = - chi{3,1} .* (t - oC.startParam) * sin(oC.alpha/2) ...
                    - 2*chi{2,1} * sin( oC.alpha/2 ) ...
                    + chi{3,2} .* ( oC.R * sin(t) ) ...
                    + 2*chi{2,2} .* ( oC.R * cos(t) ) ...
                    + chi{1,2} .* ( -oC.R * sin(t) ) ...
                    + chi{3,3} .* (oC.endParam - t) * sin(oC.alpha/2) ...
                    - 2*chi{2,3} * sin(oC.alpha/2);
            end
            
        end
        
        %> @brief Implementation of partition of unity functions
        %>
        %> The function returns a 3x3 cell array of function value arrays.
        %> The first row contains the values chi(t) for the three partition
        %> of unity functions, the second row the derivatives and the third
        %> row the second derivatives.
        function chi = partOfUnity(oC, t)
           
            chi = cell(3,3);
            
            index1 = t < oC.startParam + oC.L1;
            index2 = ( t >= oC.startParam + oC.L1 ) & ( t < oC.startParam + oC.L1 + oC.L2 );
            index3 = ( t >= oC.startParam + oC.L1 + oC.L2 ) & ( t < oC.endParam - oC.L1 - oC.L2 );
            index4 = ( t >= oC.endParam - oC.L1 - oC.L2 ) & ( t < oC.endParam - oC.L1 );
            index5 = t >= oC.endParam - oC.L1;
            
            for j=1:3
                for k=1:3
                    chi{j,k} = zeros(size(t));
                end
            end
            
            chi{1,1}(index1) = 1;            
            
            [sc, dsc, d2sc] = oC.smoothChi( ( t(index2) - oC.startParam - oC.L1 ) / oC.L2 );
            
            chi{1,1}(index2) = 1 - sc;
            chi{1,2}(index2) = sc;
            chi{2,1}(index2) = -dsc / oC.L2;
            chi{2,2}(index2) = dsc / oC.L2;
            chi{3,1}(index2) = -d2sc / oC.L2^2;
            chi{3,2}(index2) = d2sc / oC.L2^2;
            
            chi{1,2}(index3) = 1;
            
            [sc, dsc, d2sc] = oC.smoothChi( ( t(index4) - oC.endParam + oC.L1 + oC.L2 ) / oC.L2 );
            
            chi{1,2}(index4) = 1 - sc;
            chi{1,3}(index4) = sc;
            chi{2,2}(index4) = -dsc / oC.L2;
            chi{2,3}(index4) = dsc / oC.L2;
            chi{3,2}(index4) = -d2sc / oC.L2^2;
            chi{3,3}(index4) = d2sc / oC.L2^2;
            
            chi{1,3}(index5) = 1;
            
        end
        
        %> Strightly monotonic infinitely smooth function from [0,1] onto [0,1]
        %> with infinite number of derivatives vanishing at both end
        %> points.
        function [chi, d_chi, d2chi] = smoothChi( oC, t ) %#ok<INUSL>
            
            z1 = exp( -1.0 ./ t );
            dz1 = exp( -1.0 ./ t ) ./ t.^2;
            d2z1 = exp( -1.0 ./ t) .* ( 1./ t.^4 - 2 ./ t.^3 );
            z2 = exp( -1.0 ./ (1.0 - t) );
            dz2 = - exp( -1.0 ./ (1.0 - t) ) ./ (1.0 - t).^2;
            d2z2 = exp( -1.0 ./ (1.0 - t) ) .* ( 1.0 ./ (1.0 - t).^4 - 2.0 ./ (1.0 - t).^3 );

            chi = z1 ./ ( z1 + z2 );
            d_chi = ( dz1 .* z2 - z1 .* dz2 ) ./ (z1 + z2).^2;
            d2chi = ( ( d2z1 .* z2 - z1 .* d2z2) .* (z1 + z2) ...
                - 2 * ( dz1 .* z2 - z1 .* dz2 ) .* (dz1 + dz2) ) ...
                ./ ( z1 + z2 ).^3;
            
        end
        
    end
    
end

