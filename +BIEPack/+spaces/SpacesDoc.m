%> @file SpacesDoc.m
%> @brief The documentation of the @a spaces package
%
% ======================================================================
%
%> @namespace BIEPack::spaces
%> @brief Classes representing discrete spaces.
%>
%> Meshes are used to define discrete function spaces that can be used in
%> numerical methods. The classes in the package @link BIEPack::spaces spaces @endlink define the way 
%> in which degrees of freedom are associated with mesh properties. 
%>
%> The class @link BIEPack.space.SpaceOnCurve SpaceOnCurve @endlink acts as
%> an abstract umbrella class and contains common properties such as the
%> dimension and the mesh the space is based on.
%>
%> Concrete implementations are provided by two types of spaces,
%> @link BIEPack.spaces.BemSpace BemSpace @endlink and @link
%> BIEPack.spaces.Trigpol Trigpol @endlink. In a @link BIEPack.spaces.BemSpace 
%> BemSpace @endlink, the curve is divided in to segments called elements
%> and on each element, local degrees of freedom are defined. These in turn
%> are mapped to global degrees of freedom depending on the type of functions
%> used (piecewice continuous, piecewise linear,... ). An instance of @link
%> BIEPack.spaces.TrigPol TrigPol @endlink is a discrete space of
%> trigonometric polynomials defined on the parameter interval of a single
%> parametrized curve.
%
% ======================================================================
