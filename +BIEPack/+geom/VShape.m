%> @file VShape.m
%> @brief Contains the geom.VShape class.
% ======================================================================
%> @brief A V shaped obstacle with one reentrant corner, as given in 
%> Kress, Num. Math. 58, 1990
%>
%> The obstacle has a single corner in the origin which has an interior
%> angle of @f$ 3 \pi / 2 @f$ between the tangents
classdef VShape < BIEPack.geom.ParamCurve 
    
    methods
        
        %> @brief Constructs a Kress V shaped obstacle. 
        %> A random name starting with 'VShape_' is also set.
        %>
        %> @retval vshape The newly constructed VShape object.
        function vshape = VShape()
            
            vshape.randomName;
            vshape.name = strcat('VShape_',vshape.name);
            
            vshape.startParam = 0;
            vshape.endParam = 2*pi;
            
            vshape.closed = true;
            vshape.smoothness = inf;
            vshape.globalSmoothness = 0;
            vshape.highestImplementedDerivative = 2;
        end
        
        %> @brief Implementation of basic parametrization 
        %>
        %> The parametrization of the V shaped region is
        %> @f[
        %>   \gamma(t) = \left( \frac{3}{2} \, \sin\left(\frac{3t}{2} \right)) , - \sin( t ) \right)^\top, \qquad t \in [0,2*\pi]
        %> @f]
        %>
        %> Derivatives of the parametrization up to second order are
        %> implemented.
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. 
        %> @retval y2 2nd coordinate of the curve points.
        function [y1, y2] = paramEval(~, t, n)
            
            if ( n == 0 )
                y1 = -2/3 * sin( 1.5*t ); 
                y2 = -sin(t);
            elseif ( n == 1 )
                y1 = -cos( 1.5*t ); 
                y2 = -cos(t);
            else
                y1 = 1.5 * sin( 1.5*t ); 
                y2 = sin(t);
            end
            
        end
        
    end
    
end

