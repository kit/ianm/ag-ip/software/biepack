# BIEPack

The BIEPack (Boundary Integral Equations Package) is a collection of classes and functions written in the
Matlab programming language to solve 2D boundary value problems via boundary integral equations. 

The package was developed primarily with the intention of being used for demonstration purposes in class. 
Thus emphasis has been laid on the following features:
- programs should use or be as close to mathematical concepts as possible,
- it should be easy to apply different numerical methods to the same boundary value problem or
  boundary integral equation and to compare results.

The code has also been used for numerical experiments for research papers.


## License

This software is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, either version 3 of the License, 
or (at your option) any later version.

This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public 
License for more details.

You should have received a copy of the GNU General Public License along with this software package, contained
in the file LICENSE. If not, see <https://www.gnu.org/licenses/>. 


## Getting started

To use BIEPack, add the directory containing the +BIEPack directory to your Matlab path.

Some features require additional external packages.
- An HTML documentation can be generated using the [doxygen program](https://www.doxygen.nl/) together with
  the script m2cpp.pl from the [doxymatlab package](https://github.com/simgunz/doxymatlab).
- The package includes code to evaluate periodic Green's functions which
  requires the Faddeeva package by Simon Chandler-Wilde and Mohammad Al
  Azah. This package is availble from Matlab File Exchange or from 
  https://github.com/sms03snc/Faddeeva

Except for the documentation generation, optional features are made
available in the following way:
- copy the file biepack_local_settings.m.dist to biepack_local_settings.m,
- download and install the code for the optional feature,
- edit biepack_local_settings.m in the appropriate place to make the feature available.


