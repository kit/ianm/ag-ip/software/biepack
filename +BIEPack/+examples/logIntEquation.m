%> @file logIntEquation.m
%> @brief Contains the logIntEquation() example function.
%
% ======================================================================
%
%> @brief Example solving an integral equation with a logarithmic kernel.
function [phi, phi_exact, t] = logIntEquation(N)

if ( nargin == 0 )
    N = 32;
end

% import the BIEPack package
import BIEPack.*;

% construct a mesh on the interval [0,1] with uniformly distributed nodes.
curve = geom.StraightLine([0;0], [1;0]);
mesh = meshes.ParamCurveMesh(curve,N);

% construct a space
bemSpace = spaces.BemSpace( mesh, spaces.BemSpace.PIECEWISE_LINEAR );

% define logarithmic kernel operator with the function kernelFactor()
% implemented below
theOp = ops.ConcreteLogKernelOp( bemSpace, @kernelFactor, 0 );

% evaluate the right hand side function
t = mesh.t.';
psi = 9/8*t.^3 + t.^2/16 + t/24 + 1/32;
psi(2:N) = psi(2:N) - t(2:N).^4/8.*log(t(2:N)) - (1-t(2:N).^4)/8.*log(1-t(2:N));

% define the method
nystroemMethod = BIEPack.methods.NystroemLog(theOp,psi);

% compute the approximate solution and plot
phi = nystroemMethod.apply;

figure(1);
plot(mesh.t,phi);

% compute exact solution and compare
phi_exact = t.^3;

figure(2);
semilogy(t,abs(phi - phi_exact));

end


function K = kernelFactor( t )

K = 0.5 * ones(length(t),length(t));

end
