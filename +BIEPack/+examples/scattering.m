% Scattering
clear;
close all;
import BIEPack.*;

% Wellenzahl
kappa = 0.5*sqrt(5);
NN = 9;
N = 2.^(1:NN);

% Geometrie & Gitter & Räume
kite = geom.Kite;
x = linspace(-10,10,101);
y = linspace(-10,10,101);
[X,Y] = meshgrid(x,y);
d = [1; -1] / sqrt(2);
PlaneWave = exp(1j * kappa * (d(1) * X + d(2) * Y));
kRe = kappa * sqrt(X.^2 + Y.^2);
PointSource = besselh(0,kRe);
figure(1)
surf(X,Y,real(PlaneWave));
figure(2)
surf(X,Y,real(PointSource));


for i=1:length(N)
    mesh = meshes.ParamCurveMesh(kite, N(i));
    trigPols = spaces.TrigPol(mesh);

    % Operatoren
    SL = ops.HelmholtzSLOp(trigPols, trigPols, kappa);
    DL = ops.HelmholtzDLOp(trigPols, trigPols, kappa);

    % IGL
    Op = -2 * DL + 2i * kappa * SL;

    % u^i
    
    planeWave = exp(1j * kappa * (d(1) * mesh.nodesX + d(2) * mesh.nodesY )).';
    kR = kappa * sqrt(mesh.nodesX.^2 + mesh.nodesY.^2);
    pointSource = besselh(0,kR).';

    % Nyström
    nystroemEins = methods.NystroemPeriodicLog(Op, -2*planeWave);
    nystroemZwei = methods.NystroemPeriodicLog(Op, -2*pointSource);

    % Lösung
    phi = nystroemEins.apply;
    psi = nystroemZwei.apply;

    % Plots

    % Potentiale
    DL = ops.HelmholtzDLPot(trigPols, trigPols, kappa);
    SL = ops.HelmholtzSLPot(trigPols, trigPols, kappa);

    % u = DL \varphi - 1i * k * SL \varphi
    x = linspace(2,3,10);
    y = linspace(2,3,10);   
    usEins = DL.evalPotential(x,y,phi) - 1i * kappa * SL.evalPotential(x,y,phi);
    usZwei = DL.evalPotential(x,y,psi) - 1i * kappa * SL.evalPotential(x,y,psi);


    kR = kappa * sqrt(x.^2 + y.^2);
    Exact = -besselh(0,kR);
    
    rel(i) = norm(usZwei - Exact,2);
end

rel