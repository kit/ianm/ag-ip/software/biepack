function greenRepTheorem( curve, N )
%
% function greenRepTheorem( curve, N )
%
% This function demonstrates the Green representation theorem for the
% harmonic function f(x_1,x_2) = x_1^2 - x_2^2
%
% The parameter curve is a closed BIEPack.geom.Curve object. For points x 
% on this curve C, the function compares the value of the integral
%
%  S (\partial f / \partial n) (x) - D f (x) 
%
% with 1/2 f(x), where S is the single layer and D the double layer boundary 
% operator associated with the Laplace operator. The integral operators are
% discretized on a mesh with the number of points specified by N and the
% equation is colloacted in these meshpoints.
%
% The function prints the maximum norm of the difference between the two
% values.
%

% import the BIEPack package
import BIEPack.*;

% construct a mesh on the curve
aMesh = meshes.ParamCurveMesh(curve,N);

% construct a space of trigonometric polynomials
trigs = spaces.TrigPol( aMesh );

% define the two operators
S = ops.LaplaceSLOp( trigs, trigs );
D = ops.LaplaceDLOp( trigs, trigs );

% obtain discretizations of these operators
S_discrete = S.getPeriodicLogImplementation;
D_discrete = D.getPeriodicSmoothImplementation;

% evaluate the function f and its normal derivative on the curve
f = ( aMesh.nodesX.^2 - aMesh.nodesY.^2 ).';
[xip,etap] = curve.eval(aMesh.t,1);
dfdn = ( 2 * ( etap.*aMesh.nodesX + xip.*aMesh.nodesY ) ./ sqrt( xip.^2 + etap.^2 ) ).';

residual = S_discrete * dfdn - D_discrete * f - 0.5* f;

fprintf('Max-Norm of the resiudal is %10.6e.\n', norm(residual,Inf) );

end

