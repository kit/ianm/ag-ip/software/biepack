%> @file BemLogOp.m
%> @brief Contains the BemLogOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief A boundary integral operator on a BemMesh with a logarithmic
%> singularity.
%>
%> Operator of the form
%> 
%> A phi(x) = \int_C log | x - y | K(x,y) phi(y) ds(y), x \in C.
%>
%> with a smooth function K.
%> 
%> The operator is assumed to be defined on a BIEPack.geom.Curve with a 
%> BIEPack.meshes.BemMesh. Its definition and range spaces are BemSpaces.
%>
%> The mesh is assumed to have elements S_1,...,S_M with local parametrizations 
%> eta_j : [0,1] -> S_j. Then the operator is usually written as
%>
%> A phi(x) = \sum_{j=1}^M \int_0^1 1/(2pi) log | x - eta_j(tau) | 
%>                    K(x,eta_j(tau)) phi(eta_j(tau)) | eta_j'(tau) | d tau
%>
%> @todo Write checkRequirementsForBemLog() function.
% ======================================================================
classdef BemLogOp < BIEPack.ops.GeneralOp
    % BIEPack.ops.BemLogOp < BIEPack.ops.GeneralOp    
    
    methods 
        
        %> @brief Construct the operator
        %
        %> The function just passes on the call to the super class constructor. 
        %> Checks whether the spaces are compatible with a BemLogOp are 
        %> carried out by the checkRequirementsForBemLog() function.        
        function bemLogOp = BemLogOp( defSpace, rangeSpace )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            end
            
            bemLogOp@BIEPack.ops.GeneralOp(defSpace,rangeSpace);
            
        end
        
        %> @brief Evaluate the kernel on an element
        %>
        %> The function log | x - y | K(x,y) for y on the element after 
        %> transformation to the reference interval [0,1] becomes
        %> 
        %> L log |x - y(a+ L*tau)| K(x, y(tau)) |y'(a+L*tau)|
        %>
        %> This function is evaluated for points x contained in the set of
        %> collocation points of the space.
        %>
        %> @param elnum number of the element
        %> @param XIndex indeces of the collocation points x the evaluation is made at
        %> @param tau row vector of parameter values in the reference interval
        %>
        %> @retval a matrix of dimension length(xIndex) x length(tau) of values of the
        %>     kernel function
        function LK = evalKernelOnElement( bemLogOp, elnum, xIndex, tau)
                        
            mesh = bemLogOp.defSpace.theMesh;
            
            % the curve the element belongs to
            curve = mesh.theParamCurves{ mesh.curveIndexOfElement(elnum) };
        
            % compute original parameter values
            elementLength = mesh.endParameter(elnum) - mesh.startParameter(elnum);
            t = mesh.startParameter(elnum) + tau * elementLength;
            
            % evaluate parametriation
            [Y1, Y2] = curve.eval( t, 0 );
            [dY1, dY2] = curve.eval( t, 1 );
            
            K = bemLogOp.evalBemLogKFunction( bemLogOp.rangeSpace.collocPoints, xIndex, Y1, Y2 );
            
            X1diff = bemLogOp.rangeSpace.collocPoints.X(xIndex) * ones(1,length(tau)) - ones(length(xIndex),1) * Y1;
            X2diff = bemLogOp.rangeSpace.collocPoints.Y(xIndex) * ones(1,length(tau)) - ones(length(xIndex),1) * Y2;
            R = sqrt( X1diff.^2 +  X2diff.^2 ); 
            
            % compute the kernel
            LK = log(R) .* K .* ( ones(length(xIndex),1) * ( elementLength * sqrt( dY1.^2 + dY2.^2 ) ) );
            
        end
        
  
        %> @brief Implementation of the collocation method
        %>
        %> @todo How to find collocation points on an element cleanly?
        function A = getCollocationImplementation( bemLogOp, quadRules )
           
            % collocation points are the nodes of the mesh
            mesh = bemLogOp.defSpace.theMesh;
            
            A = zeros( bemLogOp.rangeSpace.dimension, bemLogOp.defSpace.dimension);
            
            for elNum = 1:mesh.numElements
                
                % classify collocation points with respect to the element
                [interior, boundary, away] = bemLogOp.rangeSpace.getCollocPointsForElement(elNum);
                
                % step 1 compute the integrals for collocation points in away. Here the integrand
                % is simply a continuous function.
                kernel = bemLogOp.evalKernelOnElement( elNum, away, quadRules.contPoints);
                
%                 elementLength = mesh.endParameter(elNum) - mesh.startParameter(elNum);
%                 contTau = mesh.startParameter(elNum) + quadRules.contPoints * elementLength;
%                 [quadX, quadY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( contTau, 0 );
%                 [dX, dY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( contTau, 1 );
%                 weights = elementLength * sqrt(dX.^2 + dY.^2) .* quadRules.contWeights;
%                 
%                 K = bemLogOp.evalBemLogKFunction( bemLogOp.rangeSpace.collocPoints, away, ...
%                                                   quadX, quadY ); 
%                                               
%                 Xdiff = bemLogOp.rangeSpace.collocPoints.X(away) * ones(1,length(quadX)) - ones(length(away),1) * quadX;
%                 Ydiff = bemLogOp.rangeSpace.collocPoints.Y(away) * ones(1,length(quadY)) - ones(length(away),1) * quadY;
%                 R = sqrt( Xdiff.^2 +  Ydiff.^2 );
%                                               
%                 % compute the kernel
%                 kernelCont = log(R) .* K .* ( ones(length(away),1) * weights );
                
                % find degrees of freedom for this element
                shapeFuncsCont = bemLogOp.defSpace.evalShapeFunctions( elNum, quadRules.contPoints.' );
                globalDofs = bemLogOp.defSpace.localToGlobalDOFMap( elNum ).globalDOF;
                
                % compute integrals and add to matrix
                A(away,globalDofs) = A(away,globalDofs) + kernel * ( diag(quadRules.contWeights) * shapeFuncsCont );
                
                % evaluate kernel on points and quadrature points for
                % log kernel with collocation point in node
                if (~ isempty(boundary))
                    
                    kernel = bemLogOp.evalKernelOnElement( elNum, boundary, quadRules.singPoints);
                
%                     logTau = mesh.startParameter(elNum) + quadRules.singPoints * elementLength;
%                     [quadX, quadY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( logTau, 0 );
%                     [dX, dY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( logTau, 1 );
%                     weights = elementLength * sqrt(dX.^2 + dY.^2) .* quadRules.singWeights;
% 
%                     K = bemLogOp.evalBemLogKFunction( bemLogOp.rangeSpace.collocPoints, boundary, ...
%                                                       quadX, quadY );
% 
%                     Xdiff = ones(length(boundary),1) * quadX - bemLogOp.rangeSpace.collocPoints.X(boundary) * ones(1,length(quadX));
%                     Ydiff = ones(length(boundary),1) * quadY - bemLogOp.rangeSpace.collocPoints.Y(boundary) * ones(1,length(quadY));
%                     R = sqrt( Xdiff.^2 +  Ydiff.^2 );
% 
%                     % compute the kernel
%                     kernelLog1 = log(R) .* K .* ( ones(length(boundary),1) * weights );                                

                    % find degrees of freedom for this element
                    shapeFuncsLog1 = bemLogOp.defSpace.evalShapeFunctions( elNum, quadRules.singPoints.' );

                    % compute integrals and add to matrix
                    A(boundary,globalDofs) = A(boundary,globalDofs) + kernel * ( diag(quadRules.singWeights) * shapeFuncsLog1 );
                end
                
                % evaluate kernel on points and quadrature points for
                % log kernel with collocation point inside element.
                for cPIndex = 1:length(interior)     
                    
                    elementLength1 = bemLogOp.rangeSpace.collocPoints.param(interior(cPIndex)) - mesh.startParameter(elNum); 
                    elementLength2 =  mesh.endParameter(elNum) - bemLogOp.rangeSpace.collocPoints.param(interior(cPIndex));
                    alpha1 = elementLength1 / (elementLength1 + elementLength2);
                    alpha2 = elementLength2 / (elementLength1 + elementLength2);
                    tau = [ alpha1 * quadRules.singPoints alpha1 + alpha2 * quadRules.singPoints ];
                    
                    kernel = bemLogOp.evalKernelOnElement( elNum, interior(cPIndex), tau );
                    
%                     elementLength1 = bemLogOp.rangeSpace.collocPoints.param(interior(cPIndex)) - mesh.startParameter(elNum);
%                     logTau1 = mesh.startParameter(elNum) + quadRules.singPoints * elementLength1;
%                     elementLength2 =  mesh.endParameter(elNum) - bemLogOp.rangeSpace.collocPoints.param(interior(cPIndex));
%                     logTau2 = bemLogOp.rangeSpace.collocPoints.param(interior(cPIndex)) + quadRules.singPoints * elementLength2;
%                     logTau = [logTau1 logTau2];                   
%                     
%                     [quadX, quadY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( logTau, 0 );
%                     [dX, dY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( logTau, 1 );
%                     weights = sqrt(dX.^2 + dY.^2) .* [ elementLength1*quadRules.singWeights ...
%                                                         elementLength2*quadRules.singWeights ];
% 
%                     K = bemLogOp.evalBemLogKFunction( bemLogOp.rangeSpace.collocPoints, interior(cPIndex), ...
%                                                       quadX, quadY );
% 
%                     Xdiff = quadX - bemLogOp.rangeSpace.collocPoints.X(interior(cPIndex)) * ones(1,length(quadX));
%                     Ydiff = quadY - bemLogOp.rangeSpace.collocPoints.Y(interior(cPIndex)) * ones(1,length(quadY));
%                     R = sqrt( Xdiff.^2 +  Ydiff.^2 );
% 
%                     % compute the kernel
%                     kernelLog2 = log(R) .* K .* weights;
                    
                    % evaluate shape functions
%                     refParam = [elementLength1/elementLength * quadRules.singPoints.'; ...
%                         elementLength1/elementLength + elementLength2/elementLength * quadRules.singPoints.'];
                    shapeFuncsLog2 = bemLogOp.defSpace.evalShapeFunctions( elNum, tau );
                    
                    A(interior(cPIndex),globalDofs) = A(interior(cPIndex),globalDofs) ...
                        + kernel * ( diag([alpha1*quadRules.singWeights alpha2*quadRules.singWeights]) * shapeFuncsLog2 );
                end
                
            end
            
        end
        
    end 
        
        
    methods (Abstract)
        
        K = evalBemLogKFunction( bemLogOp, xpoints, index, Y1, Y2 );

    end   
    
end