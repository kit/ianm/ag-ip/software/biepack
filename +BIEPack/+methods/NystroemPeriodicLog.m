classdef NystroemPeriodicLog < BIEPack.BIEPackObject
    % BIEPack.methods.NystroemPeriodicLog Solution of 2nd kind weakly singular 
    % integral equations with periodic kernels via a Nystroem method
    % 
    % The class implements the method well known from the Colton/Kress
    % books for systems of equations. These take the form
    %
    %  ( I - A ) \phi = \psi
    %
    % where A is an NxN matrix of operators A_{jk} : V_k \to W_j. Written 
    % out, we have 
    %
    % phi_j - \sum_{k = 1}^N A_{jk} \phi_k = \psi_j ,  j = 1,...,N
    %
    % Because of the identity operator in the equation, V_j = W_j is a 
    % requirement.
    %
    % The operators A_{jk} are all linear integral operators with a kernel 
    % of one of the following forms
    %
    % - a continous kernel, 2pi-periodic with respect to both arguments.
    %   Such an operator is provided by the BIEPack.ops.PeriodicSmoothOp
    %   class.
    %
    % - a weakly singular kernel of the form
    %
    %   1/(2pi) log( 4 sin^2( (t-s) / 2 ) ) K_1(s,t) + K_2(s,t)
    %
    %   as implemented in the BIEPack.ops.PeriodicLogOp class. Both K_1 and
    %   K_2 are assumed to be 2pi-periodic and at least continuous.
    %
    % The functions \psi_j are also all assumed to be 2pi-periodic.
    %
    % Either on construction or through a call to the setOperators() method
    % the integral operators for the problem need to be specified. They are
    % passed as a square cell matrix. 
    %
    % The right hand side is specified as a cell vector with each entry
    % being a coefficient vector specifying a function from the appropriate
    % space. It is also specified either on construction or by a call to
    % the setRhs() method which has to be carried out after the call to
    % setOperators().
    %
    
    properties ( Constant )
        
        %> @brief name of the method used in requirement checking
        METHOD_NAME = 'NystroemPeriodicLog';
        
    end
    
    properties (SetAccess = protected)
        
        % cell matrix of operators
        opMatrix = [];
        
        % cell matrix of implementation functions for each operator
        implMatrix = [];
        
        % cell vector of right hand side coefficient vectors
        rhsVector = [];
        
        % start indeces for coefficients of spaces
        startInd = [];
        
        % end indeces for coefficients of spaces
        endInd = [];
        
        % matrix of the linear system
        A = [];
        
        % vector for the right hand side
        b = [];
        
    end
    
    methods
        
        % Construct the method using a matrix ops of integral operators and
        % a vector rhs of right hand sides.        
        function nystroem = NystroemPeriodicLog( ops, rhs )
                        
            if ( nargin > 0 )
                nystroem.setOperators(ops);
                nystroem.setRhs(rhs);                
            end
            
        end
        
        
        % Sets a new matrix of operators. 
        %
        % Checks are made that this matrix is square and that the
        % definition and range spaces of corresponding operators match.
        %
        % A later call to the checkRequirements() method will check 
        % whether all the operators are able to provide the PeriodicLogOp 
        % or the PeriodicSmoothOp (only off the diagonal) interface and map
        % to the correct spaces.
        %
        function setOperators( nystroem, ops )
            
            [m, n] = size(ops);
                        
            if ( m ~= n )
                error( 'In NystroemPeriodicLog method setOperators: cell matrix of operators must be square' );
            end
            
            if ( m == 1 )
                ops = { ops };
            end
            
            sInd = ones(1,m);
            eInd = zeros(1,m);
            impl = cell(m);
            
            for j=1:m
                for k=1:m
                
                    % check the operator at this poisition for correct type
                    % and for requirements
                    thisOp = ops{j,k};
                    if ( ~ thisOp.checkRequirements( nystroem.METHOD_NAME ) )
%                     if ( BIEPack.utils.derivedFrom( thisOp, 'BIEPack.ops.PeriodicSmoothOp' ) ...
%                             && thisOp.checkRequirements('PeriodicSmoothOp') )                        
%                         impl{j,k} = thisOp.getPeriodicSmoothImplementation;
%                     elseif ( BIEPack.utils.derivedFrom( thisOp, 'BIEPack.ops.PeriodicLogOp' ) ...
%                             && thisOp.checkRequirements('PeriodicLogOp') )                        
%                         impl{j,k} = @thisOp.getPeriodicLogImplementation;
%                    else
                        error('In NystroemPeriodicLog method setOperators: entry (%d,%d) of operator matrix does not support periodic Nyström method.', j, k);
                    end
                    
                end
            end
            
            for j=1:m
                
                for k=2:n
                    
                    if ( ops{j,1}.rangeSpace ~= ops{j,k}.rangeSpace )
                        error('In NystroemPeriodicLog method setOperators: range spaces on line %d do not coincide.', j);
                    end
                    
                    if ( ops{1,j}.defSpace ~= ops{k,j}.defSpace )
                        error('In NystroemPeriodicLog method setOperators: definition spaces on column %d do not coincide.', j);
                    end
                    
                end
                
                % range and definition spaces on the diagonal should
                % coincide
                if ( ops{j,j}.defSpace ~= ops{j,j}.rangeSpace )
                    error('In NystroemPeriodicLog method setOperators: definition and range space of operator (%d,%d) do not coincide.', j, j);
                end
                
                % store start and end indices
                if ( j == 1 )
                    eInd(j) = ops{j,j}.defSpace.dimension;
                else
                    eInd(j) = eInd(j-1) + ops{j,j}.defSpace.dimension;
                end
                
                if ( j < n )
                    sInd(j+1) = eInd(j)+1;
                end
            end
            
            % set new state for object
            nystroem.opMatrix = ops;
            nystroem.implMatrix = impl;
            nystroem.rhsVector = [];
            nystroem.A = [];
            nystroem.b = [];
            nystroem.startInd = sInd;
            nystroem.endInd = eInd;
            
        end
        
        % Sets the right hand side vector for the problem.
        % This method should be calles after setOperators.
        function setRhs( nystroem, rhs )
            
            if ( isempty(nystroem.startInd) )
                error('In NystroemPeriodicLog method setRhs; method setOperators has not been called previously.');
            end
            
            if ( ~ iscell(rhs) )
                rhs = { rhs };
            end
            
            nystroem.b = zeros( nystroem.endInd(end), 1 );
            
            for j=1:length(rhs)
                
                if ( length(rhs{j}) ~= nystroem.endInd(j) - nystroem.startInd(j) + 1 )
                    error('In NystroemPeriodicLog method setRhs; dimension of rhs vector %d does not match space dimension.', j);                    
                end
                
                nystroem.b(nystroem.startInd(j):nystroem.endInd(j)) = rhs{j};
            end
            
            nystroem.rhsVector = rhs;
            
        end
        
        
        % Assembles the linear system matrix for the given problem         
        function assembleMatrix( nystroem )
            
            [n,~] = size( nystroem.opMatrix );
            
            % create and populate the matrix
            nystroem.A = zeros(nystroem.endInd(n));
            for j=1:n
                for k=1:n
%                    nystroem.A( nystroem.startInd(j):nystroem.endInd(j), nystroem.startInd(k):nystroem.endInd(k) ) ...
%                        = feval(nystroem.implMatrix{j,k});
                    op = nystroem.opMatrix{j,k};
                    nystroem.A( nystroem.startInd(j):nystroem.endInd(j), nystroem.startInd(k):nystroem.endInd(k) ) ...
                        = - op.getImplementation(nystroem.METHOD_NAME);
                end
                nystroem.A( nystroem.startInd(j):nystroem.endInd(j), nystroem.startInd(j):nystroem.endInd(j) ) ...
                    = nystroem.A( nystroem.startInd(j):nystroem.endInd(j), nystroem.startInd(j):nystroem.endInd(j) ) ...
                    + eye( nystroem.endInd(j)-nystroem.startInd(j)+1 );
            end
            
        end                

        
        % apply the method to the given operator and right hand side
        %
        % returns the coefficient vector for the solution
        function phi = apply( nystroem )
            
            if ( isempty( nystroem.opMatrix ) )
                error('In NystroemPeriodicLog method apply(): operator matrix is unset. ');
            elseif ( isempty( nystroem.rhsVector ) )
                error('In NystroemPeriodicLog method apply(): right hand side vector is unset. ');
            end
            
            if ( isempty(nystroem.A) )
                nystroem.assembleMatrix;
            end
            
            phi = nystroem.A \ nystroem.b;
            
        end
        
        
    end
    
end

