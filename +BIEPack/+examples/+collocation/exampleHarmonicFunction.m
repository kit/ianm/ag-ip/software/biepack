%> @file exampleHarmonicFunction.m
%> @brief Contains the exampleHarmonicFunction() example function
%
% ======================================================================
%
%> @brief Example for a harmonic function that can be used convergence in the
%>     convergence analysis of the collocation method.
%>
%> @param x1 x1-coordinate of the evaluation points (matrix)
%> @param x2 x2-coordinate of the evaluation points (matrix of same size as x)
%>
%> @retval u values of the function u = log | x - y | + 0.2 * (x1 - z1)/ |x - z|^2
%>    with y = (0.2,-0.1) and z = (-1.75,0.4)
%> @retval d1_u derivative of u with respect to x1
%> @retval d2_u derivative of u with respect to x2
function [u, d1_u, d2_u] = exampleHarmonicFunction(x1,x2)

r12 = (x1-0.2).^2 + (x2+0.1).^2;
r22 = (x1+1.75).^2 + (x2-0.4).^2;

u = log(sqrt(r12)) + 0.2*(x1+1.75)./r22;
d1_u = (x1-0.2)./r12 + 0.2*(1./r22 - 2*(x1+1.75).^2./r22.^2);
d2_u = (x2+0.1)./r12 + 0.2*(-2*(x1+1.75).*(x2-0.4)./r22.^2);

end