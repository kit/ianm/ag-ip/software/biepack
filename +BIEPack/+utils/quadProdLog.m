function varargout = quadProdLog( a, b, N, t, outputMode )
%
% function varargout = quadProdLog( a, b, N, t, outputMode )
%
% Compute the quadrature weights w_j for a product quadrature rule for
% integrals with a logarithmic singularity:
%
% \int_a^b log | t - tau | f(tau) ds = \sum_{j=0}^N w_j f(tau_j)
%
% The quadrature points are assumed to be tau_j = a + j h, h = (b-a) / N.
%
% The weights are computed according to Atkinson (1997), Section 4.2. The
% function supports two different modes for the computation:
%
% - if outputMode is unset or if it is equal to 'weights', the function
%   returns a row vector of length N with the weights w_j.
%
% other outputs: todo

% x = linspace(a,b,N+1);
h = (b-a) / N;
z = (t-a) / h;

l = 0:N-1;
index = (z - l ~= 0 & z - l ~= 1);

psi_0 = - ones(1,N);
psi_0(index) = psi_0(index) + (z-l(index)) .* log(abs(z-l(index))) ...
    + (1 - z + l(index)) .* log(abs(1 - z +l(index)));

psi_1 = -0.25 - 0.5 * (z - l);
psi_1(index) = psi_1(index) + 0.5 * (z - l(index)).^2 .* log( abs( z - l(index) ) ) ...
    + 0.5 * (1 - (z-l(index)).^2 ) .* log( abs( 1 - z + l(index) ) );

% l = k:-1:k-N+1;
% index = (l ~= 0 & l ~= 1);
% psi_0 = - ones(1,N);
% psi_0(index) = log( abs(l(index)-1) ) - 1 + l(index) .* log( l(index) ./ (l(index)-1) );
% 
% psi_1 = -0.25 * ones(1,N);
% index2 = ( l == 1 );
% psi_1(index2) = -0.75;
% psi_1(index) = 0.5 * log( abs(l(index)-1) ) - 0.5 * l(index) - 0.25 ...
%     + 0.5 * l(index).^2 .* log( l(index) ./ (l(index)-1) );

alpha_of_t_k = h/2 * log(h)  + h * ( psi_0 - psi_1 );
beta_of_t_k = h/2 * log(h) + h * psi_1;

varargout{1} = [alpha_of_t_k 0] + [0 beta_of_t_k];



end

