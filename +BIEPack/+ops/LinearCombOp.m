%> @file LinearCombOp.m
%> @brief File containing the BIEPack.ops.LinearCombOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief Operator class used for representing linear combinations.
% 
%> A LinearCombOp is an operator constructed as a linear combination of any
%> finite number of instances of @link GeneralOp BIEPack.ops.GeneralOp @endlink. It supports any
%> method that is supported by all its summands.
% ======================================================================
classdef LinearCombOp < BIEPack.ops.GeneralOp
    % BIEPack.ops.IdentityOp < BIEPack.ops.GeneralOp
    
    properties (SetAccess = protected)
        
        %> The summands in the linear combination
        summands = [];
        
        %> The coefficients in the linear combinations
        coeffs = [];
        
    end
    
    methods
        
        %> @brief Initializes the operator.
        %
        %> The arguments are optional. When passed, the function
        %> setLinearComb() is called. If the arguments are not passed, a call
        %> to this function is necessary before the object can be used.
        function lcOp = LinearCombOp( summands, coeffs )
                        
            lcOp.randomName;
            lcOp.name = strcat('LinearCombOp_',lcOp.name);
            
            if ( nargin > 0 )
                lcOp.setLinearComb( summands, coeffs );
            end
            
        end
        
        
        %> @brief sets the summands and coefficients
        %
        %> The argument @a summands should be a cell array of instances of
        %> GeneralOp. All entries in this cell array must have the same
        %> definition and range space.
        %
        %> The argument @a coeffs should be an array of scalar
        %> numbers. Both must have equal length. The function sets the
        %> operator to represent the linear combination
        %>
        %> coeffs(1) * summands{1} + coeffs(2) * summands{2} + ...
        function setLinearComb( lcOp, summands, coeffs )
            
            for j=1:length(summands)
                
                % all summands should be derived from BIEPack.ops.GeneralOp
                if ( ~ BIEPack.utils.derivedFrom(summands{j}, 'BIEPack.ops.GeneralOp' ) )
                    error( 'In LinearCombOp method setLinearComp: summand %d is not a BIEPack.ops.GeneralOp', j );
                end
            
                % all summands should have the same definition and range spaces
                if ( j == 1 )
                    defSpace = summands{j}.defSpace;
                    rangeSpace = summands{j}.rangeSpace;
                    
                    optype = cell(1,length(summands));
                    
                else
                                        
                    if ( lcOp.defSpace ~= summands{j}.defSpace )
                        error( 'In LinearCombOp method setLinearComp: summand %d has a different definition space than summand 1', j );
                    end
                                        
                    if ( lcOp.rangeSpace ~= summands{j}.rangeSpace )
                        error( 'In LinearCombOp method setLinearComp: summand %d has a different range space than summand 1', j );
                    end
                    
                end
                
            end
            
            % The number of summands and coeffs should be the same
            if ( length(summands) ~= length(coeffs) )
                error( 'In LinearCombOp method setLinearComp: not the same number of summands and coefficients.' );                        
            end
            
            % set new values
            lcOp.defSpace = defSpace;
            lcOp.rangeSpace = rangeSpace;
            lcOp.summands = summands;
            lcOp.coeffs = coeffs;
            
        end
        
        
        %> @brief Implementation of GeneralOp.checkRequirements().
        %
        %> The function returns true if the requirements are met by all the
        %> summand operators.
        function isOk = checkRequirements( lcOp, methodName )
                
            isOk = true;   
            
            for j=1:length( lcOp.summands )
                isOk = isOk & lcOp.summands{j}.checkRequirements( methodName );
            end
                        
        end
        
        %> @brief implementation of GeneralOp.getImplementation().
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. It does 
        %> so by calling the corresponding function on
        %> each summand operator and forming the linear combination of the
        %> corresponding results.
        function A = getImplementation(lcOp, methodName, varargin)
           
            if ( isempty(varargin) )
                
                A = lcOp.coeffs(1) * lcOp.summands{1}.getImplementation( methodName );
                for j = 2:length(lcOp.summands)
                    A = A + lcOp.coeffs(j) * lcOp.summands{j}.getImplementation( methodName );                    
                end
                
            else

                A = lcOp.coeffs(1) * lcOp.summands{1}.getImplementation( methodName, varargin{:} );
                for j = 2:length(lcOp.summands)
                    A = A + lcOp.coeffs(j) * lcOp.summands{j}.getImplementation( methodName, varargin{:} );
                end
            end
            
            
        end
        
        
        %> @brief Multiplication of the operator by a scalar. 
        %
        %> Overrides the general case from BIEPack.ops.GeneralOp as this
        %> just requires adjusting the coefficients.
        function linCombOp = mtimes( A, B )
            
            % one of the two factors mus be a LinearCombOp
            if ( BIEPack.utils.derivedFrom( A, 'BIEPack.ops.LinearCombOp' ) )
                oldLCOp = A;
                scalar = B;
            else
                oldLCOp = B;
                scalar = A;
            end
            
            % the other factor should be a numeric scalar
            if ( ~isnumeric(scalar) )
                error( 'In LinearCombOp method mtimes: multiplication must be with a numeric value' );
            end
            
            if ( ~isscalar(scalar) )
                error( 'In LinearCombOp method mtimes: multiplication must be with a scalar' );                
            end
            
            % compute the linear combination by adjusting coefficients
            linCombOp = BIEPack.ops.LinearCombOp( oldLCOp.summands, scalar*oldLCOp.coeffs );
            
        end
        
        
        %> @brief Addition of two operators. 
        %
        %> The result of this operation is a BIEPack.ops.LinearCombOp
        %
        %> One of the two arguments may be a numeric scalar. In this case it
        %> is replaced with scalar times identity.
        %
        %> This overrides the implementation in BIEPack.ops.GeneralOp as
        %> the summands and coefs properties just need to be modified.
        function linCombOp = plus( A, B )
            
            % One of the two summands mus be a LinearCombOp
            if ( BIEPack.utils.derivedFrom( A, 'BIEPack.ops.LinearCombOp' ) )
                lCOp1 = A;
                otherOp = B;
            else
                lCOp1 = B;
                otherOp = A;
            end
            
            % is the other operator also BIEPack.ops.LinearCombOp
            if ( BIEPack.utils.derivedFrom( otherOp, 'BIEPack.ops.LinearCombOp' ) )
                
                linCombOp = BIEPack.ops.LinearCombOp( [ lCOp1.summands otherOp.summands ], [ lCOp1.coeffs, otherOp.coeffs ] );
      
            elseif ( BIEPack.utils.derivedFrom( otherOp, 'BIEPack.ops.GeneralOp' ) )
                                
                linCombOp = BIEPack.ops.LinearCombOp( [ lCOp1.summands {otherOp} ], [ lCOp1.coeffs, 1 ] );
                
            else                
            
                % the other summand should be a numeric scalar
                if ( ~isnumeric(otherOp) )
                    error( 'In GeneralOp method plus: one summand is neither GeneralOp nor numeric' );
                end
            
                if ( ~isscalar(otherOp) )
                    error( 'In GeneralOp method plus: one summand is neither GeneralOp nor scalar' );                
                end
                                
                if ( lCOp1.defSpace ~= lCOp1.rangeSpace )
                    error( 'In LinearCombOp method plus: scalar summand requires LinearCombOp to map definition space to definition space' );                
                end
                
                genOp2 = BIEPack.ops.IdentityOp( lCOp1.defSpace );
            
                % compute the linear combination
                linCombOp = BIEPack.ops.LinearCombOp( [ lCOp1.summands, {genOp2} ], [ lCOp1.coeffs, otherOp ] );
                
            end
            
        end
                
    end
    
end

