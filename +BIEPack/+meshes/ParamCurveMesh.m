%> @file ParamCurveMesh.m
%> @brief Contains the meshes.ParamCurveMesh class.
% ======================================================================
%> @brief A mesh type constructed on a single ParamCurve object.
%>
%> This type of mesh is associated with exactly one ParamCurve object 
%> so that the node positions are uniquely associated with parameter 
%> values. The mesh can thus be used to store geometrical information 
%> such as evaluations of the derivatives of the parametrization. This
%> is done through the evalDerivs() function.
%>
%> The mesh generation scheme of the BIEPack.meshes.BemMesh class may
%> be used through a call to the generateElements() method.
%> Alternatively, a uniform discretization of the parameter interval
%> may be used to construct the nodes via the generateElementsUniform()
%> method. In this case, the 'uniformInParam' property is set to true.
%>
%> Uniform mesh refinement, doubling the number of elements by placing
%> a new node in the middle of each element in parameter space, is
%> available through the uniformRefinement() method.
% ======================================================================
classdef ParamCurveMesh < BIEPack.meshes.BemMesh
    % BIEPack.meshes.ParamCurveMesh  < BIEPack.meshes.BemMesh
    
    properties (SetAccess = protected)
        
        %> @brief Row vector containing the parameter value for each node.
        t = [];
        
        %> @brief The maximum degree of the derivative of the 
        %> parameterisation for which the values have been evaluated. 
        maxDeriv = 0;
        
        %> @brief Matrix of dimension  <C> maxDeriv x numNodes </C>. For
        %> each node contains the x1-coordinates of the values of the 
        %> parameterisation derivatives.
        xDeriv = [];
        
        %> @brief Matrix of dimension  <C> maxDeriv x numNodes </C>. For
        %> each node contains the x2-coordinates of the values of the 
        %> parameterisation derivatives.
        yDeriv = [];
       
        %> @brief True whenever the nodes parameters are uniformly spaced
        %> over the parameter interval.
        uniformInParam = false;
    end
    
    methods
        
        %> @brief Construct a mesh from a single parametrized curve
        %>
        %> If the curve is ommited on construction, it must be set later with 
        %> the setCurve method.
        %>
        %> If the input parameter N is provided, a uniform discretization in
        %> parameter space is created, using the generateElementsUniform
        %> function.
        %>
        %> @param curve The ParamCurve object that will be discretised.
        %> @param N Number of sub-intervals in which the parameter interval
        %> will be separated.
        %> 
        %> @retval The newly constructed Mesh object.
        function mesh = ParamCurveMesh( curve, N )
            
            if ( nargin == 0 )
                curve = [];
            elseif ( ~ BIEPack.utils.derivedFrom(curve, 'BIEPack.geom.ParamCurve' ) )
                error( 'In ParamCurveMesh constructor: argument 1 is not a BIEPack.geom.ParamCurve' );
            end
            
            mesh@BIEPack.meshes.BemMesh(curve);
            
            if ( nargin == 2 )
                mesh.generateElementsUniform(N);
            end
            
            mesh.randomName;
            mesh.name = strcat('ParamCurveMesh_',mesh.name);
        end
        
        
        % Setting a new curve invalidates also the fields in this class.
        function setCurve( mesh, curve )
            
            if ( ~ BIEPack.utils.derivedFrom(curve, 'BIEPack.geom.ParamCurve' ) )
                error( 'In ParamCurveMesh.setCurve(): argument is not a BIEPack.geom.ParamCurve' );
            end
            
            mesh.t = [];
            mesh.maxDeriv = 0;
            mesh.xDeriv = [];
            mesh.yDeriv = [];
            mesh.uniformInParam = false;
            
            mesh.setCurve@BIEPack.meshes.BemMesh( curve );
            
        end
        

        %> @brief Generates a mesh in a way that the paramter values of 
        %> the nodes create N uniformly distributed elements. If the curve
        %> is closed this generates N nodes and N+1 nodes otherwise.
        %>
        %> @param N Number of sub-intervals in which the parameter interval
        %> will be separated.        
        function generateElementsUniform( mesh, N )
                        
            if ( isempty( mesh.theCurve ) )
                error( 'In ParamCurveMesh.generateElementsUniform(): Curve must be set.' );
            end
            
            % clear some data
            mesh.maxDeriv = 0;
            mesh.xDeriv = [];
            mesh.yDeriv = [];
            mesh.refinementRelation = [];
            
            % set mesh properties
            mesh.theParamCurves = { mesh.theCurve };
            mesh.uniformInParam = true;
            
            s = linspace( mesh.theCurve.startParam, mesh.theCurve.endParam, N+1 );
            
            mesh.curveIndexOfElement = ones(1,N);
            mesh.startParameter = s(1:N);
            mesh.endParameter = s(2:N+1);
            mesh.numElements = N;
            
            mesh.elements = zeros(3,N);
            mesh.elements(1,:) = 2;  % each element has two nodes
            mesh.elements(2,:) = 1:N;
            mesh.elements(3,:) = 2:N+1; % correction necessary if curve is closed
            
            if ( mesh.theCurve.closed )
                mesh.numNodes = N;
                mesh.t = s(1:N);
                mesh.elements(3,N) = 1;                
%                mesh.nodeToLocalDOFMap = [ 1:N; ones(1,N); N, 1:N-1; 2*ones(1,N) ];
            else                     
                mesh.numNodes = N+1;
                mesh.t = s;               
%                mesh.nodeToLocalDOFMap = [ 1:N, N; ones(1,N), 2; 0, 1:N-1, 0; 0, 2*ones(1,N-1), 0 ];
            end   
           
            [ mesh.nodesX, mesh.nodesY ] = mesh.theCurve.eval( mesh.t, 0 );
            
            % store information on normals
            mesh.evalDerivs(1);
            normalNorm = sqrt( mesh.xDeriv(1,:).^2 + mesh.yDeriv(1,:).^2 );
            mesh.normalAtNodeX = mesh.yDeriv(1,:) ./ normalNorm;
            mesh.normalAtNodeY = -mesh.xDeriv(1,:) ./ normalNorm;
            mesh.angleAtNode = pi*ones(1,mesh.numNodes);
            mesh.normalAtNodeExists = true(1,mesh.numNodes);
            
            if ( mesh.theCurve.closed )
                % check whether normal is the same at first and last point
                [nX, nY] = mesh.theCurve.normal( s(N+1) );
                normalNorm = sqrt( nX.^2 + nY.^2 );                              
                cosAngle = ( nX * mesh.normalAtNodeX(1) ...
                    + nY * mesh.normalAtNodeY(1) ) / normalNorm;
                sinAngle = ( nX * mesh.normalAtNodeY(1) ...
                    - nY * mesh.normalAtNodeX(1) ) / normalNorm;
                angleBetweenNormals = atan2(sinAngle,cosAngle);
                if ( abs( angleBetweenNormals ) > 1e-14 )
                    % there is an angle different from pi at the first node.
                    mesh.angleAtNode(1) = pi - angleBetweenNormals;
                    mesh.normalAtNodeExists(1) = false;
                end                    
            else
                % for a non-closed curve, the normal is not defined in the
                % endpoints
                mesh.angleAtNode(1) = 2*pi;
                mesh.normalAtNodeExists(1) = false;
                mesh.angleAtNode(end) = 2*pi;
                mesh.normalAtNodeExists(end) = false;
            end
            
        end


        %> @brief Generates a mesh using a distribution of the node paramter 
        %> values graded towards the end points of the curve. If the curve
        %> is closed this generates N nodes and N+1 nodes otherwise.
        %>
        %> @param N Number of sub-intervals in which the parameter interval
        %> will be separated.  
        %>
        %> @param q Grading parameter    
        function generateElementsGraded( mesh , N , q )
            
            if ( isempty( mesh.theCurve ) )
                error( 'In ParamCurveMesh.generateElementsGraded(): Curve must be set.' );
            end
        
            % clear some data
            mesh.maxDeriv = 0;
            mesh.xDeriv = [];
            mesh.yDeriv = [];
            mesh.refinementRelation = [];
            
            % set mesh properties
            mesh.theParamCurves = { mesh.theCurve };
            
            s = linspace( 0 , 1 , N+1 );
            v = ( 1/q - 0.5 ) .* ( 1 - 2.*s ).^3 +  ( 1/q ) .* ( 2.*s - 1) + 1/2;
            
            g = v.^q ./ ( v.^q + fliplr(v).^q );
%             g = zeros(1,N+1);
%             for i=1:1:N+1
%                 g( i ) = v(i)^q / ( v(i)^q + v(N+2-i)^q );
%             end
            t = mesh.theCurve.startParam + (mesh.theCurve.endParam - mesh.theCurve.startParam) .* g;
            
            mesh.curveIndexOfElement = ones(1,N);
            mesh.startParameter = t(1:N);
            mesh.endParameter = t(2:N+1);
            mesh.numElements = N;
            
            mesh.elements = zeros(3,N);
            mesh.elements(1,:) = 2;  % each element has two nodes
            mesh.elements(2,:) = 1:N;
            mesh.elements(3,:) = 2:N+1; % correction necessary if curve is closed
            
            if ( mesh.theCurve.closed )
                mesh.numNodes = N;
                mesh.t = t(1:N);
                mesh.elements(3,N) = 1;                
%                mesh.nodeToLocalDOFMap = [ 1:N; ones(1,N); N, 1:N-1; 2*ones(1,N) ];
            else                     
                mesh.numNodes = N+1;
                mesh.t = t;               
%                mesh.nodeToLocalDOFMap = [ 1:N, N; ones(1,N), 2; 0, 1:N-1, 0; 0, 2*ones(1,N-1), 0 ];
            end
            
            [ mesh.nodesX, mesh.nodesY ] = mesh.theCurve.eval( mesh.t, 0 );
            
            % store information on normals
            mesh.evalDerivs(1);
            normalNorm = sqrt( mesh.xDeriv(1,:).^2 + mesh.yDeriv(1,:).^2 );
            mesh.normalAtNodeX = mesh.yDeriv(1,:) ./ normalNorm;
            mesh.normalAtNodeY = -mesh.xDeriv(1,:) ./ normalNorm;
            mesh.angleAtNode = pi*ones(1,mesh.numNodes);
            mesh.normalAtNodeExists = true(1,mesh.numNodes);
            
            if ( mesh.theCurve.closed )
                % check whether normal is the same at first and last point
                [nX, nY] = mesh.theCurve.normal( t(N+1) );
                normalNorm = sqrt( nX.^2 + nY.^2 );                              
                cosAngle = ( nX * mesh.normalAtNodeX(1) ...
                    + nY * mesh.normalAtNodeY(1) ) / normalNorm;
                sinAngle = ( nX * mesh.normalAtNodeY(1) ...
                    - nY * mesh.normalAtNodeX(1) ) / normalNorm;
                angleBetweenNormals = atan2(sinAngle,cosAngle);
                if ( abs( angleBetweenNormals ) > 1e-14 )
                    % there is an angle different from pi at the first node.
                    mesh.angleAtNode(1) = pi - angleBetweenNormals;
                    mesh.normalAtNodeExists(1) = false;
                end                    
            else
                % for a non-closed curve, the normal is not defined in the
                % endpoints
                mesh.angleAtNode(1) = 2*pi;
                mesh.normalAtNodeExists(1) = false;
                mesh.angleAtNode(end) = 2*pi;
                mesh.normalAtNodeExists(end) = false;
            end
            
        end
        

        %> @brief Generates a mesh from given values for the parameter.
        %> 
        %> @param t vector of parameter values of the nodes.     
        %>
        %> @todo Handle closed curves in a sensible way.
        function generateElementsFromT( mesh, t )
                        
            if ( isempty( mesh.theCurve ) )
                error( 'In ParamCurveMesh.generateElementsfromT(): Curve must be set.' );
            end
            
            % clear some data
            mesh.maxDeriv = 0;
            mesh.xDeriv = [];
            mesh.yDeriv = [];
            mesh.refinementRelation = [];
            
            % set mesh properties
            mesh.theParamCurves = { mesh.theCurve };
            mesh.uniformInParam = false;
            
            N = length(t)-1;
            mesh.curveIndexOfElement = ones(1,N);
            mesh.startParameter = t(1:N);
            mesh.endParameter = t(2:N+1);
            mesh.numElements = N;
            
            mesh.elements = zeros(3,N);
            mesh.elements(1,:) = 2;  % each element has two nodes
            mesh.elements(2,:) = 1:N;
            mesh.elements(3,:) = 2:N+1; % correction necessary if curve is closed
            
            if ( mesh.theCurve.closed )
                
                mesh.numNodes = N;
                mesh.t = t(1:N);
                mesh.elements(3,N) = 1;                
%                mesh.nodeToLocalDOFMap = [ 1:N; ones(1,N); N, 1:N-1; 2*ones(1,N) ];
                
            else        

                mesh.numNodes = N+1;
                mesh.t = t;               
%                mesh.nodeToLocalDOFMap = [ 1:N, N; ones(1,N), 2; 0, 1:N-1, 0; 0, 2*ones(1,N-1), 0 ];
                
            end   
           
            [ mesh.nodesX, mesh.nodesY ] = mesh.theCurve.eval( mesh.t, 0 );
            
        end
        
        
        %> @brief Create elements by the method implemented in the
        %> BIEPack.meshes.BemMesh class. The method of the super class is
        %> called and the additional properties of the ParamCurveMesh class
        %> are set.
        %>
        %> @param h The maximum distance that two adjacent nodes will be 
        %> apart from each other.
        function generateElements( mesh, h )
            
            generateElements@BIEPack.meshes.BemMesh( mesh, h );
            
            mesh.t = mesh.startParameter;
            if ( ~ mesh.theCurve.closed )
                mesh.t = [ mesh.t mesh.endParameter(end) ];
            end
            
            mesh.maxDeriv = 0;
            mesh.xDeriv = [];
            mesh.yDeriv = [];
            
            mesh.uniformInParam = false;
        end
        
        
        %> @brief Evaluates derivatives of the parametrization in the node 
        %> points up to a specified degree.
        %>
        %> @param mesh Mesh for which the derivatives will be computed.
        %> @param degree The degree of the highest derivative that will
        %> be evaluated.
        function evalDerivs( mesh, degree )
            
            if ( degree <= mesh.maxDeriv )
                return;
            end
                        
            if ( mesh.theCurve.highestImplementedDerivative < degree )
                error( 'In ParamCurveMesh.evalDerivs(): degree %d is higher than highest implemented derivative of curve %s.', ...
                    degree, mesh.theCurve.name );
            end
            
            newXDeriv = zeros( degree, mesh.numNodes );
            newYDeriv = zeros( degree, mesh.numNodes );
            
            if ( mesh.maxDeriv > 0 )
                newXDeriv( 1:mesh.maxDeriv, : ) = mesh.xDeriv;            
                newYDeriv( 1:mesh.maxDeriv, : ) = mesh.yDeriv;
            end
            
            for j=mesh.maxDeriv+1:degree
                [ newXDeriv(j,:), newYDeriv(j,:) ] = mesh.theCurve.eval( mesh.t, j);
            end
            
            mesh.maxDeriv = degree;
            mesh.xDeriv = newXDeriv;
            mesh.yDeriv = newYDeriv;
            
        end
         
        %> @brief Generates a uniform refinement of the mesh, such that inbetween
        %> two nodes, a new node is added in the middle of the parameter
        %> interval. The finer mesh will be a ParamCurveMesh object aswell.
        %>
        %> @param coarsMesh Initial mesh that will be refined.
        %>
        %> @retval fineMesh The newly refined mesh, also a ParamCurveMesh 
        %> object.
        %> @retval relation Object of the class RefinementRelation,
        %> describing the refinement.
        
        function [ fineMesh, relation ] = uniformRefinement( coarseMesh )
            
            if ( coarseMesh.numNodes == 0 )
                error( 'In ParamCurveMesh.refine(): a mesh must exist for refinement.');
            end
            
            fineMesh = BIEPack.meshes.ParamCurveMesh( coarseMesh.theCurve ); 
            fineMesh.theParamCurves = coarseMesh.theParamCurves;
            fineMesh.name = strcat('unif_ref_of_',coarseMesh.name);
            
            if ( coarseMesh.theCurve.closed )
                newT = zeros(1, 2*coarseMesh.numNodes + 1);
                newT(1:2:2*coarseMesh.numNodes+1) = [ coarseMesh.t, coarseMesh.theCurve.endParam ];
                newT(2:2:2*coarseMesh.numNodes) = 0.5 * ( newT(1:2:2*coarseMesh.numNodes-1) + newT(3:2:2*coarseMesh.numNodes+1) );
                fineMesh.numNodes = 2*coarseMesh.numNodes;
                fineMesh.t = newT(1:2*coarseMesh.numNodes);
            else                
                newT = zeros(1, 2*coarseMesh.numNodes - 1);
                newT(1:2:2*coarseMesh.numNodes-1) = coarseMesh.t;
                newT(2:2:2*coarseMesh.numNodes-2) = 0.5 * ( newT(1:2:2*coarseMesh.numNodes-3) + newT(3:2:2*coarseMesh.numNodes-1) );
                fineMesh.numNodes = 2*coarseMesh.numNodes - 1;
                fineMesh.t = newT;
            end 
            
            fineMesh.numElements = 2*coarseMesh.numElements;
            fineMesh.curveIndexOfElement = ones(1, fineMesh.numElements );
            fineMesh.startParameter = newT(1:fineMesh.numElements);
            fineMesh.endParameter = newT(2:fineMesh.numElements+1);
            
            fineMesh.elements = zeros(3,fineMesh.numElements);
            fineMesh.elements(1,:) = 2;  % each element has two nodes
            fineMesh.elements(2,:) = 1:fineMesh.numElements;
            
            elementMap = cell(1,coarseMesh.numElements);
            for j = 1:coarseMesh.numElements
                elementMap{j} = [ 2*j-1, 2*j ];
            end
            
            newNodeMap = [ 2:2:2*coarseMesh.numElements; 1:coarseMesh.numElements; 0.5*ones(1,coarseMesh.numElements) ];
            
%             fineMesh.nodeToLocalDOFMap = zeros(4, fineMesh.numNodes);
%             fineMesh.nodeToLocalDOFMap(1,1:end-1) = 1:fineMesh.numNodes-1;
%             fineMesh.nodeToLocalDOFMap(2,1:end-1) = 1;
%             fineMesh.nodeToLocalDOFMap(3,2:end-1) = 1:fineMesh.numNodes-2;
%             fineMesh.nodeToLocalDOFMap(4,2:end-1) = 2;
%             fineMesh.nodeToLocalDOFMap(1,end) = fineMesh.numNodes-1;
%             fineMesh.nodeToLocalDOFMap(2,end) = 2;
            
            if ( fineMesh.theCurve.closed )
                fineMesh.elements(3,:) = [ 2:fineMesh.numElements, 1];
%                 fineMesh.nodeToLocalDOFMap(3,1) = fineMesh.numElements;
%                 fineMesh.nodeToLocalDOFMap(4,1) = 2;
%                 fineMesh.nodeToLocalDOFMap(3,end) = fineMesh.numElements;
%                 fineMesh.nodeToLocalDOFMap(4,end) = 1;                
            else
                fineMesh.elements(3,:) = 2:fineMesh.numElements+1;
            end
           
            [ fineMesh.nodesX, fineMesh.nodesY ] = fineMesh.theCurve.eval( fineMesh.t, 0 );
            
            % store information on normals
            fineMesh.evalDerivs(1);
            normalNorm = sqrt( fineMesh.xDeriv(1,:).^2 + fineMesh.yDeriv(1,:).^2 );
            fineMesh.normalAtNodeX = fineMesh.yDeriv(1,:) ./ normalNorm;
            fineMesh.normalAtNodeY = -fineMesh.xDeriv(1,:) ./ normalNorm;
            fineMesh.angleAtNode = pi*ones(1,fineMesh.numNodes);
            fineMesh.normalAtNodeExists = true(1,fineMesh.numNodes);
            
            if ( fineMesh.theCurve.closed )
                % the first node remains the same, copy data from the coarse mesh
                fineMesh.angleAtNode(1) = coarseMesh.angleAtNode(1);
                fineMesh.normalAtNodeExists(1) = coarseMesh.normalAtNodeExists(1);
            else
                % for a non-closed curve, the normal is not defined in the
                % endpoints
                fineMesh.angleAtNode(1) = 2*pi;
                fineMesh.normalAtNodeExists(1) = false;
                fineMesh.angleAtNode(end) = 2*pi;
                fineMesh.normalAtNodeExists(end) = false;
            end
                        
            fineMesh.uniformInParam = coarseMesh.uniformInParam;
            
            nodeMap = 1:2:2*coarseMesh.numNodes-1;
            relation = BIEPack.meshes.RefinementRelation(coarseMesh,fineMesh,nodeMap,newNodeMap,elementMap);
            fineMesh.refinementRelation = relation;
            
        end
        
    end
    
end
