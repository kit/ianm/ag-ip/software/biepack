%> @file LaplaceDLPot.m
%> @brief File containing the @link BIEPack.ops.LaplaceDLPot LaplaceDLPot @endlink class.
% ======================================================================
%> @brief The double layer potential for Laplace's equation. 
%
%> This class implements discretizations of the operator
%>
%> DL phi (x) = 1/(2pi) \int_C n(y) . (x - y) / |x - y|^2 phi(y) ds(y), x \notin C
%>
%> Implementations as an integral operator include:
%>  - over a smooth closed curve C mapping a space of trigonometric
%>    polynomials (BIEPack.spaces.TrigPol) into a space of trigonometric 
%>    polynomials defined on a different smooth closed curve D.
%>  - over any curve to any curve when mapping a @link BIEPack.spaces.BemSpace 
%>    BemSpace @endlink onto another one
%>
%> The class may also be used for representing a double layer potential
%> that is to be evaluated on arbitrary points of the plane not in C. See
%> the function evalPotential() for corresponding functionality.
% ======================================================================
classdef LaplaceDLPot < BIEPack.ops.PeriodicSmoothOp
    
    properties (SetAccess = protected)
        
        % @brief Handle to a distance function used to determine the number
        % of Gaussian quadrature points to be used for quadrature on each element. 
        %
        % The bemDistanceFunction is only used for a potential defined on a 
        % BemSpace. Close to the singularity in the kernel, the integrand becomes
        % nearly singular. The number of quadrature points needs to be increased
        % to maintain accuracy. 
        %
        % The distance function has the calling sequence
        % @code
        %  N = function distFunc(r)
        % @endcode
        % where r is the minimal distance between points on the element and
        % evaluation points and N is the number of Gaussian quadrature
        % points to be used for computing integrals over the element.       
        bemDistanceFunction = [];
        
    end
    
    methods
        
                
        %> @brief Construct the operator
        %
        %> Just passes on the call to the super class constructor. Checks
        %> whether the spaces are compatible with this type of operator are
        %> carried out by the checkRequirements function
        function dlPot = LaplaceDLPot( defSpace, rangeSpace, distFunc )
            
            if ( nargin == 0 )
                defSpace = [];
                rangeSpace = [];
            elseif ( nargin == 1 )
                rangeSpace = [];
            end
            
            dlPot@BIEPack.ops.PeriodicSmoothOp(defSpace,rangeSpace);
            
            if ( nargin < 3 )
                dlPot.bemDistanceFunction = @BIEPack.ops.LaplaceDLPot.standardDistanceFunction;
            else
                dlPot.bemDistanceFunction = distFunc;
            end
            
            dlPot.randomName;
            dlPot.name = strcat('LaplaceDLPot_',dlPot.name);
            
        end
        
        %> @brief Matrix generation for evaluation
        %
        %> Evaluation of the kernel function of the potential in the points
        %> (X1,X2) and (Y1,Y2) with the derivatives of the parametrization 
        %> in these points  given by (dY1, dY2). X1, X2 should be column vectors
        %> of equal length, Y1, Y2, dY1,  dY2 row vectors of equal length.
        function K = evalSmoothKernelInX(dlpot,X1,X2,Y1,Y2,dY1,dY2) %#ok<INUSL>
            
            ones_h = ones(1,length(Y1));
            ones_v = ones(length(X1),1);

            diff1 = X1 * ones_h - ones_v * Y1;
            diff2 = X2 * ones_h - ones_v * Y2;
            R2 = diff1.^2 + diff2.^2;
            
            K = 1/(2*pi) * ( ones_v * dY2 .* diff1 - ones_v * dY1 .* diff2 ) ./ R2;
            
        end
        
        %> @brief Implementation of the
        %> PeriodicSmoothOp.evalPeriodicSmoothKernel()
        %
        %> Obtain the evaluation points and call evalSmoothKernelInX()
        function K = evalPeriodicSmoothKernel(dlpot)
            
            X1 = dlpot.rangeSpace.theMesh.nodesX.';
            X2 = dlpot.rangeSpace.theMesh.nodesY.';
                        
            defMesh = dlpot.defSpace.theMesh;            
            defMesh.evalDerivs(1);
            
            xi = defMesh.nodesX;
            eta = defMesh.nodesY;
                        
            % Obtain values for derivative of parametrization but
            % compensate for the interval [0, 2*pi]
            L = dlpot.defSpace.theCurve.endParam - dlpot.defSpace.theCurve.startParam;
            xip = L/(2*pi) * defMesh.xDeriv(1,:);
            etap = L/(2*pi) * defMesh.yDeriv(1,:);
            
            K = dlpot.evalSmoothKernelInX( X1, X2, xi, eta, xip, etap );
            
        end
        
        %> @brief Assemble matrix for the potential defined on a BemSpace
        %>
        %> The integral over the curve is split into integrals over
        %> elements. The integrals over each element and each basis function
        %> having support on this element are computed separately and added
        %> to the matrix.
        function A = getPotentialImplementationForBem(dlpot,X1,X2)
           
            % collocation points are the nodes of the mesh
            mesh = dlpot.defSpace.theMesh;
            
            A = zeros( length(X1), dlpot.defSpace.dimension);
            
            % define quadrature points and weights            
            % [quadPoints, quadWeights] = BIEPack.utils.gaussLegendreQuad(4,0,1);
            
            % Create a container for quadrature points and weights so that these are
            % only computed once for each number of points.
            quadContainer = cell(1,10);
            
            for elNum = 1:mesh.numElements
                
                globalDofs = dlpot.defSpace.localToGlobalDOFMap( elNum ).globalDOF;
                
                for k = 1:length(X1)
                    
                    % estimate distance of element from evaluation point 
                    boundaryNodes = [ mesh.elements(2,elNum) mesh.elements(3,elNum) ];
                    r = min( sqrt( ( (X1(k) - mesh.nodesX(boundaryNodes)).^2 + (X2(k) - mesh.nodesY(boundaryNodes)).^2 ) ) );
                    N = dlpot.bemDistanceFunction(r);
                    if ( N <= length(quadContainer) && ~isempty(quadContainer{N}) )
                        quadPoints = quadContainer{N}.points;
                        quadWeights = quadContainer{N}.weights;
                    else
                        [quadPoints, quadWeights] = BIEPack.utils.gaussLegendreQuad(N,0,1);
                        quadContainer{N} = struct('points',quadPoints,'weights',quadWeights);
                    end
                
                    % find coordinates and tangent for quadrature points
                    elementLength = mesh.endParameter(elNum) - mesh.startParameter(elNum);
                    tau = mesh.startParameter(elNum) + quadPoints * elementLength;
                    [quadX, quadY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( tau, 0 );
                    [dX, dY] = mesh.theParamCurves{ mesh.curveIndexOfElement(elNum) }.eval( tau, 1 );

                    % evaluate kernel
                    K = elementLength * dlpot.evalSmoothKernelInX(X1(k),X2(k),quadX,quadY,dX,dY);

                    % evaluate the basis functions with support on this element
                    shapeFuncs = dlpot.defSpace.evalShapeFunctions( elNum, quadPoints.' );

                    % compute integrals and add to matrix
                    A(k,globalDofs) = A(k,globalDofs) + K * (shapeFuncs .* ( quadWeights.' * ones(1,length(globalDofs),1) ) );  
                end              
                
            end
            
        end
        
        %> @brief Obtain a discretized version of the potential operator 
        %> 
        %> The method returns a matrix representing the approximation of
        %> the integral over C by a method appropriate for the definition space.
        %> Multiplication of this matrix by a column vector containing the
        %> representation of the density in the basis of the definition space
        %> gives an approximation of 
        %> the potential in the points given by the coordinates X1,X2 in
        %> the plane. These parameters should be column vectors of equal length.
        function A = getPotentialImplementation(dlpot,X1,X2)
                        
            if ( BIEPack.utils.derivedFrom(dlpot.defSpace, 'BIEPack.spaces.TrigPol' ) )
                % The density is a trigonometric polynomial in parameter
                % space. Compute the integral via composite trapeoidal
                % rule with the nodes as quadrature points.
                        
                defMesh = dlpot.defSpace.theMesh;            
                defMesh.evalDerivs(1);
            
                xi = defMesh.nodesX;
                eta = defMesh.nodesY;
            
                % Obtain values for derivative of parametrization but
                % compensate for the interval [0, 2*pi]
                xip = dlpot.defSpace.L/(2*pi) * defMesh.xDeriv(1,:);
                etap = dlpot.defSpace.L/(2*pi) * defMesh.yDeriv(1,:);
                
                A = 2*pi / dlpot.defSpace.dimension * dlpot.evalSmoothKernelInX(X1,X2,xi,eta,xip,etap);
                        
            elseif ( BIEPack.utils.derivedFrom(dlpot.defSpace, 'BIEPack.spaces.BemSpace' ) )
                % The density is a finite element function. Evaluate the
                % potential for all evaluation points and all basis
                % functions as a sum of integrals over the elements.
                
                A = getPotentialImplementationForBem(dlpot,X1,X2);
                
            else
                                
                error( 'In LaplaceSLPot.getPotentialImplementation(): no method to evaluate this potential for this type of definition space.' );
                
            end
            
        end
        
        %> @brief Evaluate the potential at arbitrary points in space. 
        %
        %> The parameters X1 and X2 should be of the same size and the result is
        %> also of this size. The argument phi is a column vector of values of
        %> the density in the mesh points of the mesh on C.
        %>
        %> The potential is evaluated by the composite trapezoidal rule.
        function P = evalPotential(dlpot,X1,X2,phi)
            
            P_size = size(X1);
            
            x1 = X1(:);
            x2 = X2(:);
            
            A = getPotentialImplementation(dlpot,x1,x2); 
            
            P = A * phi;
            P = reshape(P,P_size);
            
        end
        
        %> @brief Check whether requirements for application of a numerical
        %> method are satisfied
        function isOk = checkRequirements(laplaceDLPot, methodName )
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                isOk = laplaceDLPot.checkRequirementsForPeriodicSmoothOp();
            else         
                isOk = false;
                % warning('In LaplaceDLPot method checkRequirements: requested type %s is not supported.', optype);
            end
            
        end
        
        %> @brief Implementaton of GeneralOp.getImplementation().
        %>
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        function A = getImplementation( laplaceDLPot, methodName, varargin)
            
            if ( strcmp( methodName, BIEPack.methods.NystroemPeriodicLog.METHOD_NAME ) )
                A = laplaceDLPot.getPeriodicSmoothImplementation();
            else
                error('In LaplaceDLPot method getImplementation: Unsupported method %s requested.', methodName );
            end
            
        end
                
    end
    
    methods (Static)
        
        %> @brief The standard distance function used for potential
        %> evaluations when using a BemSpace.
        function N = standardDistanceFunction(r)
            
%             N = ceil(1./r);
%             N(N > 4) = 4;
            N = 2*ones(size(r));
            
        end
        
    end
    
end

