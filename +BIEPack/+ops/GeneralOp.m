%> @file GeneralOp.m
%> @brief File containing the ops.GeneralOp class.
% ======================================================================
%> @ingroup OpsGroup 
%> @brief Abstract general linear operator mapping from a definition space
%> to a range space.
% 
%> The GeneralOp class represents a discretization of some boundary
%> integral operator. Various subclasses of GeneralOp exist that support
%> specific numerical methods. A concrete implementation such as for example
%> a single layer boundary operator can be derived from all these
%> subclasses to support the corresponding methods.
%>
%> Before a method can be applied, checkRequirements() should
%> be called by the method. Not all requirements for the application of a
%> method can be taken care of by deriving from the right subclass of 
%> GeneralOp. For instance, the same boundary integral operator might
%> use a BemSpace or a TrigPol space as its definition space. Depending on which
%> space is used, the requirements of a collocation method might be met or not.
%>
%> GeneralOp objects can be linearly combined, forming a LinearCombOp.
%> These linear combinations can be created using the + and * operators
%> of the Matlab programming language: If A and B are instances of 
%> GeneralOp, then an expression like 3*A + 4*B makes sense.
% ======================================================================
classdef GeneralOp < BIEPack.BIEPackObject
    % BIEPack.ops.GeneralOp < BIEPack.BIEPackObject
    %
    % A general linear operator mapping from a definition space to a range space.
    %
    
    properties ( SetAccess = protected )
        
        %> The space the operator is defined on
        defSpace = [];
        
        %> The space the operator maps into
        rangeSpace = [];
        
    end
    
    methods
        
        %> @brief Construct the operator. The space of definition and the range
        %> should be specified.
        function generalOp = GeneralOp( defSpace, rangeSpace )
            
            generalOp.randomName;
            generalOp.name = strcat('GeneralOp_',generalOp.name);
            
            if ( nargin > 0 )
                generalOp.defSpace = defSpace;
                generalOp.rangeSpace = rangeSpace;
            end
            
        end
        
        %> @brief Sets the definition space
        %
        %> @todo check whether this is a valid  space
        function setDefSpace( generalOp, defSpace )
            generalOp.defSpace = defSpace;
        end
        
        %> @brief Sets the range space
        function setRangeSpace( generalOp, rangeSpace )
            generalOp.rangeSpace = rangeSpace;
        end
        
        
        %> @brief multiplication of an operator by a scalar. 
        %
        %> One of the factors should be a scalar, the other an instance of
        %> GeneralOp.
        %> The result of this operation is a BIEPack.ops.LinearCombOp
        function linCombOp = mtimes( A, B )
            
            % one of the two factors mus be a GeneralOp
            if ( BIEPack.utils.derivedFrom( A, 'BIEPack.ops.GeneralOp' ) )
                genOp = A;
                scalar = B;
            else
                genOp = B;
                scalar = A;
            end
            
            % the other factor should be a numeric scalar
            if ( ~isnumeric(scalar) )
                error( 'In GeneralOp method mtimes: multiplication must be with a numeric value' );
            end
            
            if ( ~isscalar(scalar) )
                error( 'In GeneralOp method mtimes: multiplication must be with a scalar' );                
            end
            
            % compute the linear combination
            linCombOp = BIEPack.ops.LinearCombOp( { genOp }, scalar );
            
        end
        
        
        %> @brief Addition of two operators. 
        %
        %> The result of this operation is a BIEPack.ops.LinearCombOp
        %
        %> Usually, both arguments are instances of GeneralOp.
        %> One of the two arguments may be a numeric scalar. In this case it
        %> is replaced with scalar times identity.
        function linCombOp = plus( A, B )
            
            % One of the two summands mus be a GeneralOp
            if ( BIEPack.utils.derivedFrom( A, 'BIEPack.ops.GeneralOp' ) )
                genOp1 = A;
                otherOp = B;
            else
                genOp1 = B;
                otherOp = A;
            end
            
            if ( BIEPack.utils.derivedFrom( otherOp, 'BIEPack.ops.LinearCombOp' ) )
                linCombOp = BIEPack.ops.LinearCombOp( [otherOp.summands {genOp1}], [otherOp.coeffs, 1] );
                return
            end
            
            % is the other operator a BIEPack.ops.GeneralOp
            if ( BIEPack.utils.derivedFrom( otherOp, 'BIEPack.ops.GeneralOp' ) )
                
                scalar2 = 1;
                genOp2 = otherOp;
      
            else                
            
                % the other summand should be a numeric scalar
                if ( ~isnumeric(otherOp) )
                    error( 'In GeneralOp method plus: one summand is neither GeneralOp nor numeric' );
                end
            
                if ( ~isscalar(otherOp) )
                    error( 'In GeneralOp method plus: one summand is neither GeneralOp nor scalar' );                
                end
                                
                if ( genOp1.defSpace ~= genOp1.rangeSpace )
                    error( 'In GeneralOp method plus: scalar summand requires GeneralOp to map definition space to definition space' );                
                end
                
                scalar2 = otherOp;
                genOp2 = BIEPack.ops.IdentityOp( genOp1.defSpace );
                
            end
            
           % compute the linear combination
           linCombOp = BIEPack.ops.LinearCombOp( { genOp1, genOp2 }, [ 1, scalar2 ] );
            
        end
        
    end
    
    methods (Abstract)
                
        %> @brief Checks whether the method given by methodName can be applied to
        %> this operator. If all requirements are met, isOk is returned as
        %> true, otherwise as false.
        %
        %> This abstract function must be implemented by any concrete operator
        %> implementing GeneralOp. It can in turn call specific functions of
        %> intermediate classes for checking requirements of certain
        %> methods.
        isOk = checkRequirements( genOp, methodName );
        
        %> @brief Obtain the implementation of the operator as a matrix.
        %
        %> The function returns a matrix representation of the discrete
        %> operator required by the method given by @a methodName. 
        A = getImplementation(lcOp, methodName, varargin)
        
    end
    
end

