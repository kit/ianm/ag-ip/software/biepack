%> @file StraightLine.m
%> @brief Contains the geom.StraightLine class.
% ======================================================================
%> @brief Straight line connecting two points.
classdef StraightLine < BIEPack.geom.ParamCurve
    % BIEPack.geom.StraightLine < BIEPack.geom.ParamCurve
    
    properties
        
        %> @brief Initial x1-coordinate of the starting point before 
        %> application of any affine transformations implemented in the 
        %> ParamCurve class.
        startPoint1 = 0;
        
        %> @brief Initial x2-coordinate of the starting point.
        startPoint2 = 0;
        
        
        %> @brief Initial x1-coordinate of the ending point.
        endPoint1 = 1;
        
        %> @brief Initial x1-coordinate of the ending point.
        endPoint2 = 0;
        
        
        %> @brief Initial x1-coordinate of the direction vector.
        dir1 = 1;
        
        %> @brief Initial x2-coordinate of the direction vector.
        dir2 = 0;
        
    end
    
    methods
        
        %> @brief Constructs a straight line that connects the points s and
        %> e.
        %>
        %> @param s Vector containing the coordinates of the starting point
        %> of the line.
        %> @param e Vector containing the coordinates of the ending point
        %> of the line.
        %> 
        %> @retval straightLine The newly constructed StraightLine object.
        function straightLine = StraightLine( s, e ) 
            
            straightLine.randomName;
            straightLine.name = strcat('StraightLine_',straightLine.name);
            
            straightLine.startPoint1 = s(1);
            straightLine.startPoint2 = s(2);
            straightLine.endPoint1 = e(1);
            straightLine.endPoint2 = e(2);
            straightLine.dir1 = e(1) - s(1);
            straightLine.dir2 = e(2) - s(2);
            
            straightLine.smoothness = inf;
            straightLine.globalSmoothness = inf;
            straightLine.highestImplementedDerivative = inf;
        end
                
        %> @brief Implementation of basic parametrization p + t*d over the interval [0,1]
        %>
        %> Derivatives of the parametrization up to any order are
        %> implemented.
        %>
        %> @param t The parameter value, can be vector or matrix.
        %> @param n Order of the derivative to be evaluated, must be an
        %> an integer.
        %>
        %> @retval y1 1st coordinate of the curve points. 
        %> @retval y2 2nd coordinate of the curve points.
        function [y1, y2] = paramEval(straightLine, t, n)
            
            if ( n == 0 )
                y1 = straightLine.startPoint1 + t * straightLine.dir1;
                y2 = straightLine.startPoint2 + t * straightLine.dir2;
            elseif ( n == 1 )
                y1 = straightLine.dir1 * ones(size(t));
                y2 = straightLine.dir2 * ones(size(t));
            else
                y1 = zeros(size(t));
                y2 = zeros(size(t));
            end
            
        end
    end
    
end

